﻿using System;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements a framework dependency.</summary>
    public sealed class FwDependency: IItem, IDependency
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /** Type ID. */
        public const string TYPE_ID = "NuA";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="group">Dependency group.</param>
        /// <param name="sec">Section.</param>
        public FwDependency(DependencyGroup group, DdpSection sec)
        {
            Group = group;

            ID = sec.Name;
            Name = sec.GetString("Name");
            Description = sec.GetString("Description");
            AssemblyName = sec.GetString("AssemblyName");
            TargetFramework = sec.GetString("TargetFramework");
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="group">Dependency group.</param>
        public FwDependency(DependencyGroup group)
        {
            Group = group;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";
            AssemblyName = "";
            TargetFramework = "";
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="group">Dependency group.</param>
        /// <param name="template">Template.</param>
        public FwDependency(DependencyGroup group, FwDependency template)
        {
            Group = group;

            ID = null;
            Name = template.Name;
            Description = template.Description;
            AssemblyName = template.AssemblyName;
            TargetFramework = template.TargetFramework;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Group.Repository; }
        }


        /// <summary>Gets the dependecy group for this dependency.</summary>
        public DependencyGroup Group
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency assembly name.</summary>
        public string AssemblyName
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency target framework.</summary>
        public string TargetFramework
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the dependency ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the ddp section that contains this item.</summary>
        public DdpSection Section
        {
            get { return Group.Section.GetSection(ID); }
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            Group.FrameworkDependencies._Items.Remove(ID);
        }


        /// <summary>Saves the object.</summary>
        /// <param name="sec">Parent section.</param>
        public void Save(DdpSection sec)
        {
            if(ID == null)
            {
                ID = (TYPE_ID + "." + StringOp.Unique());
                Group.FrameworkDependencies._Items.Add(ID, this);
            }

            sec = sec.Sections.Add(ID);

            sec.SetString("Name", Name);
            sec.SetString("Description", Description);
            sec.SetString("AssemblyName", AssemblyName);
            sec.SetString("TargetFramework", TargetFramework);
        }

        
        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is DependencyGroup)) return null;

            return new FwDependency((DependencyGroup) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is DependencyGroup)) return null;
            if(((DependencyGroup) target).ID == Group.ID) return null;

            Group.FrameworkDependencies._Items.Remove(ID);
            (Group = (DependencyGroup) target).FrameworkDependencies._Items.Add(ID, this);
            return this;
        }
    }
}
