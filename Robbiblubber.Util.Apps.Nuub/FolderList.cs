﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class provides a list of folders.</summary>
    public sealed class FolderList: IImmutableList<Folder>, IEnumerable<Folder>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Items.</summary>
        internal Dictionary<string, Folder> _Items = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="parent">Parent folder.</param>
        internal FolderList(Package package, Folder parent)
        {
            Package = package;
            Parent = parent;

            Refresh();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a folder item for a given name.</summary>
        /// <param name="name">Name.</param>
        /// <returns>Solution.</returns>
        public Folder this[string name]
        {
            get
            {
                foreach(Folder i in _Items.Values)
                {
                    if(i.Name == name) return i;
                }

                return null;
            }
        }


        /// <summary>Gets the parent package.</summary>
        public Package Package
        {
            get; private set;
        }


        /// <summary>Gets the parent folder.</summary>
        public Folder Parent
        {
            get; private set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Package.Repository; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a folder with a given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Solution.</returns>
        public Folder ForID(string id)
        {
            return _Items[id];
        }


        /// <summary>Refreshes this instance.</summary>
        public void Refresh()
        {
            _Items = new Dictionary<string, Folder>();

            DdpSection sec = ((Parent == null) ? Repository.Section : Parent.Section);
            
            foreach(DdpSection i in sec.Sections)
            {
                if(!i.Name.StartsWith(Folder.TYPE_ID + ".")) continue;

                _Items.Add(i.Name, new Folder(Package, Parent, i));
            }
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="name">Item name.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public bool Contains(string name)
        {
            foreach(Folder i in _Items.Values)
            {
                if(i.Name == name) return true;
            }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IReadOnlyList<Folder>                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Element.</returns>
        Folder IReadOnlyList<Folder>.this[int i]
        {
            get { return _Items.Values.ElementAt(i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableList<Folder>                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the number of items in this collection.</summary>
        public int Count
        {
            get { return _Items.Count; }
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        bool IImmutableList<Folder>.Contains(Folder item)
        {
            return _Items.Values.Contains(item);
        }


        /// <summary>Gets the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        int IImmutableList<Folder>.IndexOf(Folder item)
        {
            return _Items.Values.GetIndex(item);
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<Folder>                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator</returns>
        IEnumerator<Folder> IEnumerable<Folder>.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
