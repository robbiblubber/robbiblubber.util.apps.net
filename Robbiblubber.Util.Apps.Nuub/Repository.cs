﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Robbiblubber.Util;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements a Nuub repository.</summary>
    public sealed class Repository: IItem, IExecutable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /** Type ID. */
        public const string TYPE_ID = "NuR";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Repository(string file)
        {
            File = Path.GetFullPath(file);
            Refresh();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads a repository.</summary>
        /// <param name="file">File name.</param>
        /// <returns>Repository.</returns>
        public static Repository Load(string file)
        {
            Repository rval = new Repository(file);

            DdpFile cfg = Ddp.Load(PathOp.UserSettingsFile);
            cfg.SetString("/settings/repository", file);
            cfg.Save();

            return rval;
        }


        /// <summary>Creates and loads a repository.</summary>
        /// <param name="file">File name.</param>
        /// <returns>Repsoitory.</returns>
        public static Repository Create(string file)
        {
            if(!Directory.Exists(Path.GetDirectoryName(file))) { Directory.CreateDirectory(Path.GetDirectoryName(file)); }
            if(System.IO.File.Exists(file)) { System.IO.File.Delete(file); }

            return Load(file);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the repository file name.</summary>
        public string File
        {
            get; private set;
        }


        /// <summary>Gets the solutions in this repository.</summary>
        public SolutionList Solutions
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes this instance.</summary>
        public void Refresh()
        {
            Ddp ddp = Ddp.Load(File);

            Name = ddp.GetString("Name");
            Description = ddp.GetString("Description");

            Solutions = new SolutionList(this);
        }


        /// <summary>Adds a new solution to the repository.</summary>
        /// <returns>Solution.</returns>
        public Solution AddSolution()
        {
            return new Solution(this);
        }


        /// <summary>Saves the repository details.</summary>
        public void Save()
        {
            Save(null);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item ID.</summary>
        string IItem.ID
        {
            get { return File; }
        }


        /// <summary>Gets or sets the repository name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the repository description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the ddp section that contains this item.</summary>
        public DdpSection Section 
        { 
            get { return Ddp.Load(File); }
        }


        /// <summary>Saves the item.</summary>
        /// <param name="sec">Parent section.</param>
        public void Save(DdpSection sec)
        {
            Ddp ddp = new Ddp();
            ddp.SetString("Name", Name);
            ddp.SetString("Description", Description);
            ddp.SetString("ApplicationType", "Nuub");
            ddp.SetString("Type", "Repository");
            ddp.SetString("Version", VersionOp.ApplicationVersion.ToVersionString());

            foreach(Solution i in Solutions) { i.Save(ddp); }

            ddp.Save(File);
        }


        /// <summary>Deletes the item.</summary>
        void IItem.Delete()
        {
            throw new InvalidOperationException("Repository cannot be deleted.");
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        IItem IItem.CopyTo(IItem target)
        {
            return null;
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        IItem IItem.MoveTo(IItem target)
        {
            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an array of all packages in this repository.</summary>
        /// <returns>Packages array.</returns>
        internal Package[] _GetPackages()
        {
            List<Package> rval = new List<Package>();

            foreach(Solution i in Solutions)
            {
                rval.AddRange(i.Packages);
            }

            return rval.OrderBy(m => m.Name).ToArray();
        }


        /// <summary>Gets an object by its ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Returns the object with the given ID, returns NULL when no object with this ID has been found.</returns>
        internal object _ObjectByID(string id)
        {
            return _ObjectByID(this, id);
        }


        /// <summary>Searches the object hierarchy for an ID.</summary>
        /// <param name="obj">Root object.</param>
        /// <param name="id">ID.</param>
        /// <returns>Returns the object with the given ID, returns NULL when no object with this ID has been found.</returns>
        private object _ObjectByID(object obj, string id)
        {
            object rval = null;

            if(obj is Repository)
            {
                if(id == File) return this;

                foreach(Solution i in ((Repository) obj).Solutions)
                {
                    if((rval = _ObjectByID(i, id)) != null) { return rval; }
                }
            }
            else if(obj is Solution)
            {
                if(((Solution) obj).ID == id) { return obj; }

                foreach(Package i in ((Solution) obj).Packages)
                {
                    if((rval = _ObjectByID(i, id)) != null) { return rval; }
                }
            }
            else if(obj is Package)
            {
                if(((Package) obj).ID == id) { return obj; }

                foreach(Folder i in ((Package) obj).Folders)
                {
                    if((rval = _ObjectByID(i, id)) != null) { return rval; }
                }
            }
            else if(obj is Folder)
            {
                if(((Folder) obj).ID == id) { return obj; }

                foreach(Folder i in ((Folder) obj).Folders)
                {
                    if((rval = _ObjectByID(i, id)) != null) { return rval; }
                }

                foreach(CopyJob i in ((Folder) obj).Jobs)
                {
                    if(i.ID == id) return i;
                }
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExecutable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Builds this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Build(ExecutionMetadata meta)
        {
            foreach(Solution i in Solutions) { i.Build(meta); }
        }


        /// <summary>Deploys this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Deploy(ExecutionMetadata meta)
        {
            foreach(Solution i in Solutions) { i.Deploy(meta); }
        }
    }
}
