﻿namespace Robbiblubber.Util.Apps.Nuub
{
    partial class PkgDependencyControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PkgDependencyControl));
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._ButtonReload = new System.Windows.Forms.Button();
            this._ButtonSave = new System.Windows.Forms.Button();
            this._LabelFiles = new System.Windows.Forms.Label();
            this._CboTarget = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(49, 36);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "nuub::udiag.name";
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(52, 52);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(645, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _TextDescription
            // 
            this._TextDescription.BackColor = System.Drawing.SystemColors.Info;
            this._TextDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDescription.Location = new System.Drawing.Point(52, 166);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.Size = new System.Drawing.Size(645, 93);
            this._TextDescription.TabIndex = 2;
            this._TextDescription.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescription.Location = new System.Drawing.Point(49, 150);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(69, 13);
            this._LabelDescription.TabIndex = 2;
            this._LabelDescription.Tag = "nuub::udiag.descr";
            this._LabelDescription.Text = "&Description:";
            // 
            // _ButtonReload
            // 
            this._ButtonReload.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._ButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonReload.Image")));
            this._ButtonReload.Location = new System.Drawing.Point(9, 50);
            this._ButtonReload.Name = "_ButtonReload";
            this._ButtonReload.Size = new System.Drawing.Size(24, 24);
            this._ButtonReload.TabIndex = 4;
            this._ButtonReload.TabStop = false;
            this._ButtonReload.Tag = "||nuub::udiag.tbutton.undo";
            this._ButtonReload.UseVisualStyleBackColor = true;
            this._ButtonReload.Click += new System.EventHandler(this._ButtonReload_Click);
            // 
            // _ButtonSave
            // 
            this._ButtonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._ButtonSave.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonSave.Image")));
            this._ButtonSave.Location = new System.Drawing.Point(9, 20);
            this._ButtonSave.Name = "_ButtonSave";
            this._ButtonSave.Size = new System.Drawing.Size(24, 24);
            this._ButtonSave.TabIndex = 3;
            this._ButtonSave.TabStop = false;
            this._ButtonSave.Tag = "||nuub::udiag.tbutton.save";
            this._ButtonSave.UseVisualStyleBackColor = true;
            this._ButtonSave.Click += new System.EventHandler(this._ButtonSave_Click);
            // 
            // _LabelFiles
            // 
            this._LabelFiles.AutoSize = true;
            this._LabelFiles.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelFiles.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelFiles.Location = new System.Drawing.Point(49, 88);
            this._LabelFiles.Name = "_LabelFiles";
            this._LabelFiles.Size = new System.Drawing.Size(86, 13);
            this._LabelFiles.TabIndex = 1;
            this._LabelFiles.Tag = "nuub::udiag.target";
            this._LabelFiles.Text = "&Target Package:";
            // 
            // _CboTarget
            // 
            this._CboTarget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CboTarget.FormattingEnabled = true;
            this._CboTarget.Location = new System.Drawing.Point(52, 104);
            this._CboTarget.Name = "_CboTarget";
            this._CboTarget.Size = new System.Drawing.Size(645, 25);
            this._CboTarget.TabIndex = 1;
            this._CboTarget.SelectedIndexChanged += new System.EventHandler(this._Changed);
            // 
            // PkgDependencyControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._CboTarget);
            this.Controls.Add(this._LabelFiles);
            this.Controls.Add(this._ButtonReload);
            this.Controls.Add(this._ButtonSave);
            this.Controls.Add(this._TextDescription);
            this.Controls.Add(this._LabelDescription);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "PkgDependencyControl";
            this.Size = new System.Drawing.Size(722, 485);
            this.Resize += new System.EventHandler(this._Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.Button _ButtonSave;
        private System.Windows.Forms.Button _ButtonReload;
        private System.Windows.Forms.Label _LabelFiles;
        private System.Windows.Forms.ComboBox _CboTarget;
    }
}
