﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements the settings window.</summary>
    public sealed partial class FormSettings: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormSettings()
        {
            InitializeComponent();

            this.RestoreLayout();
            this.Localize();

            if(Locale.Locales.Count == 0)
            {
                _LabelLanguage.Visible = _LcboLanguage.Visible = false;
            }

            _TextNuget.Text = Program._Settings.NuGetExecutable;
            _CheckAutoSave.Checked = Program._Settings.AutoSave;
            _CheckSaveKey.Checked = Program._Settings.SaveAPIKey;

            _ChkDebug.Checked = Program._Main.__ShowDebug;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            if(_LcboLanguage.Value != Locale.Selected)
            {
                Locale.Selected = _LcboLanguage.Value;
                Locale.SaveSelection("Robbiblubber.Util.NET");

                Program._Main.Localize();
            }
            
            Program._Main.__ShowDebug = _ChkDebug.Checked;

            Program._Settings.NuGetExecutable = _TextNuget.Text;
            Program._Settings.AutoSave = _CheckAutoSave.Checked;
            Program._Settings.SaveAPIKey = _CheckSaveKey.Checked;
        }


        /// <summary>Button "Browse" click.</summary>
        private void _ButtonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "nuub::udiag.filter.nuger".Localize("NuGet Executable") + " (nuget.exe)|nuget.exe|" + "nuub::udiag.filter.all".Localize("All Files") + "|*.*";

            if(f.ShowDialog() == DialogResult.OK)
            {
                _TextNuget.Text = f.FileName;
            }
        }
    }
}
