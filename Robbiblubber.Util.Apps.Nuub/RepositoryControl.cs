﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements the repository control.</summary>
    public sealed partial class RepositoryControl: UserControl, IControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Tree node.</summary>
        private TreeNode _Node;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public RepositoryControl()
        {
            InitializeComponent();
        }

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IControl                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set
            {
                _Node = value;
                if(value == null) return;

                Reload();
            }
        }


        /// <summary>Gets if the contents have been modified.</summary>
        public bool Modified
        {
            get
            {
                if(_TextName.Text != Program._Repository.Name) return true;
                if(_TextDescription.Text != Program._Repository.Description) return true;

                return false;
            }
        }


        /// <summary>Saves the contents.</summary>
        /// <param name="name">New name.</param>
        /// <returns>Returns TRUE if the object has been saved, FALSE if an error has occured.</returns>
        public bool Save(string name = null)
        {
            if(name != null) { _TextName.Text = name; }

            Program._Repository.Name = _TextName.Text;
            Program._Repository.Description = _TextDescription.Text;

            Program._Repository.Save();

            if(name == null) { _Node.Text = Program._Repository.Name; }
            _ButtonSave.Enabled = false;

            return true;
        }


        /// <summary>Refreshes the contents.</summary>
        public void Reload()
        {
            _TextName.Text = Program._Repository.Name;
            _TextDescription.Text = Program._Repository.Description;

            _Node.Text = Program._Repository.Name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Resize.</summary>
        private void _Resize(object sender, EventArgs e)
        {
            _TextName.Width = _TextDescription.Width = (Width - 77);
        }


        /// <summary>Text changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            _ButtonSave.Enabled = Modified;
        }


        /// <summary>Button "Save" click.</summary>
        private void _ButtonSave_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>Button "Reload" click.</summary>
        private void _ButtonReload_Click(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
