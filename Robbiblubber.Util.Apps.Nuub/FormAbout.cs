﻿using System.Windows.Forms;

using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements the about window.</summary>
    internal sealed partial class FormAbout: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormAbout()
        {
            InitializeComponent();

            this.Localize();
            _LabelVersion.Text = "nuub::udiag.about.version".Localize("Version") + ' ' + VersionOp.ApplicationVersion.ToVersionString();
        }
    }
}
