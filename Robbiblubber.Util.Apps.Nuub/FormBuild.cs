﻿using System;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements the build window,</summary>
    public sealed partial class FormBuild: Form, ILogTarget
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Running flag.</summary>
        private bool _Running = false;
        
        /// <summary>Execution metadata.</summary>
        private ExecutionMetadata _Meta;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormBuild()
        {
            InitializeComponent();

            this.RestoreLayout();

            _TextOutputPath.Text = Program._Settings.OutputPath;
            _CheckDeploy.Checked = Program._Settings.DeployOnBuild;

            this.Localize();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the build target.</summary>
        public IItem Target
        {
            get
            {
                if(_RadioRepository.Checked)
                {
                    return Program._Repository;
                }
                else if(_RadioSolution.Checked)
                {
                    if(_CboSolutions.SelectedItem is __ComboItem)
                    {
                        return ((__ComboItem) _CboSolutions.SelectedItem).Item;
                    }
                }
                else if(_RadioPackage.Checked)
                {
                    if(_CboPackages.SelectedItem is __ComboItem)
                    {
                        return ((__ComboItem) _CboPackages.SelectedItem).Item;
                    }
                }

                return null;
            }
            set
            {
                _CboSolutions.Enabled = _CboPackages.Enabled = false;

                if(value is Repository)
                {
                    _RadioRepository.Checked = true;
                }

                _CboSolutions.BeginUpdate();
                _CboSolutions.Items.Clear();
                foreach(Solution i in Program._Repository.Solutions)
                {
                    _CboSolutions.Items.Add(new __ComboItem(i));
                }
                if(value is Solution)
                {
                    _RadioSolution.Checked = true;
                    __ComboItem.Select(_CboSolutions, value);
                    _CboSolutions.Enabled = true;
                }
                else
                {
                    try { _CboSolutions.SelectedIndex = 0; } catch(Exception) {}
                }
                _CboSolutions.EndUpdate();

                _CboPackages.BeginUpdate();
                _CboPackages.Items.Clear();
                foreach(Package i in Program._Repository._GetPackages())
                {
                    _CboPackages.Items.Add(new __ComboItem(i));
                }
                if(value is Package)
                {
                    _RadioPackage.Checked = true;
                    __ComboItem.Select(_CboPackages, value);
                    _CboPackages.Enabled = true;
                }
                else
                {
                    try { _CboPackages.SelectedIndex = 0; } catch(Exception) {}
                }
                _CboSolutions.EndUpdate();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Expands the window.</summary>
        public void Expand()
        {
            _Meta = new ExecutionMetadata(this, _TextOutputPath.Text, true);

            _TextOut.Clear();

            Height = 773;
            _LabelErrors.Visible = true;

            _LabelErrors.Text = _Meta.StatusText;
            _LabelErrors.Visible = true;

            Application.DoEvents();
        }


        /// <summary>Gets if the build process is currently running.</summary>
        public bool Running
        {
            get { return _Running; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ILogTarget                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Appends a log entry.</summary>
        /// <param name="severity">Severity.</param>
        /// <param name="text">Text.</param>
        public void AppendLog(LogSeverity severity, string text)
        {
            text += "\r\n";

            _TextOut.AppendText(text);
            Application.DoEvents();

            try { File.AppendAllText(_Meta.LogFile, text); } catch(Exception) {}
            Application.DoEvents();

            if(severity == LogSeverity.WARNING)
            {
                _Meta.WarningCount++;
            }
            else if(severity == LogSeverity.ERROR)
            {
                _Meta.ErrorCount++;
            }

            _LabelErrors.Text = _Meta.StatusText;
            Application.DoEvents();
        }

        /// <summary>Appends a log entry.</summary>
        /// <param name="text">Text.</param>
        public void AppendLog(string text)
        {
            AppendLog(LogSeverity.INFORMATION, text);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Radio button check changed.</summary>
        private void _Check(object sender, EventArgs e)
        {
            _CboSolutions.Enabled = _RadioSolution.Checked;
            _CboPackages.Enabled = _RadioPackage.Checked;
        }


        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Form closing.</summary>
        private void FormBuild_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(_Running)
            {
                e.Cancel = true;
                return;
            }

            Program._Settings.OutputPath = _TextOutputPath.Text;
            Program._Settings.DeployOnBuild = _CheckDeploy.Checked;

            Program._Build = null;

            this.SaveLayout();
        }


        /// <summary>Button "Build" click.</summary>
        private void _ButtonBuild_Click(object sender, EventArgs e)
        {
            Expand();

            Enabled = false;
            _Running = true;
            Cursor = Cursors.WaitCursor;

            try
            {
                ((IExecutable) Target).Build(_Meta);
            }
            catch(Exception)
            {
                MessageBox.Show("nuub::udiag.build.fail".Localize("Failed to build."), "nuub::udiag.build.caption".Localize("Build"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            bool deploy = _CheckDeploy.Checked;

            if(deploy)
            {
                if(_Meta.ErrorCount > 0)
                {
                    deploy = (MessageBox.Show("nuub::udiag.build.deployerr".Localize("Build process has completed with errors. Do you want to deploy anyway?"), "nuub::udiag.build.deploycap".Localize("Deploy"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes);
                }
                else if(_Meta.WarningCount > 0)
                {
                    deploy = (MessageBox.Show("nuub::udiag.build.deploywarn".Localize("Build process has completed with warnings. Do you want to deploy anyway?"), "nuub::udiag.build.deploycap".Localize("Deploy"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes);
                }
            }

            if(deploy && string.IsNullOrWhiteSpace(Program._Settings.APIKey))
            {
                FormAPIKey f = new FormAPIKey();
                deploy = (f.ShowDialog() == DialogResult.OK);
            }

            if(deploy)
            {
                try
                {
                    ((IExecutable) Target).Deploy(_Meta);
                }
                catch(Exception)
                {
                    MessageBox.Show("nuub::udiag.deploy.fail".Localize("Failed to deploy package."), "nuub::udiag.deploy.caption".Localize("Deploy"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

            Enabled = true;
            _Running = false;
            Cursor = Cursors.Default;
        }


        /// <summary>Button "Browse" click.</summary>
        private void _ButtonBrowse_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog f = new FolderBrowserDialog();
            f.SelectedPath = _TextOutputPath.Text;

            if(f.ShowDialog() == DialogResult.OK)
            {
                _TextOutputPath.Text = f.SelectedPath;
            }
        }
    }
}
