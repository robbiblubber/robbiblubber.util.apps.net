﻿using System;
using System.Data;
using System.IO;
using System.Xml;
using System.Threading;
using System.Text;
using System.Diagnostics;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements a Nuub package.</summary>
    public sealed class Package: IItem, IExecutable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /** Type ID. */
        public const string TYPE_ID = "NuP";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="sec">Section.</param>
        public Package(Solution solution, DdpSection sec)
        {
            Solution = solution;

            ID = sec.Name;
            Name = sec.GetString("Name");
            Description = sec.GetString("Description");

            Version = (string.IsNullOrWhiteSpace(sec.GetString("Version")) ? null : new Version(sec.GetString("Version")));

            Authors = sec.GetString("Authors");
            Owners = sec.GetString("Owners");
            Copyright = sec.GetString("Copyright");
            License = sec.GetString("License");

            ProjectURL = sec.GetString("ProjectURL");
            RepositoryURL = sec.GetString("RepositoryURL");
            RepositoryType = sec.GetString("RepositoryType");

            Tags = sec.GetString("Tags");
            Icon = sec.GetString("Icon");
            ReleaseNotes = sec.GetString("ReleaseNotes");

            Folders = new FolderList(this, null);
            Dependencies = new DependencyGroupList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="solution">Solution</param>
        public Package(Solution solution)
        {
            Solution = solution;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";

            Version = null;

            Authors = Owners = Copyright = License = "";
            ProjectURL = RepositoryURL = RepositoryType = "";
            Tags = Icon = ReleaseNotes = "";

            Folders = new FolderList(this, null);
            Dependencies = new DependencyGroupList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="template">Template package.</param>
        /// <param name="solution">Repository</param>
        public Package(Solution solution, Package template)
        {
            Solution = solution;

            ID = null;
            Name = template.Name;
            Description = template.Description;

            Version = ((template.Version == null) ? null : new Version(template.Version.ToVersionString()));

            Authors = template.Authors;
            Owners = template.Owners;
            Copyright = template.Copyright;
            License = template.License;

            ProjectURL = template.ProjectURL;
            RepositoryURL = template.RepositoryURL;
            RepositoryType = template.RepositoryType;

            Tags = template.Tags;
            Icon = template.Icon;
            ReleaseNotes = template.ReleaseNotes;

            Folders = new FolderList(this, null);
            Dependencies = new DependencyGroupList(this);

            foreach(Folder i in template.Folders) { new Folder(this, null, i); }
            foreach(DependencyGroup i in template.Dependencies) { new DependencyGroup(this, i); }
        }
        
        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the solution version.</summary>
        public Version Version
        {
            get; set;
        }


        /// <summary>Gets or sets the solution authors.</summary>
        public string Authors
        {
            get; set;
        }


        /// <summary>Gets or sets the solution owners.</summary>
        public string Owners
        {
            get; set;
        }


        /// <summary>Gets or sets the solution authors.</summary>
        public string Copyright
        {
            get; set;
        }


        /// <summary>Gets or sets the solution license URL.</summary>
        public string License
        {
            get; set;
        }


        /// <summary>Gets or sets the solution project URL.</summary>
        public string ProjectURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution icon URL.</summary>
        public string Icon
        {
            get; set;
        }


        /// <summary>Gets or sets the solution tags.</summary>
        public string Tags
        {
            get; set;
        }


        /// <summary>Gets or sets the solution release notes.</summary>
        public string ReleaseNotes
        {
            get; set;
        }


        /// <summary>Gets or sets the solution repository URL.</summary>
        public string RepositoryURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution repository type.</summary>
        public string RepositoryType
        {
            get; set;
        }


        /// <summary>Gets the parent solution.</summary>
        public Solution Solution
        {
            get; set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Solution.Repository; }
        }


        /// <summary>Gets the folders for this package.</summary>
        public FolderList Folders
        {
            get; private set;
        }


        /// <summary>Gets the package dependencies.</summary>
        public DependencyGroupList Dependencies
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a new folder to the package.</summary>
        /// <returns>Folder.</returns>
        public Folder AddFolder()
        {
            return new Folder(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a NuGet package.</summary>
        /// <param name="meta"></param>
        private void _CreatePackage(ExecutionMetadata meta)
        {
            try
            {
                Process p = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = Program._Settings.NuGetExecutable,
                        Arguments = "pack " + meta.OutputDirectory + @"\temp\" + Name + ".nuspec -OutputDirectory " + meta.OutputDirectory,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        CreateNoWindow = true
                    }
                };

                p.Start();

                while(!p.StandardOutput.EndOfStream)
                {
                    string l = p.StandardOutput.ReadLine();

                    if(l.ToLower().StartsWith("error"))
                    {
                        meta.Target.AppendLog(LogSeverity.ERROR, l);
                    }
                    else if(l.ToLower().StartsWith("warning"))
                    {
                        meta.Target.AppendLog(LogSeverity.WARNING, l);
                    }
                    else { meta.Target.AppendLog(LogSeverity.INFORMATION, l); }
                }

                while(!p.StandardError.EndOfStream)
                {
                    meta.Target.AppendLog(LogSeverity.ERROR, p.StandardError.ReadLine());
                }
            }
            catch(Exception ex)
            {
                meta.Target.AppendLog(LogSeverity.ERROR, "nuub::build.packfailed".Localize("Failed to build package: $(1)").Replace("$(1)", ex.Message));
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the solution ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the solution name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the solution description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the ddp section that contains this item.</summary>
        public DdpSection Section
        {
            get { return Solution.Section.GetSection(ID); }
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            Solution.Packages._Items.Remove(ID);
        }


        /// <summary>Saves the item.</summary>
        /// <param name="sec">Parent section.</param>
        public void Save(DdpSection sec)
        {
            if(ID == null)
            {
                ID = (TYPE_ID + "." + StringOp.Unique());
                Solution.Packages._Items.Add(ID, this);
            }

            sec = sec.GetSection(ID);

            sec.SetString("Name", Name);
            sec.SetString("Description", Description);
            sec.SetString("Version", Version.ToVersionString());

            sec.SetString("Authors", Authors);
            sec.SetString("Owners", Owners);
            sec.SetString("Copyright", Copyright);
            sec.SetString("License", License);

            sec.SetString("ProjectURL", ProjectURL);
            sec.SetString("RepositoryURL", RepositoryURL);
            sec.SetString("RepositoryType", RepositoryType);
            
            sec.SetString("Tags", Tags);
            sec.SetString("Icon", Icon);
            sec.SetString("ReleaseNotes", ReleaseNotes);

            foreach(Folder i in Folders) { i.Save(sec); }
            foreach(DependencyGroup i in Dependencies) { i.Save(sec); }
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is Solution)) return null;

            return new Package((Solution) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is Solution)) return null;
            if(((Solution) target).ID == Solution.ID) return null;

            Solution.Packages._Items.Remove(ID);
            Solution.Packages._Items.Add(ID, this);

            return this;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExecutable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Builds this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Build(ExecutionMetadata meta)
        {
            meta.Target.AppendLog("nuub::build.buildingpackage".Localize("Building package \"$(1)\".").Replace("$(1)", Name));

            try
            {
                if(Directory.Exists(meta.OutputDirectory + @"\temp"))
                {
                    Directory.Delete(meta.OutputDirectory + @"\temp", true);
                }

                Thread.Sleep(60);
                Directory.CreateDirectory(meta.OutputDirectory + @"\temp");

                foreach(Folder i in Folders)
                {
                    i.Build(meta.OutputDirectory + @"\temp", meta);
                }
            }
            catch(Exception) { meta.Target.AppendLog(LogSeverity.WARNING, "nuub::build.tmpfailed".Localize("Failed to create temporary directory.")); }

            try
            { 
                XmlTextWriter xml = new XmlTextWriter(meta.OutputDirectory + @"\temp\" + Name + ".nuspec", Encoding.UTF8);
                xml.Formatting = Formatting.Indented;
                xml.Indentation = 3;

                xml.WriteStartDocument();

                xml.WriteStartElement("package");
                xml.WriteStartElement("metadata");

                xml.WriteStartElement("id");
                xml.WriteString(Name);
                xml.WriteEndElement();

                xml.WriteStartElement("version");
                xml.WriteString((Version == null) ? Solution.Version.ToVersionString() : Version.ToVersionString());
                xml.WriteEndElement();

                xml.WriteStartElement("authors");
                xml.WriteString(string.IsNullOrWhiteSpace(Authors) ? Solution.Authors : Authors);
                xml.WriteEndElement();

                xml.WriteStartElement("owners");
                xml.WriteString(string.IsNullOrWhiteSpace(Owners) ? Solution.Owners : Owners);
                xml.WriteEndElement();

                xml.WriteStartElement("requireLicenseAcceptance");
                xml.WriteString("false");
                xml.WriteEndElement();

                xml.WriteStartElement("licenseUrl");
                xml.WriteString(string.IsNullOrWhiteSpace(License) ? Solution.License : License);
                xml.WriteEndElement();

                xml.WriteStartElement("projectUrl");
                xml.WriteString(string.IsNullOrWhiteSpace(ProjectURL) ? Solution.ProjectURL : ProjectURL);
                xml.WriteEndElement();

                xml.WriteStartElement("iconUrl");
                xml.WriteString(string.IsNullOrWhiteSpace(Icon) ? Solution.Icon : Icon);
                xml.WriteEndElement();

                xml.WriteStartElement("description");
                xml.WriteString(string.IsNullOrWhiteSpace(Description) ? Solution.Description : Description);
                xml.WriteEndElement();

                xml.WriteStartElement("releaseNotes");
                xml.WriteString(string.IsNullOrWhiteSpace(ReleaseNotes) ? Solution.ReleaseNotes : ReleaseNotes);
                xml.WriteEndElement();

                xml.WriteStartElement("copyright");
                xml.WriteString(string.IsNullOrWhiteSpace(Copyright) ? Solution.Copyright : Copyright);
                xml.WriteEndElement();

                xml.WriteStartElement("tags");
                xml.WriteString(string.IsNullOrWhiteSpace(Tags) ? Solution.Tags : Tags);
                xml.WriteEndElement();

                xml.WriteStartElement("repository");
                xml.WriteAttributeString("type", string.IsNullOrWhiteSpace(RepositoryType) ? Solution.RepositoryType : RepositoryType);
                xml.WriteAttributeString("url", string.IsNullOrWhiteSpace(RepositoryURL) ? Solution.RepositoryURL : RepositoryURL);
                xml.WriteEndElement();

                // TODO: rebuild with new logic!
                /*
                if((PackageDependencies.Count + NuGetDependencies.Count) > 0)
                {
                    xml.WriteStartElement("dependencies");

                    foreach(PkgDependency i in PackageDependencies)
                    {
                        xml.WriteStartElement("dependency");
                        xml.WriteAttributeString("id", i.Target.Name);
                        xml.WriteAttributeString("version", (i.Target.Version == null) ? i.Target.Solution.Version.ToVersionString() : i.Target.Version.ToVersionString());
                        xml.WriteEndElement();

                        meta.Target.AppendLog("nuub::build.addeddep".Localize("Added dependency for \"$(1)\".").Replace("$(1)", i.Target.Name));
                    }

                    foreach(NuDependency i in NuGetDependencies)
                    {
                        xml.WriteStartElement("dependency");
                        xml.WriteAttributeString("id", i.PackageID);
                        xml.WriteAttributeString("version", i.PackageVersion);
                        xml.WriteEndElement();

                        meta.Target.AppendLog("nuub::build.addeddep".Localize("Added dependency for \"$(1)\".").Replace("$(1)", i.PackageID));
                    }

                    xml.WriteEndElement();
                }

                if(FrameworkDependencies.Count > 0)
                {
                    xml.WriteStartElement("frameworkAssemblies");

                    foreach(FwDependency i in FrameworkDependencies)
                    {
                        xml.WriteStartElement("frameworkAssembly");
                        xml.WriteAttributeString("assemblyName", i.AssemblyName);
                        if(!string.IsNullOrWhiteSpace(i.TargetFramework)) { xml.WriteAttributeString("targetFramework", i.TargetFramework); }
                        xml.WriteEndElement();

                        meta.Target.AppendLog("nuub::build.addeddep".Localize("Added dependency for \"$(1)\".").Replace("$(1)", i.AssemblyName));
                    }

                    xml.WriteEndElement();
                }
                */
                xml.WriteEndElement();
                xml.WriteEndElement();
                xml.WriteEndDocument();

                xml.Close();
            }
            catch(Exception ex)
            {
                meta.Target.AppendLog(LogSeverity.ERROR, "nuub::build.filefailed".Localize("Failed to create .nuspec file: $(1)").Replace("$(1)", ex.Message));
                return;
            }
            
            meta.Target.AppendLog("nuub::build.createdfile".Localize("Created file \"$(1)\".").Replace("$(1)", Name + ".nuspec"));

            _CreatePackage(meta);

            try
            {
                Directory.Delete(meta.OutputDirectory + @"\temp", true);
            }
            catch(Exception) { meta.Target.AppendLog(LogSeverity.WARNING, "nuub::build.rtmpfailed".Localize("Failed to remove temporary directory.")); }
            meta.Target.AppendLog("");
        }


        /// <summary>Deploys this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Deploy(ExecutionMetadata meta)
        {
            if(!File.Exists(meta.OutputDirectory + @"\" + Name + "." + Version + ".nupkg"))
            {
                meta.Target.AppendLog(LogSeverity.ERROR, "nuub::deploy.filefailed".Localize("Unable to deploy. Package \"$(1)\" was not found.").Replace("$(1)", Name + "." + Version + ".nupkg"));
                return;
            }

            meta.Target.AppendLog("nuub::deploy.deployinggpackage".Localize("Deploying package \"$(1)\".").Replace("$(1)", Name));

            try
            {
                Process p = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = Program._Settings.NuGetExecutable,
                        Arguments = "push " + meta.OutputDirectory + @"\" + Name + "." + Version + ".nupkg " + Program._Settings.APIKey + " -Source https://api.nuget.org/v3/index.json",
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };

                p.Start();

                while(!p.StandardOutput.EndOfStream)
                {
                    string l = p.StandardOutput.ReadLine();

                    if(l.ToLower().StartsWith("error"))
                    {
                        meta.Target.AppendLog(LogSeverity.ERROR, l);
                    }
                    else if(l.ToLower().StartsWith("warning"))
                    {
                        meta.Target.AppendLog(LogSeverity.WARNING, l);
                    }
                    else { meta.Target.AppendLog(LogSeverity.INFORMATION, l); }
                }
            }
            catch(Exception ex)
            {
                meta.Target.AppendLog(LogSeverity.ERROR, "nuub::deploy.packfailed".Localize("Failed to deploy package: $(1)").Replace("$(1)", ex.Message));
            }
        }
    }
}
