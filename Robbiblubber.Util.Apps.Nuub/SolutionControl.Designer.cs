﻿namespace Robbiblubber.Util.Apps.Nuub
{
    partial class SolutionControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SolutionControl));
            this._LabelName = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._TextVersion = new System.Windows.Forms.TextBox();
            this._LabelVersion = new System.Windows.Forms.Label();
            this._ButtonReload = new System.Windows.Forms.Button();
            this._ButtonSave = new System.Windows.Forms.Button();
            this._TabPages = new System.Windows.Forms.TabControl();
            this._TabAuthors = new System.Windows.Forms.TabPage();
            this._TextLicense = new System.Windows.Forms.TextBox();
            this._LabelLicense = new System.Windows.Forms.Label();
            this._TextCopyright = new System.Windows.Forms.TextBox();
            this._LabelCopyrigt = new System.Windows.Forms.Label();
            this._TextOwners = new System.Windows.Forms.TextBox();
            this._LabelOwners = new System.Windows.Forms.Label();
            this._TextAuthors = new System.Windows.Forms.TextBox();
            this._LabelAuthors = new System.Windows.Forms.Label();
            this._TabProject = new System.Windows.Forms.TabPage();
            this._TextRepositoryType = new System.Windows.Forms.TextBox();
            this._LabelRepositoryType = new System.Windows.Forms.Label();
            this._TextRepositoryURL = new System.Windows.Forms.TextBox();
            this._LabelRepositoryuURL = new System.Windows.Forms.Label();
            this._TextProjectURL = new System.Windows.Forms.TextBox();
            this._LabelProjectURL = new System.Windows.Forms.Label();
            this._TabRelease = new System.Windows.Forms.TabPage();
            this._TextRelNotes = new System.Windows.Forms.TextBox();
            this._LabelRelNotes = new System.Windows.Forms.Label();
            this._TextIcon = new System.Windows.Forms.TextBox();
            this._LabelIcon = new System.Windows.Forms.Label();
            this._TextTags = new System.Windows.Forms.TextBox();
            this._LabelTags = new System.Windows.Forms.Label();
            this._TabDescription = new System.Windows.Forms.TabPage();
            this._TabPages.SuspendLayout();
            this._TabAuthors.SuspendLayout();
            this._TabProject.SuspendLayout();
            this._TabRelease.SuspendLayout();
            this._TabDescription.SuspendLayout();
            this.SuspendLayout();
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelName.Location = new System.Drawing.Point(49, 36);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(39, 13);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "nuub::udiag.name";
            this._LabelName.Text = "&Name:";
            // 
            // _TextName
            // 
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(52, 52);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(645, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _TextDescription
            // 
            this._TextDescription.BackColor = System.Drawing.SystemColors.Window;
            this._TextDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDescription.Location = new System.Drawing.Point(22, 34);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.Size = new System.Drawing.Size(591, 214);
            this._TextDescription.TabIndex = 12;
            this._TextDescription.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelDescription.Location = new System.Drawing.Point(19, 18);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(69, 13);
            this._LabelDescription.TabIndex = 12;
            this._LabelDescription.Tag = "nuub::udiag.descr";
            this._LabelDescription.Text = "&Description:";
            // 
            // _TextVersion
            // 
            this._TextVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextVersion.Location = new System.Drawing.Point(52, 102);
            this._TextVersion.Name = "_TextVersion";
            this._TextVersion.Size = new System.Drawing.Size(226, 25);
            this._TextVersion.TabIndex = 1;
            this._TextVersion.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelVersion
            // 
            this._LabelVersion.AutoSize = true;
            this._LabelVersion.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelVersion.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelVersion.Location = new System.Drawing.Point(49, 86);
            this._LabelVersion.Name = "_LabelVersion";
            this._LabelVersion.Size = new System.Drawing.Size(48, 13);
            this._LabelVersion.TabIndex = 1;
            this._LabelVersion.Tag = "nuub::udiag.version";
            this._LabelVersion.Text = "&Version:";
            // 
            // _ButtonReload
            // 
            this._ButtonReload.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._ButtonReload.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonReload.Image")));
            this._ButtonReload.Location = new System.Drawing.Point(9, 50);
            this._ButtonReload.Name = "_ButtonReload";
            this._ButtonReload.Size = new System.Drawing.Size(24, 24);
            this._ButtonReload.TabIndex = 14;
            this._ButtonReload.TabStop = false;
            this._ButtonReload.Tag = "||nuub::udiag.tbutton.undo";
            this._ButtonReload.UseVisualStyleBackColor = true;
            this._ButtonReload.Click += new System.EventHandler(this._ButtonReload_Click);
            // 
            // _ButtonSave
            // 
            this._ButtonSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._ButtonSave.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonSave.Image")));
            this._ButtonSave.Location = new System.Drawing.Point(9, 20);
            this._ButtonSave.Name = "_ButtonSave";
            this._ButtonSave.Size = new System.Drawing.Size(24, 24);
            this._ButtonSave.TabIndex = 13;
            this._ButtonSave.TabStop = false;
            this._ButtonSave.Tag = "||nuub::udiag.tbutton.save";
            this._ButtonSave.UseVisualStyleBackColor = true;
            this._ButtonSave.Click += new System.EventHandler(this._ButtonSave_Click);
            // 
            // _TabPages
            // 
            this._TabPages.Controls.Add(this._TabAuthors);
            this._TabPages.Controls.Add(this._TabProject);
            this._TabPages.Controls.Add(this._TabRelease);
            this._TabPages.Controls.Add(this._TabDescription);
            this._TabPages.Location = new System.Drawing.Point(52, 160);
            this._TabPages.Name = "_TabPages";
            this._TabPages.SelectedIndex = 0;
            this._TabPages.Size = new System.Drawing.Size(645, 298);
            this._TabPages.TabIndex = 2;
            this._TabPages.Tag = "";
            // 
            // _TabAuthors
            // 
            this._TabAuthors.BackColor = System.Drawing.SystemColors.Control;
            this._TabAuthors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TabAuthors.Controls.Add(this._TextLicense);
            this._TabAuthors.Controls.Add(this._LabelLicense);
            this._TabAuthors.Controls.Add(this._TextCopyright);
            this._TabAuthors.Controls.Add(this._LabelCopyrigt);
            this._TabAuthors.Controls.Add(this._TextOwners);
            this._TabAuthors.Controls.Add(this._LabelOwners);
            this._TabAuthors.Controls.Add(this._TextAuthors);
            this._TabAuthors.Controls.Add(this._LabelAuthors);
            this._TabAuthors.Location = new System.Drawing.Point(4, 26);
            this._TabAuthors.Name = "_TabAuthors";
            this._TabAuthors.Padding = new System.Windows.Forms.Padding(3);
            this._TabAuthors.Size = new System.Drawing.Size(637, 268);
            this._TabAuthors.TabIndex = 0;
            this._TabAuthors.Tag = "nuub::udiag.tab.authors";
            this._TabAuthors.Text = "Authors";
            // 
            // _TextLicense
            // 
            this._TextLicense.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLicense.Location = new System.Drawing.Point(21, 185);
            this._TextLicense.Name = "_TextLicense";
            this._TextLicense.Size = new System.Drawing.Size(591, 25);
            this._TextLicense.TabIndex = 5;
            this._TextLicense.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelLicense
            // 
            this._LabelLicense.AutoSize = true;
            this._LabelLicense.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLicense.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLicense.Location = new System.Drawing.Point(18, 169);
            this._LabelLicense.Name = "_LabelLicense";
            this._LabelLicense.Size = new System.Drawing.Size(70, 13);
            this._LabelLicense.TabIndex = 5;
            this._LabelLicense.Tag = "nuub::udiag.license";
            this._LabelLicense.Text = "&License URL:";
            // 
            // _TextCopyright
            // 
            this._TextCopyright.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextCopyright.Location = new System.Drawing.Point(21, 135);
            this._TextCopyright.Name = "_TextCopyright";
            this._TextCopyright.Size = new System.Drawing.Size(591, 25);
            this._TextCopyright.TabIndex = 4;
            this._TextCopyright.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelCopyrigt
            // 
            this._LabelCopyrigt.AutoSize = true;
            this._LabelCopyrigt.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelCopyrigt.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelCopyrigt.Location = new System.Drawing.Point(18, 119);
            this._LabelCopyrigt.Name = "_LabelCopyrigt";
            this._LabelCopyrigt.Size = new System.Drawing.Size(61, 13);
            this._LabelCopyrigt.TabIndex = 4;
            this._LabelCopyrigt.Tag = "nuub::udiag.copyright";
            this._LabelCopyrigt.Text = "&Copyright:";
            // 
            // _TextOwners
            // 
            this._TextOwners.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextOwners.Location = new System.Drawing.Point(21, 85);
            this._TextOwners.Name = "_TextOwners";
            this._TextOwners.Size = new System.Drawing.Size(591, 25);
            this._TextOwners.TabIndex = 3;
            this._TextOwners.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelOwners
            // 
            this._LabelOwners.AutoSize = true;
            this._LabelOwners.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelOwners.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelOwners.Location = new System.Drawing.Point(18, 69);
            this._LabelOwners.Name = "_LabelOwners";
            this._LabelOwners.Size = new System.Drawing.Size(50, 13);
            this._LabelOwners.TabIndex = 3;
            this._LabelOwners.Tag = "nuub::udiag.owners";
            this._LabelOwners.Text = "&Owners:";
            // 
            // _TextAuthors
            // 
            this._TextAuthors.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextAuthors.Location = new System.Drawing.Point(21, 35);
            this._TextAuthors.Name = "_TextAuthors";
            this._TextAuthors.Size = new System.Drawing.Size(591, 25);
            this._TextAuthors.TabIndex = 2;
            this._TextAuthors.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelAuthors
            // 
            this._LabelAuthors.AutoSize = true;
            this._LabelAuthors.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelAuthors.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelAuthors.Location = new System.Drawing.Point(18, 19);
            this._LabelAuthors.Name = "_LabelAuthors";
            this._LabelAuthors.Size = new System.Drawing.Size(51, 13);
            this._LabelAuthors.TabIndex = 2;
            this._LabelAuthors.Tag = "nuub::udiag.authors";
            this._LabelAuthors.Text = "&Authors:";
            // 
            // _TabProject
            // 
            this._TabProject.BackColor = System.Drawing.SystemColors.Control;
            this._TabProject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TabProject.Controls.Add(this._TextRepositoryType);
            this._TabProject.Controls.Add(this._LabelRepositoryType);
            this._TabProject.Controls.Add(this._TextRepositoryURL);
            this._TabProject.Controls.Add(this._LabelRepositoryuURL);
            this._TabProject.Controls.Add(this._TextProjectURL);
            this._TabProject.Controls.Add(this._LabelProjectURL);
            this._TabProject.Location = new System.Drawing.Point(4, 26);
            this._TabProject.Name = "_TabProject";
            this._TabProject.Padding = new System.Windows.Forms.Padding(3);
            this._TabProject.Size = new System.Drawing.Size(637, 268);
            this._TabProject.TabIndex = 1;
            this._TabProject.Tag = "nuub::udiag.tab.proj";
            this._TabProject.Text = "Project";
            // 
            // _TextRepositoryType
            // 
            this._TextRepositoryType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextRepositoryType.Location = new System.Drawing.Point(21, 135);
            this._TextRepositoryType.Name = "_TextRepositoryType";
            this._TextRepositoryType.Size = new System.Drawing.Size(591, 25);
            this._TextRepositoryType.TabIndex = 8;
            this._TextRepositoryType.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelRepositoryType
            // 
            this._LabelRepositoryType.AutoSize = true;
            this._LabelRepositoryType.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelRepositoryType.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelRepositoryType.Location = new System.Drawing.Point(18, 119);
            this._LabelRepositoryType.Name = "_LabelRepositoryType";
            this._LabelRepositoryType.Size = new System.Drawing.Size(90, 13);
            this._LabelRepositoryType.TabIndex = 8;
            this._LabelRepositoryType.Tag = "nuub::udiag.reptype";
            this._LabelRepositoryType.Text = "Repository &Type:";
            // 
            // _TextRepositoryURL
            // 
            this._TextRepositoryURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextRepositoryURL.Location = new System.Drawing.Point(21, 85);
            this._TextRepositoryURL.Name = "_TextRepositoryURL";
            this._TextRepositoryURL.Size = new System.Drawing.Size(591, 25);
            this._TextRepositoryURL.TabIndex = 7;
            this._TextRepositoryURL.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelRepositoryuURL
            // 
            this._LabelRepositoryuURL.AutoSize = true;
            this._LabelRepositoryuURL.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelRepositoryuURL.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelRepositoryuURL.Location = new System.Drawing.Point(18, 69);
            this._LabelRepositoryuURL.Name = "_LabelRepositoryuURL";
            this._LabelRepositoryuURL.Size = new System.Drawing.Size(88, 13);
            this._LabelRepositoryuURL.TabIndex = 7;
            this._LabelRepositoryuURL.Tag = "nuub::udiag.repurl";
            this._LabelRepositoryuURL.Text = "&Repository URL:";
            // 
            // _TextProjectURL
            // 
            this._TextProjectURL.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextProjectURL.Location = new System.Drawing.Point(21, 35);
            this._TextProjectURL.Name = "_TextProjectURL";
            this._TextProjectURL.Size = new System.Drawing.Size(591, 25);
            this._TextProjectURL.TabIndex = 6;
            this._TextProjectURL.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelProjectURL
            // 
            this._LabelProjectURL.AutoSize = true;
            this._LabelProjectURL.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelProjectURL.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelProjectURL.Location = new System.Drawing.Point(18, 19);
            this._LabelProjectURL.Name = "_LabelProjectURL";
            this._LabelProjectURL.Size = new System.Drawing.Size(68, 13);
            this._LabelProjectURL.TabIndex = 6;
            this._LabelProjectURL.Tag = "nuub::udiag.projurl";
            this._LabelProjectURL.Text = "&Project URL:";
            // 
            // _TabRelease
            // 
            this._TabRelease.BackColor = System.Drawing.SystemColors.Control;
            this._TabRelease.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TabRelease.Controls.Add(this._TextRelNotes);
            this._TabRelease.Controls.Add(this._LabelRelNotes);
            this._TabRelease.Controls.Add(this._TextIcon);
            this._TabRelease.Controls.Add(this._LabelIcon);
            this._TabRelease.Controls.Add(this._TextTags);
            this._TabRelease.Controls.Add(this._LabelTags);
            this._TabRelease.Location = new System.Drawing.Point(4, 26);
            this._TabRelease.Name = "_TabRelease";
            this._TabRelease.Size = new System.Drawing.Size(637, 268);
            this._TabRelease.TabIndex = 2;
            this._TabRelease.Tag = "nuub::udiag.tab.release";
            this._TabRelease.Text = "Release";
            // 
            // _TextRelNotes
            // 
            this._TextRelNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextRelNotes.Location = new System.Drawing.Point(21, 135);
            this._TextRelNotes.Multiline = true;
            this._TextRelNotes.Name = "_TextRelNotes";
            this._TextRelNotes.Size = new System.Drawing.Size(591, 112);
            this._TextRelNotes.TabIndex = 11;
            this._TextRelNotes.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelRelNotes
            // 
            this._LabelRelNotes.AutoSize = true;
            this._LabelRelNotes.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelRelNotes.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelRelNotes.Location = new System.Drawing.Point(18, 119);
            this._LabelRelNotes.Name = "_LabelRelNotes";
            this._LabelRelNotes.Size = new System.Drawing.Size(82, 13);
            this._LabelRelNotes.TabIndex = 11;
            this._LabelRelNotes.Tag = "nuub::udiag.relnotes";
            this._LabelRelNotes.Text = "Release &Notes:";
            // 
            // _TextIcon
            // 
            this._TextIcon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextIcon.Location = new System.Drawing.Point(21, 85);
            this._TextIcon.Name = "_TextIcon";
            this._TextIcon.Size = new System.Drawing.Size(591, 25);
            this._TextIcon.TabIndex = 10;
            this._TextIcon.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelIcon
            // 
            this._LabelIcon.AutoSize = true;
            this._LabelIcon.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelIcon.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelIcon.Location = new System.Drawing.Point(18, 69);
            this._LabelIcon.Name = "_LabelIcon";
            this._LabelIcon.Size = new System.Drawing.Size(55, 13);
            this._LabelIcon.TabIndex = 10;
            this._LabelIcon.Tag = "nuub::udiag.icon";
            this._LabelIcon.Text = "&Icon URL:";
            // 
            // _TextTags
            // 
            this._TextTags.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextTags.Location = new System.Drawing.Point(21, 35);
            this._TextTags.Name = "_TextTags";
            this._TextTags.Size = new System.Drawing.Size(591, 25);
            this._TextTags.TabIndex = 9;
            this._TextTags.TextChanged += new System.EventHandler(this._Changed);
            // 
            // _LabelTags
            // 
            this._LabelTags.AutoSize = true;
            this._LabelTags.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelTags.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelTags.Location = new System.Drawing.Point(18, 19);
            this._LabelTags.Name = "_LabelTags";
            this._LabelTags.Size = new System.Drawing.Size(32, 13);
            this._LabelTags.TabIndex = 9;
            this._LabelTags.Tag = "nuub::udiag.tags";
            this._LabelTags.Text = "&Tags:";
            // 
            // _TabDescription
            // 
            this._TabDescription.BackColor = System.Drawing.SystemColors.Control;
            this._TabDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TabDescription.Controls.Add(this._TextDescription);
            this._TabDescription.Controls.Add(this._LabelDescription);
            this._TabDescription.Location = new System.Drawing.Point(4, 26);
            this._TabDescription.Name = "_TabDescription";
            this._TabDescription.Size = new System.Drawing.Size(637, 268);
            this._TabDescription.TabIndex = 3;
            this._TabDescription.Tag = "nuub::udiag.tab.decr";
            this._TabDescription.Text = "Description";
            // 
            // SolutionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._TabPages);
            this.Controls.Add(this._TextVersion);
            this.Controls.Add(this._LabelVersion);
            this.Controls.Add(this._ButtonReload);
            this.Controls.Add(this._ButtonSave);
            this.Controls.Add(this._TextName);
            this.Controls.Add(this._LabelName);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "SolutionControl";
            this.Size = new System.Drawing.Size(722, 485);
            this.Resize += new System.EventHandler(this._Resize);
            this._TabPages.ResumeLayout(false);
            this._TabAuthors.ResumeLayout(false);
            this._TabAuthors.PerformLayout();
            this._TabProject.ResumeLayout(false);
            this._TabProject.PerformLayout();
            this._TabRelease.ResumeLayout(false);
            this._TabRelease.PerformLayout();
            this._TabDescription.ResumeLayout(false);
            this._TabDescription.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.Button _ButtonSave;
        private System.Windows.Forms.Button _ButtonReload;
        private System.Windows.Forms.TextBox _TextVersion;
        private System.Windows.Forms.Label _LabelVersion;
        private System.Windows.Forms.TabControl _TabPages;
        private System.Windows.Forms.TabPage _TabAuthors;
        private System.Windows.Forms.TextBox _TextLicense;
        private System.Windows.Forms.Label _LabelLicense;
        private System.Windows.Forms.TextBox _TextCopyright;
        private System.Windows.Forms.Label _LabelCopyrigt;
        private System.Windows.Forms.TextBox _TextOwners;
        private System.Windows.Forms.Label _LabelOwners;
        private System.Windows.Forms.TextBox _TextAuthors;
        private System.Windows.Forms.Label _LabelAuthors;
        private System.Windows.Forms.TabPage _TabProject;
        private System.Windows.Forms.TextBox _TextRepositoryType;
        private System.Windows.Forms.Label _LabelRepositoryType;
        private System.Windows.Forms.TextBox _TextRepositoryURL;
        private System.Windows.Forms.Label _LabelRepositoryuURL;
        private System.Windows.Forms.TextBox _TextProjectURL;
        private System.Windows.Forms.Label _LabelProjectURL;
        private System.Windows.Forms.TabPage _TabRelease;
        private System.Windows.Forms.TextBox _TextRelNotes;
        private System.Windows.Forms.Label _LabelRelNotes;
        private System.Windows.Forms.TextBox _TextIcon;
        private System.Windows.Forms.Label _LabelIcon;
        private System.Windows.Forms.TextBox _TextTags;
        private System.Windows.Forms.Label _LabelTags;
        private System.Windows.Forms.TabPage _TabDescription;
    }
}
