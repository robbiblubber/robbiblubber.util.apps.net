﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements a dependency.</summary>
    public sealed class PkgDependency: IItem, IDependency
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /** Type ID. */
        public const string TYPE_ID = "NuB";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Target package ID.</summary>
        internal string _TargetID = null;

        
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="group">Dependency group.</param>
        /// <param name="sec">Section.</param>
        public PkgDependency(DependencyGroup group, DdpSection sec)
        {
            Group = group;

            ID = sec.Name;
            Name = sec.GetString("Name");
            Description = sec.GetString("Description");
            _TargetID = sec.GetString("TargetPackage", null);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="group">Dependency group.</param>
        public PkgDependency(DependencyGroup group)
        {
            Group = group;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="group">Dependency group.</param>
        /// <param name="template">Template.</param>
        public PkgDependency(DependencyGroup group, PkgDependency template)
        {
            Group = group;

            ID = null;
            Name = template.Name;
            Description = template.Description;
            _TargetID = template._TargetID;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Group.Repository; }
        }

        
        /// <summary>Gets the dependency group for this dependency.</summary>
        public DependencyGroup Group
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency target.</summary>
        public Package Target
        {
            get { return (Package) Repository._ObjectByID(_TargetID); }
            set
            {
                if(value == null)
                {
                    _TargetID = null;
                }
                else { _TargetID = value.ID; }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the dependency ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the ddp section that contains this item.</summary>
        public DdpSection Section
        {
            get { return Group.Section.GetSection(ID); }
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            Group.PackageDependencies._Items.Remove(ID);
        }


        /// <summary>Saves the object.</summary>
        /// <param name="sec">Parent section.</param>
        public void Save(DdpSection sec)
        {
            if(ID == null)
            {
                ID = (TYPE_ID + "." + StringOp.Unique());
                Group.PackageDependencies._Items.Add(ID, this);
            }

            sec = sec.Sections.Add(ID);

            sec.SetString("Name", Name);
            sec.SetString("Description", Description);
            if(_TargetID != null) { sec.SetString("TargetPackage", _TargetID); }
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is DependencyGroup)) return null;

            return new PkgDependency((DependencyGroup) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is DependencyGroup)) return null;
            if(((DependencyGroup) target).ID == Group.ID) return null;

            Group.PackageDependencies._Items.Remove(ID);
            (Group = (DependencyGroup) target).PackageDependencies._Items.Add(ID, this);

            return this;
        }
    }
}
