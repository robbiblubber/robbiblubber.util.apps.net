﻿using System;

using Robbiblubber.Util;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class defines system settings.</summary>
    public sealed class Settings
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>NuGet Executable.</summary>
        private string _NuGetExecutable;

        /// <summary>Output path.</summary>
        private string _OutputPath;

        /// <summary>Save API Key flag.</summary>
        private bool _SaveAPIKey;

        /// <summary>NuGet API key.</summary>
        private string _APIKey;

        /// <summary>Deploy on build flag.</summary>
        private bool _DeployOnBuild;

        /// <summary>Auto-save flag.</summary>
        private bool _AutoSave;
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Settings()
        {
            Refresh();
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the NuGet executable file name.</summary>
        public string NuGetExecutable
        {
            get { return _NuGetExecutable; }
            set
            {
                _NuGetExecutable = value;
                _SaveLocals();
            }
        }


        /// <summary>Gets or sets the application output path.</summary>
        public string OutputPath
        {
            get { return _OutputPath; }
            set
            {
                _OutputPath = value;
                _SaveUser();
            }
        }

        /// <summary>Gets or sets if the API Key should be saved.</summary>
        public bool SaveAPIKey
        {
            get { return _SaveAPIKey; }
            set
            {
                _SaveAPIKey = value;
                _SaveUser();
            }
        }


        /// <summary>Gets or sets the NuGet API key.</summary>
        public string APIKey
        {
            get { return _APIKey; }
            set
            {
                _APIKey = value;
                _SaveUser();
            }
        }


        /// <summary>Gets or sets if the build process should include deployment.</summary>
        public bool DeployOnBuild
        {
            get { return _DeployOnBuild; }
            set
            {
                _DeployOnBuild = value;
                _SaveUser();
            }
        }


        /// <summary>Gets or sets if changes should be saved automatically.</summary>
        public bool AutoSave
        {
            get { return _AutoSave; }
            set
            {
                _AutoSave = value;
                _SaveUser();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves local machine settings.</summary>
        private void _SaveLocals()
        {
            DdpFile cfg = Ddp.Load(PathOp.LocalConfigurationPath + @"\application.config");

            cfg.SetString("/paths/nuget", _NuGetExecutable);
            cfg.Save();
        }


        /// <summary>Saves user settings.</summary>
        private void _SaveUser()
        {
            DdpFile cfg = Ddp.Load(PathOp.UserConfigurationPath + @"\application.config");

            cfg.SetBoolean("/nuget/savekey", _SaveAPIKey);
            cfg.SetString("/nuget/key", _SaveAPIKey ? _APIKey : "");

            cfg.SetString("build/output", _OutputPath);
            cfg.SetBoolean("/build/deploy", _DeployOnBuild);

            cfg.SetBoolean("/misc/autosave", _AutoSave);
            cfg.Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes all settings.</summary>
        public void Refresh()
        {
            DdpFile cfg = Ddp.Load(PathOp.LocalConfigurationPath + @"\application.config");

            _NuGetExecutable = cfg.GetString("/paths/nuget", "NuGet");

            cfg = Ddp.Load(PathOp.UserConfigurationPath + @"\application.config");

            _SaveAPIKey = cfg.GetBoolean("nuget/savekey", true);
            _APIKey = cfg.GetString("nuget/key");
            _OutputPath = cfg.GetString("build/output", PathOp.ApplicationDirectory + @"\out");
            _DeployOnBuild = cfg.GetBoolean("build/deploy");
            _AutoSave = cfg.GetBoolean("/misc/autosave");
        }


        /// <summary>Saves all settings.</summary>
        public void Save()
        {
            _SaveLocals();
            _SaveUser();
        }
    }
}
