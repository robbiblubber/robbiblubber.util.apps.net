﻿namespace Robbiblubber.Util.Apps.Nuub
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
            this._ButtonBrowse = new System.Windows.Forms.Button();
            this._LabelNuget = new System.Windows.Forms.Label();
            this._TextNuget = new System.Windows.Forms.TextBox();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._LcboLanguage = new Robbiblubber.Util.Localization.Controls.LocaleComboBox();
            this._LabelLanguage = new System.Windows.Forms.Label();
            this._ChkDebug = new System.Windows.Forms.CheckBox();
            this._CheckSaveKey = new System.Windows.Forms.CheckBox();
            this._CheckAutoSave = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // _ButtonBrowse
            // 
            this._ButtonBrowse.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonBrowse.Image")));
            this._ButtonBrowse.Location = new System.Drawing.Point(539, 50);
            this._ButtonBrowse.Name = "_ButtonBrowse";
            this._ButtonBrowse.Size = new System.Drawing.Size(25, 25);
            this._ButtonBrowse.TabIndex = 1;
            this._ButtonBrowse.TabStop = false;
            this._ButtonBrowse.Tag = "||nuub::udiag.settings.browse";
            this._ToolTip.SetToolTip(this._ButtonBrowse, "Browse...");
            this._ButtonBrowse.UseVisualStyleBackColor = true;
            this._ButtonBrowse.Click += new System.EventHandler(this._ButtonBrowse_Click);
            // 
            // _LabelNuget
            // 
            this._LabelNuget.AutoSize = true;
            this._LabelNuget.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelNuget.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelNuget.Location = new System.Drawing.Point(25, 34);
            this._LabelNuget.Name = "_LabelNuget";
            this._LabelNuget.Size = new System.Drawing.Size(101, 13);
            this._LabelNuget.TabIndex = 0;
            this._LabelNuget.Tag = "nuub::udiag.settings.nuget";
            this._LabelNuget.Text = "NuGet &Executable:";
            // 
            // _TextNuget
            // 
            this._TextNuget.Location = new System.Drawing.Point(28, 50);
            this._TextNuget.Name = "_TextNuget";
            this._TextNuget.Size = new System.Drawing.Size(505, 25);
            this._TextNuget.TabIndex = 0;
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(392, 280);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(141, 29);
            this._ButtonCancel.TabIndex = 7;
            this._ButtonCancel.Tag = "nuub::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(245, 280);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(141, 29);
            this._ButtonOK.TabIndex = 6;
            this._ButtonOK.Tag = "nuub::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _LcboLanguage
            // 
            this._LcboLanguage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LcboLanguage.Location = new System.Drawing.Point(28, 114);
            this._LcboLanguage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LcboLanguage.Name = "_LcboLanguage";
            this._LcboLanguage.Size = new System.Drawing.Size(505, 31);
            this._LcboLanguage.TabIndex = 2;
            this._LcboLanguage.Value = null;
            // 
            // _LabelLanguage
            // 
            this._LabelLanguage.AutoSize = true;
            this._LabelLanguage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLanguage.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLanguage.Location = new System.Drawing.Point(25, 95);
            this._LabelLanguage.Name = "_LabelLanguage";
            this._LabelLanguage.Size = new System.Drawing.Size(135, 13);
            this._LabelLanguage.TabIndex = 2;
            this._LabelLanguage.Tag = "cfgnpd::udiag.settings.language";
            this._LabelLanguage.Text = "User Interface &Language:";
            // 
            // _ChkDebug
            // 
            this._ChkDebug.AutoSize = true;
            this._ChkDebug.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._ChkDebug.Location = new System.Drawing.Point(28, 223);
            this._ChkDebug.Name = "_ChkDebug";
            this._ChkDebug.Size = new System.Drawing.Size(372, 21);
            this._ChkDebug.TabIndex = 5;
            this._ChkDebug.Tag = "debugx::settings.showdebug";
            this._ChkDebug.Text = "   Enable &debug features (show Debug menu in menu strip)";
            this._ChkDebug.UseVisualStyleBackColor = true;
            // 
            // _CheckSaveKey
            // 
            this._CheckSaveKey.AutoSize = true;
            this._CheckSaveKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._CheckSaveKey.Location = new System.Drawing.Point(28, 190);
            this._CheckSaveKey.Name = "_CheckSaveKey";
            this._CheckSaveKey.Size = new System.Drawing.Size(113, 21);
            this._CheckSaveKey.TabIndex = 4;
            this._CheckSaveKey.Tag = "nuub::udiag.settings.savekey";
            this._CheckSaveKey.Text = "   Save API &Key";
            this._CheckSaveKey.UseVisualStyleBackColor = true;
            // 
            // _CheckAutoSave
            // 
            this._CheckAutoSave.AutoSize = true;
            this._CheckAutoSave.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._CheckAutoSave.Location = new System.Drawing.Point(28, 165);
            this._CheckAutoSave.Name = "_CheckAutoSave";
            this._CheckAutoSave.Size = new System.Drawing.Size(270, 21);
            this._CheckAutoSave.TabIndex = 3;
            this._CheckAutoSave.Tag = "nuub::udiag.settings.autosave";
            this._CheckAutoSave.Text = "   Automatically &save changes to elements";
            this._CheckAutoSave.UseVisualStyleBackColor = true;
            // 
            // FormSettings
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(597, 342);
            this.Controls.Add(this._CheckAutoSave);
            this.Controls.Add(this._CheckSaveKey);
            this.Controls.Add(this._LcboLanguage);
            this.Controls.Add(this._LabelLanguage);
            this.Controls.Add(this._ChkDebug);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ButtonBrowse);
            this.Controls.Add(this._LabelNuget);
            this.Controls.Add(this._TextNuget);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSettings";
            this.Tag = "nuub::udiag.settings.caption";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _ButtonBrowse;
        private System.Windows.Forms.Label _LabelNuget;
        private System.Windows.Forms.TextBox _TextNuget;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
        private Util.Localization.Controls.LocaleComboBox _LcboLanguage;
        private System.Windows.Forms.Label _LabelLanguage;
        private System.Windows.Forms.CheckBox _ChkDebug;
        private System.Windows.Forms.CheckBox _CheckSaveKey;
        private System.Windows.Forms.CheckBox _CheckAutoSave;
    }
}