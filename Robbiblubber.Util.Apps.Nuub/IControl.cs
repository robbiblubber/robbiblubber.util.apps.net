﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>Nuub controls implement this interface.</summary>
    public interface IControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the tree node.</summary>
        TreeNode Node { get; set; }


        /// <summary>Gets if the contents have been modified.</summary>
        bool Modified { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the contents.</summary>
        /// <param name="name">New name.</param>
        /// <returns>Returns TRUE if the object has been saved, FALSE if an error has occured.</returns>
        bool Save(string name = null);
        

        /// <summary>Reloads the contents.</summary>
        void Reload();
    }
}
