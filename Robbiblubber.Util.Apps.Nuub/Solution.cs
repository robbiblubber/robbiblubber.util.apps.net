﻿using System;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements a Nuub solution.</summary>
    public sealed class Solution: IItem, IExecutable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /** Type ID. */
        public const string TYPE_ID = "NuS";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Version.</summary>
        private Version _Version;


        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="repository">Repository</param>
        /// <param name="sec">ddp Section.</param>
        public Solution(Repository repository, DdpSection sec)
        {
            Repository = repository;

            ID = sec.Name;
            Name = sec.GetString("Name");
            Description = sec.GetString("Description");

            _Version = new Version(sec.GetString("Version"));

            Authors = sec.GetString("Authors");
            Owners = sec.GetString("Owners");
            Copyright = sec.GetString("Copyright");
            License = sec.GetString("License");

            ProjectURL = sec.GetString("ProjectURL");
            RepositoryURL = sec.GetString("RepositoryURL");
            RepositoryType = sec.GetString("RepositoryType");

            Tags = sec.GetString("Tags");
            Icon = sec.GetString("Icon");
            ReleaseNotes = sec.GetString("ReleaseNotes");

            Packages = new PackageList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="repository">Repository</param>
        public Solution(Repository repository)
        {
            Repository = repository;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";

            _Version = new Version("1.0.0");

            Authors = Owners = Copyright = License = "";
            ProjectURL = RepositoryURL = RepositoryType = "";
            Tags = Icon = ReleaseNotes = "";

            Packages = new PackageList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="template">Template solution.</param>
        /// <param name="repository">Repository</param>
        public Solution(Repository repository, Solution template)
        {
            Repository = repository;

            ID = null;
            Name = template.Name;
            Description = template.Description;

            Version = new Version(template.Version.ToVersionString());

            Authors = template.Authors;
            Owners = template.Owners;
            Copyright = template.Copyright;
            License = template.License;

            ProjectURL = template.ProjectURL;
            RepositoryURL = template.RepositoryURL;
            RepositoryType = template.RepositoryType;

            Tags = template.Tags;
            Icon = template.Icon;
            ReleaseNotes = template.ReleaseNotes;

            Packages = new PackageList(this);

            foreach(Package i in template.Packages) { new Package(this, i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the solution version.</summary>
        public Version Version
        {
            get { return _Version; }
            set
            {
                foreach(Package i in Packages)
                {
                    if(i.Version == _Version)
                    {
                        i.Version = value;
                    }
                }

                _Version = value;
            }
        }


        /// <summary>Gets or sets the solution authors.</summary>
        public string Authors
        {
            get; set;
        }


        /// <summary>Gets or sets the solution owners.</summary>
        public string Owners
        {
            get; set;
        }


        /// <summary>Gets or sets the solution authors.</summary>
        public string Copyright
        {
            get; set;
        }


        /// <summary>Gets or sets the solution license code or file.</summary>
        public string License
        {
            get; set;
        }


        /// <summary>Gets or sets the solution project URL.</summary>
        public string ProjectURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution icon URL.</summary>
        public string Icon
        {
            get; set;
        }


        /// <summary>Gets or sets the solution tags.</summary>
        public string Tags
        {
            get; set;
        }


        /// <summary>Gets or sets the solution release notes.</summary>
        public string ReleaseNotes
        {
            get; set;
        }


        /// <summary>Gets or sets the solution repository URL.</summary>
        public string RepositoryURL
        {
            get; set;
        }


        /// <summary>Gets or sets the solution repository type.</summary>
        public string RepositoryType
        {
            get; set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get; private set;
        }


        /// <summary>Gets the packages for this solution.</summary>
        public PackageList Packages
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a package to the solution.</summary>
        /// <returns>Package.</returns>
        public Package AddPackage()
        {
            return new Package(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the solution ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the solution name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the solution description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the ddp section that contains this item.</summary>
        public DdpSection Section
        {
            get { return Repository.Section.GetSection(ID); }
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            Repository.Solutions._Items.Remove(ID);
        }


        /// <summary>Saves the item.</summary>
        /// <param name="sec">Parent section.</param>
        public void Save(DdpSection sec)
        {
            if(ID == null) 
            { 
                ID = (TYPE_ID + "." + StringOp.Unique());
                Repository.Solutions._Items.Add(ID, this);
            }

            sec = sec.Sections.Add(ID);
            sec.SetString("Name", Name);
            sec.SetString("Description", Description);

            sec.SetString("Version", Version.ToVersionString());

            sec.SetString("Authors", Authors);
            sec.SetString("Owners", Owners);
            sec.SetString("Copyright", Copyright);
            sec.SetString("License", License);

            sec.SetString("ProjectURL", ProjectURL);
            sec.SetString("RepositoryURL", RepositoryURL);
            sec.SetString("RepositoryType", RepositoryType);

            sec.SetString("Tags", Tags);
            sec.SetString("Icon", Icon);
            sec.SetString("ReleaseNotes", ReleaseNotes);

            foreach(Package i in Packages) { i.Save(sec); }
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is Repository)) return null;

            return new Solution((Repository) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be moved.</returns>
        IItem IItem.MoveTo(IItem target)
        {
            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExecutable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Builds this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Build(ExecutionMetadata meta)
        {
            foreach(Package i in Packages) { i.Build(meta); }
        }


        /// <summary>Deploys this item.</summary>
        /// <param name="meta">Execution metadata.</param>
        public void Deploy(ExecutionMetadata meta)
        {
            foreach(Package i in Packages) { i.Deploy(meta); }
        }
    }
}
