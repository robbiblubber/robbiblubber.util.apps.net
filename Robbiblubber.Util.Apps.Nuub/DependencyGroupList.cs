﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Util;
using Robbiblubber.Util.Collections;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class provides a list of NuGet dependencies groups for a package.</summary>
    public sealed class DependencyGroupList: IImmutableList<DependencyGroup>, IEnumerable<DependencyGroup>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Items.</summary>
        internal Dictionary<string, DependencyGroup> _Items = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Package.</param>
        internal DependencyGroupList(Package package)
        {
            Package = package;

            Refresh();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a dependency item for a given name.</summary>
        /// <param name="name">Name.</param>
        /// <returns>Dependency.</returns>
        public DependencyGroup this[string name]
        {
            get
            {
                foreach(DependencyGroup i in _Items.Values)
                {
                    if(i.Name == name) return i;
                }

                return null;
            }
        }


        /// <summary>Gets the parent folder.</summary>
        public Package Package
        {
            get; private set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Package.Repository; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a dependency with a given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Dependency.</returns>
        public DependencyGroup ForID(string id)
        {
            return _Items[id];
        }


        /// <summary>Refreshes this instance.</summary>
        public void Refresh()
        {
            foreach(DdpSection i in Package.Section.Sections)
            {
                if(i.GetString("Type") != "DependencyGroup") continue;

                _Items.Add(i.Name, new DependencyGroup(this, i));
            }
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="name">Item name.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public bool Contains(string name)
        {
            foreach(DependencyGroup i in _Items.Values)
            {
                if(i.Name == name) return true;
            }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IReadOnlyList<NuDependency>                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Element.</returns>
        DependencyGroup IReadOnlyList<DependencyGroup>.this[int i]
        {
            get { return _Items.Values.ElementAt(i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableList<NuDependency>                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the number of items in this collection.</summary>
        public int Count
        {
            get { return _Items.Count; }
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        bool IImmutableList<DependencyGroup>.Contains(DependencyGroup item)
        {
            return _Items.Values.Contains(item);
        }


        /// <summary>Gets the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        int IImmutableList<DependencyGroup>.IndexOf(DependencyGroup item)
        {
            return _Items.Values.GetIndex(item);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<NuDependency>                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator</returns>
        IEnumerator<DependencyGroup> IEnumerable<DependencyGroup>.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
