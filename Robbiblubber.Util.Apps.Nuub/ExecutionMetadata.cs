﻿using System;
using System.IO;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class provides execution metadata.</summary>
    public sealed class ExecutionMetadata
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="target">Log target.</param>
        /// <param name="outputDirectory">Output directory.</param>
        /// <param name="build">Determines if execution is build (or deploy).</param>
        public ExecutionMetadata(ILogTarget target, string outputDirectory, bool build)
        {
            ErrorCount = WarningCount = 0;

            Target = target;
            OutputDirectory = outputDirectory.TrimEnd('\\');

            if(!Directory.Exists(OutputDirectory))
            {
                Directory.CreateDirectory(OutputDirectory);
            }

            LogFile = OutputDirectory + (build ? @"\build.log" : @"\deploy.log");

            try
            {
                if(File.Exists(LogFile)) { File.Delete(LogFile); }
            }
            catch(Exception) { Target.AppendLog(LogSeverity.ERROR, "Failed to remove old log file."); }
            
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the log target.</summary>
        public ILogTarget Target
        {
            get; private set;
        }


        /// <summary>Gets the output directory.</summary>
        public string OutputDirectory
        {
            get; private set;
        }


        /// <summary>Gets the log file.</summary>
        public string LogFile
        {
            get; private set;
        }


        /// <summary>Error counter.</summary>
        public int ErrorCount
        {
            get; set;
        }


        /// <summary>Warning counter.</summary>
        public int WarningCount
        {
            get; set;
        }


        /// <summary>Gets the status text.</summary>
        public string StatusText
        {
            get { return "nuub::udiag.status.text".Localize("$(1) Errors      $(2) Warnings.").Replace("$(1)", ErrorCount.ToString()).Replace("$(2)", WarningCount.ToString()); }
        }
    }
}
