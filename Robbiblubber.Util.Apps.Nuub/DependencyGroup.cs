﻿using System;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class represents a dependency group.</summary>
    public class DependencyGroup: IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /** Type ID. */
        public const string TYPE_ID = "NuD";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="sec">Section.</param>
        public DependencyGroup(Package package, DdpSection sec)
        {
            Package = package;

            ID = sec.Name;
            Name = sec.GetString("Name");
            Description = sec.GetString("Description");
            TargetFramework = sec.GetString("TargetFramework");

            PackageDependencies = new PkgDependencyList(this);
            NuGetDependencies = new NuDependencyList(this);
            FrameworkDependencies = new FwDependencyList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        public DependencyGroup(Package package)
        {
            Package = package;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = TargetFramework = "";

            PackageDependencies = new PkgDependencyList(this);
            NuGetDependencies = new NuDependencyList(this);
            FrameworkDependencies = new FwDependencyList(this);

            Save();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="template">Template.</param>
        public DependencyGroup(Package package, DependencyGroup template)
        {
            Package = package;

            ID = null;
            Name = template.Name;
            Description = template.Description;
            TargetFramework = template.TargetFramework;

            PackageDependencies = new PkgDependencyList(this);
            NuGetDependencies = new NuDependencyList(this);
            FrameworkDependencies = new FwDependencyList(this);

            foreach(PkgDependency i in template.PackageDependencies) { new PkgDependency(this, i); }
            foreach(NuDependency i in template.NuGetDependencies) { new NuDependency(this, i); }
            foreach(FwDependency i in template.FrameworkDependencies) { new FwDependency(this, i); }

            Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Package.Repository; }
        }


        /// <summary>Gets the parent package for this dependency.</summary>
        public Package Package
        {
            get; private set;
        }


        /// <summary>Gets or sets the target framework.</summary>
        public string TargetFramework
        {
            get; set;
        }


        /// <summary>Gets the package dependencies for this package.</summary>
        public PkgDependencyList PackageDependencies
        {
            get; private set;
        }


        /// <summary>Gets the NuGet dependencies for this package.</summary>
        public NuDependencyList NuGetDependencies
        {
            get; private set;
        }


        /// <summary>Gets the framework dependencies for this package.</summary>
        public FwDependencyList FrameworkDependencies
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a new package dependency to the package.</summary>
        /// <returns>Folder.</returns>
        public PkgDependency AddPackageDependency()
        {
            return new PkgDependency(this);
        }


        /// <summary>Adds a new package dependency to the package.</summary>
        /// <returns>Folder.</returns>
        public NuDependency AddNuGetDependency()
        {
            return new NuDependency(this);
        }


        /// <summary>Adds a new package dependency to the package.</summary>
        /// <returns>Folder.</returns>
        public FwDependency AddFrameworkDependency()
        {
            return new FwDependency(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the dependency ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the ddp section that contains this item.</summary>
        public DdpSection Section
        {
            get { return Package.Section.GetSection(ID); }
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            Package.Dependencies._Items.Remove(ID);
            Save();
        }


        /// <summary>Saves the object.</summary>
        /// <param name="sec">Parent section.</param>
        public void Save(DdpSection sec)
        {
            if(ID == null)
            {
                ID = (TYPE_ID + "." + StringOp.Unique());
                Package.Dependencies._Items.Add(ID, this);
            }

            sec = sec.Sections.Add(ID);

            sec.SetString("Name", Name);
            sec.SetString("Description", Description);
            sec.SetString("TargetFramework", TargetFramework);

            foreach(FwDependency i  in FrameworkDependencies) { i.Save(sec); }
            foreach(NuDependency i  in NuGetDependencies)     { i.Save(sec); }
            foreach(PkgDependency i in PackageDependencies)   { i.Save(sec); }
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            Repository.Save();
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is Package)) return null;

            return new DependencyGroup((Package) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is Package)) return null;
            if(((Package) target).ID == Package.ID) return null;

            Package.Dependencies._Items.Remove(ID);
            ((Package) target).Dependencies._Items.Add(ID, this);
            Repository.Save();

            return this;
        }
    }
}
