﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements the repository control.</summary>
    public sealed partial class CopyJobControl: UserControl, IControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Tree node.</summary>
        private TreeNode _Node;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public CopyJobControl()
        {
            InitializeComponent();
        }

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IControl                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the tree node.</summary>
        public TreeNode Node
        {
            get { return _Node; }
            set
            {
                _Node = value;
                if(value == null) return;

                Reload();
            }
        }


        /// <summary>Gets if the contents have been modified.</summary>
        public bool Modified
        {
            get
            {
                if(_TextName.Text != ((CopyJob) _Node.Tag).Name) return true;
                if(_TextDescription.Text != ((CopyJob) _Node.Tag).Description) return true;
                if(_TextFiles.Text != ((CopyJob) _Node.Tag).Files) return true;

                return false;
            }
        }


        /// <summary>Saves the contents.</summary>
        /// <param name="name">New name.</param>
        /// <returns>Returns TRUE if the object has been saved, FALSE if an error has occured.</returns>
        public bool Save(string name = null)
        {
            if(name != null) { _TextName.Text = name; }

            ((CopyJob) _Node.Tag).Name = _TextName.Text;
            ((CopyJob) _Node.Tag).Description = _TextDescription.Text;
            ((CopyJob) _Node.Tag).Files = _TextFiles.Text;

            ((CopyJob) _Node.Tag).Save();

            if(name == null) { _Node.Text = ((CopyJob) _Node.Tag).Name; }
            _ButtonSave.Enabled = false;

            return true;
        }


        /// <summary>Refreshes the contents.</summary>
        public void Reload()
        {
            _TextName.Text = ((CopyJob) _Node.Tag).Name;
            _TextDescription.Text = ((CopyJob) _Node.Tag).Description;
            _TextFiles.Text = ((CopyJob) _Node.Tag).Files;

            _Node.Text = ((CopyJob) _Node.Tag).Name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Resize.</summary>
        private void _Resize(object sender, EventArgs e)
        {
            _TextName.Width = _TextDescription.Width = _TextFiles.Width = (Width - 77);
        }


        /// <summary>Text changed.</summary>
        private void _Changed(object sender, EventArgs e)
        {
            _ButtonSave.Enabled = Modified;
        }


        /// <summary>Button "Save" click.</summary>
        private void _ButtonSave_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>Button "Reload" click.</summary>
        private void _ButtonReload_Click(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
