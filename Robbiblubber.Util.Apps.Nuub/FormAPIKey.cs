﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements the API key window.</summary>
    public partial class FormAPIKey: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormAPIKey()
        {
            InitializeComponent();

            _TextKey.Text = Program._Settings.APIKey;
            _TextKey_TextChanged(null, null);

            this.Localize();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the API Key.</summary>
        public string Key
        {
            get { return _TextKey.Text; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Key changed.</summary>
        private void _TextKey_TextChanged(object sender, EventArgs e)
        {
            _ButtonOK.Enabled = (!string.IsNullOrWhiteSpace(_TextKey.Text));
        }


        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            Program._Settings.APIKey = _TextKey.Text;
            Close();
        }
    }
}
