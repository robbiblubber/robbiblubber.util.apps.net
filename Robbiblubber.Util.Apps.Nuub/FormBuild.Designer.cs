﻿namespace Robbiblubber.Util.Apps.Nuub
{
    partial class FormBuild
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBuild));
            this._RadioRepository = new System.Windows.Forms.RadioButton();
            this._RadioSolution = new System.Windows.Forms.RadioButton();
            this._RadioPackage = new System.Windows.Forms.RadioButton();
            this._CboSolutions = new System.Windows.Forms.ComboBox();
            this._CboPackages = new System.Windows.Forms.ComboBox();
            this._TextOutputPath = new System.Windows.Forms.TextBox();
            this._LabelOutput = new System.Windows.Forms.Label();
            this._CheckDeploy = new System.Windows.Forms.CheckBox();
            this._ButtonBuild = new System.Windows.Forms.Button();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._TextOut = new System.Windows.Forms.TextBox();
            this._PanelSolutions = new System.Windows.Forms.Panel();
            this._PanelPackages = new System.Windows.Forms.Panel();
            this._ButtonBrowse = new System.Windows.Forms.Button();
            this._LabelErrors = new System.Windows.Forms.Label();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._PanelSolutions.SuspendLayout();
            this._PanelPackages.SuspendLayout();
            this.SuspendLayout();
            // 
            // _RadioRepository
            // 
            this._RadioRepository.AutoSize = true;
            this._RadioRepository.Location = new System.Drawing.Point(33, 27);
            this._RadioRepository.Name = "_RadioRepository";
            this._RadioRepository.Size = new System.Drawing.Size(185, 21);
            this._RadioRepository.TabIndex = 0;
            this._RadioRepository.TabStop = true;
            this._RadioRepository.Tag = "nuub::udiag.build.repository";
            this._RadioRepository.Text = " Build Complete &Repository";
            this._RadioRepository.UseVisualStyleBackColor = true;
            this._RadioRepository.CheckedChanged += new System.EventHandler(this._Check);
            // 
            // _RadioSolution
            // 
            this._RadioSolution.AutoSize = true;
            this._RadioSolution.Location = new System.Drawing.Point(33, 58);
            this._RadioSolution.Name = "_RadioSolution";
            this._RadioSolution.Size = new System.Drawing.Size(112, 21);
            this._RadioSolution.TabIndex = 1;
            this._RadioSolution.TabStop = true;
            this._RadioSolution.Tag = "nuub::udiag.build.solution";
            this._RadioSolution.Text = " Build &Solution:";
            this._RadioSolution.UseVisualStyleBackColor = true;
            this._RadioSolution.CheckedChanged += new System.EventHandler(this._Check);
            // 
            // _RadioPackage
            // 
            this._RadioPackage.AutoSize = true;
            this._RadioPackage.Location = new System.Drawing.Point(33, 89);
            this._RadioPackage.Name = "_RadioPackage";
            this._RadioPackage.Size = new System.Drawing.Size(113, 21);
            this._RadioPackage.TabIndex = 3;
            this._RadioPackage.TabStop = true;
            this._RadioPackage.Tag = "nuub::udiag.build.package";
            this._RadioPackage.Text = " Build &Package:";
            this._RadioPackage.UseVisualStyleBackColor = true;
            this._RadioPackage.CheckedChanged += new System.EventHandler(this._Check);
            // 
            // _CboSolutions
            // 
            this._CboSolutions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CboSolutions.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._CboSolutions.FormattingEnabled = true;
            this._CboSolutions.Location = new System.Drawing.Point(0, 0);
            this._CboSolutions.Name = "_CboSolutions";
            this._CboSolutions.Size = new System.Drawing.Size(489, 25);
            this._CboSolutions.TabIndex = 2;
            // 
            // _CboPackages
            // 
            this._CboPackages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CboPackages.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this._CboPackages.FormattingEnabled = true;
            this._CboPackages.Location = new System.Drawing.Point(0, 0);
            this._CboPackages.Name = "_CboPackages";
            this._CboPackages.Size = new System.Drawing.Size(489, 25);
            this._CboPackages.TabIndex = 4;
            // 
            // _TextOutputPath
            // 
            this._TextOutputPath.Location = new System.Drawing.Point(33, 172);
            this._TextOutputPath.Name = "_TextOutputPath";
            this._TextOutputPath.Size = new System.Drawing.Size(769, 25);
            this._TextOutputPath.TabIndex = 5;
            // 
            // _LabelOutput
            // 
            this._LabelOutput.AutoSize = true;
            this._LabelOutput.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelOutput.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelOutput.Location = new System.Drawing.Point(30, 156);
            this._LabelOutput.Name = "_LabelOutput";
            this._LabelOutput.Size = new System.Drawing.Size(97, 13);
            this._LabelOutput.TabIndex = 5;
            this._LabelOutput.Tag = "nuub::udiag.build.outp";
            this._LabelOutput.Text = "&Output Directory:";
            // 
            // _CheckDeploy
            // 
            this._CheckDeploy.AutoSize = true;
            this._CheckDeploy.Location = new System.Drawing.Point(33, 235);
            this._CheckDeploy.Name = "_CheckDeploy";
            this._CheckDeploy.Size = new System.Drawing.Size(130, 21);
            this._CheckDeploy.TabIndex = 7;
            this._CheckDeploy.Tag = "nuub::udiag.build.deploy";
            this._CheckDeploy.Text = " &Deploy Packages";
            this._CheckDeploy.UseVisualStyleBackColor = true;
            // 
            // _ButtonBuild
            // 
            this._ButtonBuild.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonBuild.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonBuild.Location = new System.Drawing.Point(544, 282);
            this._ButtonBuild.Name = "_ButtonBuild";
            this._ButtonBuild.Size = new System.Drawing.Size(141, 29);
            this._ButtonBuild.TabIndex = 8;
            this._ButtonBuild.Tag = "nuub::common.button.build";
            this._ButtonBuild.Text = "&Build";
            this._ButtonBuild.UseVisualStyleBackColor = true;
            this._ButtonBuild.Click += new System.EventHandler(this._ButtonBuild_Click);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(691, 282);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(141, 29);
            this._ButtonCancel.TabIndex = 9;
            this._ButtonCancel.Tag = "nuub::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _TextOut
            // 
            this._TextOut.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextOut.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TextOut.Location = new System.Drawing.Point(1, 357);
            this._TextOut.Multiline = true;
            this._TextOut.Name = "_TextOut";
            this._TextOut.ReadOnly = true;
            this._TextOut.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._TextOut.Size = new System.Drawing.Size(866, 376);
            this._TextOut.TabIndex = 11;
            // 
            // _PanelSolutions
            // 
            this._PanelSolutions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelSolutions.Controls.Add(this._CboSolutions);
            this._PanelSolutions.Location = new System.Drawing.Point(164, 54);
            this._PanelSolutions.Name = "_PanelSolutions";
            this._PanelSolutions.Size = new System.Drawing.Size(491, 27);
            this._PanelSolutions.TabIndex = 2;
            // 
            // _PanelPackages
            // 
            this._PanelPackages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._PanelPackages.Controls.Add(this._CboPackages);
            this._PanelPackages.Location = new System.Drawing.Point(164, 89);
            this._PanelPackages.Name = "_PanelPackages";
            this._PanelPackages.Size = new System.Drawing.Size(491, 27);
            this._PanelPackages.TabIndex = 4;
            // 
            // _ButtonBrowse
            // 
            this._ButtonBrowse.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonBrowse.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonBrowse.Image")));
            this._ButtonBrowse.Location = new System.Drawing.Point(808, 172);
            this._ButtonBrowse.Name = "_ButtonBrowse";
            this._ButtonBrowse.Size = new System.Drawing.Size(25, 25);
            this._ButtonBrowse.TabIndex = 6;
            this._ButtonBrowse.TabStop = false;
            this._ButtonBrowse.Tag = "||nuub::udiag.build.browse";
            this._ToolTip.SetToolTip(this._ButtonBrowse, "Browse...");
            this._ButtonBrowse.UseVisualStyleBackColor = true;
            this._ButtonBrowse.Click += new System.EventHandler(this._ButtonBrowse_Click);
            // 
            // _LabelErrors
            // 
            this._LabelErrors.AutoSize = true;
            this._LabelErrors.Location = new System.Drawing.Point(30, 328);
            this._LabelErrors.Name = "_LabelErrors";
            this._LabelErrors.Size = new System.Drawing.Size(144, 17);
            this._LabelErrors.TabIndex = 10;
            this._LabelErrors.Text = "0 Errors      0 Warnings";
            this._LabelErrors.Visible = false;
            // 
            // FormBuild
            // 
            this.AcceptButton = this._ButtonBuild;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(870, 334);
            this.Controls.Add(this._LabelErrors);
            this.Controls.Add(this._PanelPackages);
            this.Controls.Add(this._PanelSolutions);
            this.Controls.Add(this._TextOut);
            this.Controls.Add(this._ButtonBrowse);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonBuild);
            this.Controls.Add(this._CheckDeploy);
            this.Controls.Add(this._LabelOutput);
            this.Controls.Add(this._TextOutputPath);
            this.Controls.Add(this._RadioPackage);
            this.Controls.Add(this._RadioSolution);
            this.Controls.Add(this._RadioRepository);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FormBuild";
            this.Text = "Build";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormBuild_FormClosing);
            this._PanelSolutions.ResumeLayout(false);
            this._PanelPackages.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton _RadioRepository;
        private System.Windows.Forms.RadioButton _RadioSolution;
        private System.Windows.Forms.RadioButton _RadioPackage;
        private System.Windows.Forms.ComboBox _CboSolutions;
        private System.Windows.Forms.ComboBox _CboPackages;
        private System.Windows.Forms.TextBox _TextOutputPath;
        private System.Windows.Forms.Label _LabelOutput;
        private System.Windows.Forms.CheckBox _CheckDeploy;
        private System.Windows.Forms.Button _ButtonBuild;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonBrowse;
        private System.Windows.Forms.TextBox _TextOut;
        private System.Windows.Forms.Panel _PanelSolutions;
        private System.Windows.Forms.Panel _PanelPackages;
        private System.Windows.Forms.Label _LabelErrors;
        private System.Windows.Forms.ToolTip _ToolTip;
    }
}