﻿namespace Robbiblubber.Util.Apps.Nuub
{
    partial class FormAPIKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAPIKey));
            this._LabelKey = new System.Windows.Forms.Label();
            this._TextKey = new System.Windows.Forms.TextBox();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _LabelKey
            // 
            this._LabelKey.AutoSize = true;
            this._LabelKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelKey.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelKey.Location = new System.Drawing.Point(33, 23);
            this._LabelKey.Name = "_LabelKey";
            this._LabelKey.Size = new System.Drawing.Size(46, 13);
            this._LabelKey.TabIndex = 0;
            this._LabelKey.Tag = "nuub::udiag.deploy.key";
            this._LabelKey.Text = "API &Key:";
            // 
            // _TextKey
            // 
            this._TextKey.Location = new System.Drawing.Point(36, 39);
            this._TextKey.Name = "_TextKey";
            this._TextKey.Size = new System.Drawing.Size(461, 25);
            this._TextKey.TabIndex = 0;
            this._TextKey.TextChanged += new System.EventHandler(this._TextKey_TextChanged);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(356, 104);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(141, 29);
            this._ButtonCancel.TabIndex = 2;
            this._ButtonCancel.Tag = "nuub::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(209, 104);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(141, 29);
            this._ButtonOK.TabIndex = 1;
            this._ButtonOK.Tag = "nuub::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // FormAPIKey
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(538, 161);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._LabelKey);
            this.Controls.Add(this._TextKey);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAPIKey";
            this.Text = "API Key";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelKey;
        private System.Windows.Forms.TextBox _TextKey;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
    }
}