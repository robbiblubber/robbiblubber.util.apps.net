﻿using System;
using System.Data;
using System.IO;

using Robbiblubber.Util.Localization.Controls;




namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements a package source folder.</summary>
    public sealed class Folder: IItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /** Type ID. */
        public const string TYPE_ID = "NuF";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="parent">Parent folder.</param>
        /// <param name="sec">Section.</param>
        public Folder(Package package, Folder parent, DdpSection sec)
        {
            Package = package;
            Parent = parent;

            ID = sec.Name;
            Name = sec.GetString("Name");
            Description = sec.GetString("Description");

            Folders = new FolderList(package, this);
            Jobs = new CopyJobList(this);
        }

        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="parent">Parent folder.</param>
        public Folder(Package package, Folder parent = null)
        {
            Package = package;
            Parent = parent;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";

            Folders = new FolderList(package, this);
            Jobs = new CopyJobList(this);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="package">Parent package.</param>
        /// <param name="parent">Parent folder.</param>
        /// <param name="template">Template.</param>
        public Folder(Package package, Folder parent, Folder template)
        {
            Package = package;
            Parent = parent;

            ID = null;
            Name = template.Name;
            Description = template.Description;

            Folders = new FolderList(package, this);
            Jobs = new CopyJobList(this);

            foreach(Folder i in template.Folders) { new Folder(Package, this, i); }
            foreach(CopyJob i in template.Jobs) { new CopyJob(this, i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent folder for this folder.</summary>
        public Folder Parent
        {
            get; private set;
        }


        /// <summary>Gets or sets the package name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the package description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the ddp section that contains this item.</summary>
        public DdpSection Section
        {
            get { return ((Parent == null) ? Package.Section : Parent.Section).GetSection(ID); }
        }


        /// <summary>Gets the child folders for this folder.</summary>
        public FolderList Folders
        {
            get; private set;
        }


        /// <summary>Gets the copy jobs for this folder.</summary>
        public CopyJobList Jobs
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a new child folder to this folder.</summary>
        /// <returns>Folder.</returns>
        public Folder AddFolder()
        {
            return new Folder(Package, this);
        }


        /// <summary>Adds a new copy job to this folder.</summary>
        /// <returns>Folder.</returns>
        public CopyJob AddCopyJob()
        {
            return new CopyJob(this);
        }


        /// <summary>Creates the folder in a build process.</summary>
        /// <param name="path">Folder path.</param>
        /// <param name="meta">Metadata.</param>
        public void Build(string path, ExecutionMetadata meta)
        {
            Directory.CreateDirectory(path + @"\" + Name);
            meta.Target.AppendLog("nuub::build.createdfolder".Localize("Created folder \"$(1)\".").Replace("$(1)", Name));

            foreach(CopyJob i in Jobs)
            {
                i.Build(path + @"\" + Name, meta);
            }

            foreach(Folder i in Folders)
            {
                i.Build(path + @"\" + Name, meta);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates package and cascades change to child folders.</summary>
        /// <param name="package">Package</param>
        private void _UpdatePackage(Package package)
        {
            Package = package;
            foreach(Folder i in Folders) { i._UpdatePackage(package); }
        }


        /// <summary>Gets if this folder is a child folder of a given folder.</summary>
        /// <param name="folder">Folder.</param>
        /// <returns>Returns TRUE if the folder is a child, otherwise returns FALSE.</returns>
        private bool _IsChildOf(Folder folder)
        {
            Folder cur = this;

            while(cur != null)
            {
                if(cur.ID == folder.ID) { return true; }

                cur = cur.Parent;
            }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the folder ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Package.Repository; }
        }


        /// <summary>Gets the package for this folder.</summary>
        public Package Package
        {
            get; private set;
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            if(Parent == null)
            {
                Package.Folders._Items.Remove(ID);
            }
            else
            {
                Parent.Folders._Items.Remove(ID);
            }
        }


        /// <summary>Saves the object.</summary>
        /// <param name="sec">Parent section.</param>
        public void Save(DdpSection sec)
        {
            if(ID == null)
            {
                ID = (TYPE_ID + "." + StringOp.Unique());

                if(Parent == null)
                {
                    Package.Folders._Items.Add(ID, this);
                }
                else { Parent.Folders._Items.Add(ID, this); }
            }

            sec = sec.Sections.Add(ID);

            sec.SetString("Name", Name);
            sec.SetString("Description", Description);

            foreach(Folder i in Folders) { i.Save(sec); }
            foreach(CopyJob i in Jobs) { i.Save(sec); }
        }


        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(target is Package)
            {
                return new Folder((Package) target, null, this);
            }
            else if(target is Folder)
            {
                return new Folder(((Folder) target).Package, (Folder) target, this);
            }
            else { return null; }
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(target is Package)
            {
                if((((Package) target).ID == Package.ID) && (Parent == null)) { return null; }

                ((Parent == null) ? Package.Folders : Parent.Folders)._Items.Remove(ID);

                _UpdatePackage((Package) target);
                Parent = null;

                Package.Folders._Items.Add(ID, this);

                return this;
            }
            else if(target is Folder)
            {
                if(((Folder) target)._IsChildOf(this)) { return null; }

                ((Parent == null) ? Package.Folders : Parent.Folders)._Items.Remove(ID);

                Parent = ((Folder) target);
                _UpdatePackage(Parent.Package);

                Parent.Folders._Items.Add(ID, this);

                return this;
            }

            return null;
        }
    }
}
