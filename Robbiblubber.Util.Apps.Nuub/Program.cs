﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>Main program class.</summary>
    static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Current repository.</summary>
        internal static Repository _Repository = null;
        
        /// <summary>Splash window.</summary>
        internal static FormSplash _Splash;

        /// <summary>Main window.</summary>
        internal static FormMain _Main;

        /// <summary>Build window.</summary>
        internal static FormBuild _Build = null;

        /// <summary>Deploy window.</summary>
        internal static FormDeploy _Deploy = null;

        /// <summary>Settings.</summary>
        internal static Settings _Settings = null;

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Main entry point                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="argv">Arguments.</param>
        [STAThread]
        static void Main(string[] argv)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            PathOp.Initialize("robbiblubber.org/Nuub");
            _Settings = new Settings();

            Locale.UsingPrefixes("nuub", "utctrl", "debugx");
            Locale.LoadSelection("Robbiblubber.Util.NET");

            _Splash = new FormSplash();
            _Splash.Show();
            Application.DoEvents();

            string file = null;

            foreach(string i in argv) { file = i; }

            if(file != null)
            {
                try
                {
                    _Repository = Repository.Load(file);
                }
                catch(Exception ex) { MessageBox.Show("nuub::udiag.open.fail".Localize("Failed to open file. $(1).").Replace("$(1)", ex.Message), "nuub::udiag.open.caption".Localize(), MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }

            _Splash.DelayClose();

            Application.Run(_Main = new FormMain());
        }
    }
}
