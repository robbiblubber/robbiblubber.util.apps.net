﻿using System;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>Log targets implement this interface.</summary>
    public interface ILogTarget
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Appends a log entry.</summary>
        /// <param name="severity">Severity.</param>
        /// <param name="text">Text.</param>
        void AppendLog(LogSeverity severity, string text);


        /// <summary>Appends a log entry.</summary>
        /// <param name="text">Text.</param>
        void AppendLog(string text);
    }
}
