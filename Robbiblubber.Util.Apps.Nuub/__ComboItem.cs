﻿using System;
using System.Windows.Forms;

namespace Robbiblubber.Util.Apps.Nuub
{
    internal sealed class __ComboItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Item.</summary>
        public IItem Item = null;

        /// <summary>Index.</summary>
        public int Index;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="item">Package.</param>
        public __ComboItem(IItem item)
        {
            Item = item;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Selects an item in a combobox.</summary>
        /// <param name="cbo">Combobox.</param>
        /// <param name="item">Item.</param>
        public static void Select(ComboBox cbo, IItem item)
        {
            for(int i = 0; i < cbo.Items.Count; i++)
            {
                if(((__ComboItem) cbo.Items[i]).Item.ID == item.ID)
                {
                    cbo.SelectedIndex = i;
                    break;
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the package ID.</summary>
        public string ID
        {
            get
            {
                if(Item == null) return null;
                return Item.ID;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            if(Item == null) { return "<NONE>"; }
            return Item.Name;
        }
    }
}
