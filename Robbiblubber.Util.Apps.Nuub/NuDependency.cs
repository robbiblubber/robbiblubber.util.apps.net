﻿using System;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class implements a NuGet dependency.</summary>
    public sealed class NuDependency: IItem, IDependency
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /** Type ID. */
        public const string TYPE_ID = "NuN";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="group">Dependency group.</param>
        /// <param name="sec">Section.</param>
        public NuDependency(DependencyGroup group, DdpSection sec)
        {
            Group = group;

            ID = sec.Name;
            Name = sec.GetString("Name");
            Description = sec.GetString("Description");
            PackageID = sec.GetString("PackageID");
            PackageVersion = sec.GetString("PackageVersion");
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="group">Dependency group.</param>
        public NuDependency(DependencyGroup group)
        {
            Group = group;

            ID = null;
            Name = "nuub::udiag.unnamed".Localize("Unnamed");
            Description = "";
            PackageID = "";
            PackageVersion = "";
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="group">Dependency group.</param>
        /// <param name="template">Template.</param>
        public NuDependency(DependencyGroup group, NuDependency template)
        {
            Group = group;

            ID = null;
            Name = template.Name;
            Description = template.Description;
            PackageID = template.PackageID;
            PackageVersion = template.PackageVersion;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent repository.</summary>
        public Repository Repository
        {
            get { return Group.Repository; }
        }


        /// <summary>Gets the dependency group for this dependency.</summary>
        public DependencyGroup Group
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency package ID.</summary>
        public string PackageID
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency package version.</summary>
        public string PackageVersion
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IItem                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the dependency ID.</summary>
        public string ID
        {
            get; private set;
        }


        /// <summary>Gets or sets the dependency name.</summary>
        public string Name
        {
            get; set;
        }


        /// <summary>Gets or sets the dependency description.</summary>
        public string Description
        {
            get; set;
        }


        /// <summary>Gets the ddp section that contains this item.</summary>
        public DdpSection Section
        {
            get { return Group.Section.GetSection(ID); }
        }


        /// <summary>Deletes the objct.</summary>
        public void Delete()
        {
            Group.NuGetDependencies._Items.Remove(ID);
        }


        /// <summary>Saves the object.</summary>
        /// <param name="sec">Parent section.</param>
        public void Save(DdpSection sec)
        {
            if(ID == null)
            {
                ID = (TYPE_ID + "." + StringOp.Unique());
                Group.NuGetDependencies._Items.Add(ID, this);
            }

            sec = sec.Sections.Add(ID);

            sec.SetString("Name", Name);
            sec.SetString("Description", Description);
            sec.SetString("PackageID", PackageID);
            sec.SetString("PackageVersion", PackageVersion);
        }

        
        /// <summary>Copies the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the copied item or NULL if the object could not be copied.</returns>
        public IItem CopyTo(IItem target)
        {
            if(!(target is DependencyGroup)) return null;

            return new NuDependency((DependencyGroup) target, this);
        }


        /// <summary>Moves the item.</summary>
        /// <param name="target">Target.</param>
        /// <returns>Returns the moved item or NULL if the object could not be copied.</returns>
        public IItem MoveTo(IItem target)
        {
            if(!(target is DependencyGroup)) return null;
            if(((DependencyGroup) target).ID == Group.ID) return null;

            Group.NuGetDependencies._Items.Remove(ID);
            (Group = (DependencyGroup) target).NuGetDependencies._Items.Add(ID, this);

            return this;
        }
    }
}
