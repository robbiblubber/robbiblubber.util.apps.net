﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.Nuub
{
    /// <summary>This class defines the main window.</summary>
    public sealed partial class FormMain: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Show debug menu.</summary>
        private bool _ShowDebug = false;

        /// <summary>Control showing.</summary>
        private IControl _ShowingControl;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormMain()
        {
            InitializeComponent();

            _RepositoryCtrl.Dock = _SolutionCtrl.Dock = _PackageCtrl.Dock = _FolderCtrl.Dock = _CopyJobCtrl.Dock = 
                                   _PkgDependencyCtrl.Dock = _NuDependencyCtrl.Dock = _FwDependencyCtrl.Dock = DockStyle.Fill;
            
            LayoutOp.Initialize(PathOp.UserConfigurationPath + @"\application.layout");
            this.RestoreLayout();

            _LoadSettings();
            this.Localize();

            _Draw(_TreeMain.Nodes, Program._Repository);

            if(_TreeMain.Nodes.Count > 0) { _TreeMain.Nodes[0].Expand(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the active control.</summary>
        private Control _ActiveControl
        {
            get
            {
                Control rval = _SplitMain.ActiveControl;
                if(rval is UserControl) { rval = ((UserControl) rval).ActiveControl; }

                return rval;
            }
        }


        /// <summary>Gets or sets if the debug menu is shown.</summary>
        internal bool __ShowDebug
        {
            get { return _ShowDebug; }
            set
            {
                _ShowDebug = _MenuDebug.Visible = value;

                DdpFile cfg = Ddp.Load(PathOp.UserSettingsFile);
                cfg.SetBoolean("/settings/debug", value);
                cfg.Save();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the tree.</summary>
        private void _Refresh()
        {
            object cur = null;
            if(_TreeMain.SelectedNode != null) { cur = _TreeMain.SelectedNode.Tag; }

            List<object> open = new List<object>();
            if(_TreeMain.Nodes.Count > 0) _GetOpenNodes(ref open, _TreeMain.Nodes[0]);

            _TreeMain.BeginUpdate();
            _TreeMain.Nodes.Clear();
            if(Program._Repository != null) _Draw(_TreeMain.Nodes, Program._Repository);
            _TreeMain.EndUpdate();

            if(_TreeMain.Nodes.Count > 0) _RestoreOpenNodes(open, cur, _TreeMain.Nodes[0]);
        }


        /// <summary>Gathers tags of open nodes.</summary>
        /// <param name="open">Open node list.</param>
        /// <param name="n">Tree node.</param>
        private void _GetOpenNodes(ref List<object> open, TreeNode n)
        {
            if(n.IsExpanded) { open.Add(n.Tag); }

            foreach(TreeNode i in n.Nodes) _GetOpenNodes(ref open, i);
        }


        /// <summary>Restores open nodes.</summary>
        /// <param name="open">Open node list.</param>
        /// <param name="selected">Selected node tag.</param>
        /// <param name="n">Tree node.</param>
        private void _RestoreOpenNodes(List<object> open, object selected, TreeNode n)
        {
            if(open.Contains(n.Tag)) { n.Expand(); }
            if(n.Tag == selected) { _TreeMain.SelectedNode = n; }

            foreach(TreeNode i in n.Nodes) _RestoreOpenNodes(open, selected, i);
        }


        /// <summary>Draws the current project.</summary>
        /// <param name="parent">Parent node collection.</param>
        /// <param name="item">Item.</param>
        private void _Draw(TreeNodeCollection parent, object item)
        {
            if(item == null) return;

            TreeNode n = null;

            if(item is Repository)
            {
                n = new TreeNode(((Repository) item).Name);
                n.ImageKey = n.SelectedImageKey = "img::repository";
                n.Tag = item;

                foreach(Solution i in ((Repository) item).Solutions) _Draw(n.Nodes, i);
            }
            else if(item is Solution)
            {
                n = new TreeNode(((Solution) item).Name);
                n.ImageKey = n.SelectedImageKey = "img::solution";
                n.Tag = item;

                foreach(Package i in ((Solution) item).Packages) _Draw(n.Nodes, i);
            }
            else if(item is Package)
            {
                n = new TreeNode(((Package) item).Name);
                n.ImageKey = n.SelectedImageKey = "img::package";
                n.Tag = item;

                foreach(PkgDependency i in ((Package) item).PackageDependencies) _Draw(n.Nodes, i);
                foreach(NuDependency i in ((Package) item).NuGetDependencies) _Draw(n.Nodes, i);
                foreach(FwDependency i in ((Package) item).FrameworkDependencies) _Draw(n.Nodes, i);
                foreach(Folder i in ((Package) item).Folders) _Draw(n.Nodes, i);
            }
            else if(item is Folder)
            {
                n = new TreeNode(((Folder) item).Name);
                n.ImageKey = n.SelectedImageKey = "img::folder";
                n.Tag = item;

                foreach(Folder i in ((Folder) item).Folders) _Draw(n.Nodes, i);
                foreach(CopyJob i in ((Folder) item).Jobs) _Draw(n.Nodes, i);
            }
            else if(item is CopyJob)
            {
                n = new TreeNode(((CopyJob) item).Name);
                n.ImageKey = n.SelectedImageKey = "img::job";
                n.Tag = item;
            }
            else if(item is PkgDependency)
            {
                n = new TreeNode(((PkgDependency) item).Name);
                n.ImageKey = n.SelectedImageKey = "img::pkgdep";
                n.Tag = item;
            }
            else if(item is NuDependency)
            {
                n = new TreeNode(((NuDependency) item).Name);
                n.ImageKey = n.SelectedImageKey = "img::nudep";
                n.Tag = item;
            }
            else if(item is FwDependency)
            {
                n = new TreeNode(((FwDependency) item).Name);
                n.ImageKey = n.SelectedImageKey = "img::fwdep";
                n.Tag = item;
            }

            if(n != null) parent.Add(n);
        }


        /// <summary>Loads the application settings.</summary>
        internal void _LoadSettings()
        {
            try
            {
                DdpFile cfg = Ddp.Load(PathOp.UserSettingsFile);
                __ShowDebug = cfg.GetBoolean("/settings/debug", false);

                if(Program._Repository == null)
                {
                    try { Program._Repository = Repository.Load(cfg.GetString("/settings/repository")); } catch(Exception) {}
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("NUUB00200", ex); }
        }
        


        /// <summary>Checks if current date must be saved.</summary>
        /// <returns>Returns TRUE if the application may proceed, otherwise returns FALSE.</returns>
        private bool _CheckSaveCancel()
        {
            if((_ShowingControl != null) && _ShowingControl.Modified)
            {
                DialogResult d = DialogResult.Yes;

                if(!Program._Settings.AutoSave)
                {
                    d = MessageBox.Show("nuub::udiag.savechanges.text".Localize("The current item has been modified. Do you want to save these changes to the repository?"), "nuub::udiag.savechanges.caption".Localize("Save Changes"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                }

                if(d == DialogResult.Cancel)
                {
                    _TreeMain.SelectedNode = _ShowingControl.Node;
                    return false;
                }
                else if(d == DialogResult.Yes)
                {
                    return _ShowingControl.Save();
                }
            }

            return true;
        }


        /// <summary>Selects the tree node for an item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="start">Starting node collection.</param>
        /// <returns>Returns TRUE if an item has been selected, otherwise returns FALSE.</returns>
        private bool _SelectTreeNodeFor(object item, TreeNodeCollection start = null)
        {
            TreeNode n = _TreeMain.SelectedNode;
            _TreeMain.SelectedNode = _TreeMain.Nodes[0];

            if(start == null) { start = _TreeMain.Nodes; }

            foreach(TreeNode i in start)
            {
                if(i.Tag == item)
                {
                    i.EnsureVisible();
                    _TreeMain.SelectedNode = i;

                    return true;
                }

                if(_SelectTreeNodeFor(item, i.Nodes)) return true;
            }

            return false;
        }


        /// <summary>Gets the tree node for an item.</summary>
        /// <param name="id">Item ID.</param>
        /// <param name="start">Starting node collection.</param>
        /// <returns>Tree node.</returns>
        private TreeNode _NodeFor(string id, TreeNodeCollection start = null)
        {
            if(start == null) { start = _TreeMain.Nodes; }

            TreeNode rval;

            foreach(TreeNode i in start)
            {
                if(((IItem) i.Tag).ID == id) return i;

                if((rval = _NodeFor(id, i.Nodes)) != null) return rval;
            }

            return null;
        }


        /// <summary>Gets the tree node for an item.</summary>
        /// <param name="item">Item.</param>
        /// <param name="start">Starting node collection.</param>
        /// <returns>Tree node.</returns>
        private TreeNode _NodeFor(object item, TreeNodeCollection start = null)
        {
            if(item is IItem) { return _NodeFor(((IItem) item).ID); }

            return null;
        }


        /// <summary>Copies a dragged object.</summary>
        private void _DropCopy()
        {
            Cursor c = Cursor;
            Cursor = Cursors.WaitCursor;

            if(!__ClipOp._DropAllowed) return;

            IItem m = ((IItem) __ClipOp._DragNode.Tag).CopyTo((IItem) __ClipOp._DropTarget.Tag);

            if(m != null)
            {
                _Refresh();
                _SelectTreeNodeFor(m);
            }
            Cursor = c;
        }


        /// <summary>Moves a dragged object.</summary>
        private void _DropMove()
        {
            TreeNode n = _TreeMain.SelectedNode;
            _TreeMain.SelectedNode = _TreeMain.Nodes[0];

            Cursor c = Cursor;
            Cursor = Cursors.WaitCursor;

            if(!__ClipOp._DropAllowed) return;

            IItem m = ((IItem) __ClipOp._DragNode.Tag).MoveTo((IItem) __ClipOp._DropTarget.Tag);

            if(m == null)
            {
                _TreeMain.SelectedNode = n;
            }
            else
            {
                _Refresh();
                _SelectTreeNodeFor(m);
            }
            Cursor = c;
        }


        /// <summary>Cancels cut operation.</summary>
        private void _CancelCut()
        {
            TreeNode n = _NodeFor((string) Clipboard.GetData("nuub://item/cut"));
            if(n == null) { return; }

            if(n.ImageKey.EndsWith("/l")) { n.ImageKey = n.SelectedImageKey = n.ImageKey.Substring(0, n.ImageKey.Length - 2); }
            n.ForeColor = SystemColors.WindowText;

            Clipboard.SetData("nuub://item/cut", null);
        }


        /// <summary>Returns if an object can be pasted into the tree.</summary>
        /// <returns>Returns TRUE if paste is possible, otherwise returns FALSE.</returns>
        private bool _CanPasteIntoTree()
        {
            if((_TreeMain.SelectedNode != null) || (_TreeMain.SelectedNode.Tag is CopyJob) || (_TreeMain.SelectedNode.Tag is PkgDependency) || (_TreeMain.SelectedNode.Tag is NuDependency) || (_TreeMain.SelectedNode.Tag is FwDependency))
            {
                return false;
            }
            TreeNode n = _NodeFor((string) Clipboard.GetData("nuub://item/copy"));
            if(n == null)
            {
                n = _NodeFor((string) Clipboard.GetData("nuub://item/cut"));
            }

            if(n == null) return false;

            return ((IItem) n.Tag)._IsAssignableTo((IItem) _TreeMain.SelectedNode.Tag);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Menu "Open Repository" click.</summary>
        private void _MenuOpenRepository_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "nuub::udiag.filter.nuub".Localize("Nuub Repositories (.nuub)") + "|*.nuub|" + "nuub::udiag.filter.all".Localize("All Files") + "|*.*";

            if(d.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if(_CheckSaveCancel()) { Program._Repository = Repository.Load(d.FileName); }
                }
                catch(Exception ex)
                {
                    MessageBox.Show("nuub::udiag.open.fail".Localize("Failed to open file. $(1).").Replace("$(1)", ex.Message), "nuub::udiag.open.caption".Localize(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                _Refresh();
            }
        }


        /// <summary>After node selected.</summary>
        private void _TreeMain_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if((_ShowingControl != null) && (_ShowingControl.Node == e.Node)) return;

            if(!_CheckSaveCancel()) return;

            _RepositoryCtrl.Visible = _SolutionCtrl.Visible = _PackageCtrl.Visible = _FolderCtrl.Visible = _CopyJobCtrl.Visible =
                                      _PkgDependencyCtrl.Visible = _NuDependencyCtrl.Visible = _FwDependencyCtrl.Visible = false;

            if(e.Node.Tag is Repository)
            {
                _ShowingControl = _RepositoryCtrl;

                _RepositoryCtrl.Node = e.Node;
                _RepositoryCtrl.Visible = true;
            }
            else if(e.Node.Tag is Solution)
            {
                _ShowingControl = _SolutionCtrl;

                _SolutionCtrl.Node = e.Node;
                _SolutionCtrl.Visible = true;
            }
            else if(e.Node.Tag is Package)
            {
                _ShowingControl = _PackageCtrl;

                _PackageCtrl.Node = e.Node;
                _PackageCtrl.Visible = true;
            }
            else if(e.Node.Tag is Folder)
            {
                _ShowingControl = _FolderCtrl;

                _FolderCtrl.Node = e.Node;
                _FolderCtrl.Visible = true;
            }
            else if(e.Node.Tag is CopyJob)
            {
                _ShowingControl = _CopyJobCtrl;

                _CopyJobCtrl.Node = e.Node;
                _CopyJobCtrl.Visible = true;
            }
            else if(e.Node.Tag is PkgDependency)
            {
                _ShowingControl = _PkgDependencyCtrl;

                _PkgDependencyCtrl.Node = e.Node;
                _PkgDependencyCtrl.Visible = true;
            }
            else if(e.Node.Tag is NuDependency)
            {
                _ShowingControl = _NuDependencyCtrl;

                _NuDependencyCtrl.Node = e.Node;
                _NuDependencyCtrl.Visible = true;
            }
            else if(e.Node.Tag is FwDependency)
            {
                _ShowingControl = _FwDependencyCtrl;

                _FwDependencyCtrl.Node = e.Node;
                _FwDependencyCtrl.Visible = true;
            }
            else
            {
                _ShowingControl = null;
            }
        }


        /// <summary>Form closing.</summary>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(!_CheckSaveCancel())
            {
                e.Cancel = true;
                return;
            }

            this.SaveLayout();
        }


        /// <summary>Menu "Add Solution" click.</summary>
        private void _MenuAddSolution_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;
            if(!(_TreeMain.SelectedNode.Tag is Repository)) return;

            Solution s = Program._Repository.AddSolution();

            _Draw(_TreeMain.SelectedNode.Nodes, s);
            _SelectTreeNodeFor(s);
        }


        /// <summary>Menu "Add Package" click.</summary>
        private void _MenuAddPackage_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;
            if(!(_TreeMain.SelectedNode.Tag is Solution)) return;

            Package p = ((Solution) _TreeMain.SelectedNode.Tag).AddPackage();

            _Draw(_TreeMain.SelectedNode.Nodes, p);
            _SelectTreeNodeFor(p);
        }


        /// <summary>Menu "Add Folder" click.</summary>
        private void _MenuAddFolder_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;

            Folder f = null;

            if(_TreeMain.SelectedNode.Tag is Package)
            {
                f = ((Package) _TreeMain.SelectedNode.Tag).AddFolder();
            }
            else if(_TreeMain.SelectedNode.Tag is Folder)
            {
                f = ((Folder) _TreeMain.SelectedNode.Tag).AddFolder();
            }

            if(f != null)
            {
                _Refresh();
                _SelectTreeNodeFor(f);
            }
        }


        /// <summary>Menu "Add Copy Job" click.</summary>
        private void _MenuAddCopyJob_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;
            if(!(_TreeMain.SelectedNode.Tag is Folder)) return;

            CopyJob j = ((Folder) _TreeMain.SelectedNode.Tag).AddCopyJob();

            _Refresh();
            _SelectTreeNodeFor(j);
        }


        /// <summary>Menu "Add Package Dependency" click.</summary>
        private void _MenuAddPkgDependency_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;
            if(!(_TreeMain.SelectedNode.Tag is Package)) return;

            PkgDependency d = ((Package) _TreeMain.SelectedNode.Tag).AddPackageDependency();

            _Refresh();
            _SelectTreeNodeFor(d);
        }


        /// <summary>Menu "Add NuGet Dependency" click.</summary>
        private void _MenuAddNuDependency_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;
            if(!(_TreeMain.SelectedNode.Tag is Package)) return;

            NuDependency d = ((Package) _TreeMain.SelectedNode.Tag).AddNuGetDependency();

            _Refresh();
            _SelectTreeNodeFor(d);
        }


        /// <summary>Menu "Add Framework Dependency" click.</summary>
        private void _MenuAddFwDependency_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;
            if(!(_TreeMain.SelectedNode.Tag is Package)) return;

            FwDependency d = ((Package) _TreeMain.SelectedNode.Tag).AddFrameworkDependency();

            _Refresh();
            _SelectTreeNodeFor(d);
        }


        /// <summary>Tree mouse down.</summary>
        private void _TreeMain_MouseDown(object sender, MouseEventArgs e)
        {
            _TreeMain.SelectedNode = _TreeMain.HitTest(e.Location).Node;
        }


        /// <summary>Tree mouse move.</summary>
        private void _TreeMain_MouseMove(object sender, MouseEventArgs e)
        {
            if((e.Button == MouseButtons.None) || (_TreeMain.SelectedNode == null)) return;


            if((_TreeMain.SelectedNode != null) && (_TreeMain.SelectedNode._IsDragable()))
            {
                __ClipOp._DragNode = _TreeMain.SelectedNode;
                _TreeMain.DoDragDrop("nuub://item/drag/" + ((IItem) _TreeMain.SelectedNode.Tag).ID, DragDropEffects.Copy | DragDropEffects.Move);
            }
            else
            {
                _TreeMain.DoDragDrop("", DragDropEffects.None);
            }
        }


        /// <summary>Tree drag over.</summary>
        private void _TreeMain_DragOver(object sender, DragEventArgs e)
        {
            if(!e.Data.GetDataPresent(typeof(string)) || (!(((string) e.Data.GetData(typeof(string)))).StartsWith("nuub://item/drag/")))
            {
                __ClipOp._DropTarget = __ClipOp._DragNode = null;
                return;
            }

            __ClipOp._DropTarget = _TreeMain.GetNodeAt(_TreeMain.PointToClient(new Point(e.X, e.Y)));

            if(!__ClipOp._DropAllowed)
            {
                e.Effect = DragDropEffects.None;
            }
            else
            {
                __ClipOp._DragState = e.KeyState;

                if(((e.KeyState & 9) == 9) && ((e.KeyState & 2) != 2))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else { e.Effect = DragDropEffects.Move; }
            }
        }


        /// <summary>Tree drop.</summary>
        private void _TreeMain_DragDrop(object sender, DragEventArgs e)
        {
            __ClipOp._DropTarget = _TreeMain.GetNodeAt(_TreeMain.PointToClient(new Point(e.X, e.Y)));

            if(!__ClipOp._DropAllowed) return;
            try
            {
                if((__ClipOp._DragState & 2) == 2)
                {
                    _CmenuDrop.Show(e.X, e.Y);
                }
                else if((__ClipOp._DragState & 9) == 9)
                {
                    _DropCopy();
                }
                else
                {
                    _DropMove();
                }
            }
            catch(Exception) { }
        }


        /// <summary>Tree key down.</summary>
        private void _TreeMain_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape) { _CancelCut(); }
        }


        /// <summary>Context menu "Copy" click.</summary>
        private void _CmenuDropCopy_Click(object sender, EventArgs e)
        {
            _DropCopy();
        }


        /// <summary>Context menu "Move" click.</summary>
        private void _CmenuDropMove_Click(object sender, EventArgs e)
        {
            _DropMove();
        }


        /// <summary>Menu "Undo" click.</summary>
        private void _MenuUndo_Click(object sender, EventArgs e)
        {
            if(_ActiveControl is TextBoxBase)
            {
                ((TextBoxBase) _ActiveControl).Undo();
            }
        }


        /// <summary>Menu "Cut" click.</summary>
        private void _MenuCut_Click(object sender, EventArgs e)
        {
            if(_ActiveControl is TextBoxBase)
            {
                ((TextBoxBase) _ActiveControl).Cut();
            }
            else if(_ActiveControl == _TreeMain)
            {
                if(_TreeMain.SelectedNode == null) return;
                if(_TreeMain.SelectedNode.Tag is Repository) return;

                _CancelCut();
                Clipboard.SetData("nuub://item/cut", ((IItem) _TreeMain.SelectedNode.Tag).ID);

                if(!_TreeMain.ImageList.Images.ContainsKey(_TreeMain.SelectedNode.ImageKey + "/l"))
                {
                    Bitmap img = new Bitmap(_TreeMain.ImageList.Images[_TreeMain.SelectedNode.ImageKey]);
                    Graphics.FromImage(img).FillRectangle(new SolidBrush(Color.FromArgb(160, Color.White)), new Rectangle(0, 0, img.Width, img.Height));

                    _TreeMain.ImageList.Images.Add(_TreeMain.SelectedNode.ImageKey + "/l", img);
                }
                _TreeMain.SelectedNode.ImageKey = _TreeMain.SelectedNode.SelectedImageKey = _TreeMain.SelectedNode.ImageKey + "/l";
                _TreeMain.SelectedNode.ForeColor = SystemColors.GrayText;
            }
        }


        /// <summary>Menu "Copy" click.</summary>
        private void _MenuCopy_Click(object sender, EventArgs e)
        {
            if(_ActiveControl is TextBoxBase)
            {
                ((TextBoxBase) _ActiveControl).Copy();
            }
            else if(_ActiveControl == _TreeMain)
            {
                if(_TreeMain.SelectedNode == null) return;
                if(_TreeMain.SelectedNode.Tag is Repository) return;

                _CancelCut();
                Clipboard.SetData("nuub://item/copy", ((IItem) _TreeMain.SelectedNode.Tag).ID);
            }
        }


        /// <summary>Menu "Paste" click.</summary>
        private void _MenuPaste_Click(object sender, EventArgs e)
        {
            if(_ActiveControl is TextBoxBase)
            {
                ((TextBoxBase) _ActiveControl).Paste();
            }
            else if(_ActiveControl == _TreeMain)
            {
                bool copy = false;
                if(_TreeMain.SelectedNode == null) { return; }
                
                TreeNode n = _NodeFor((string) Clipboard.GetData("nuub://item/cut"));
                if(n == null)
                {
                    copy = true;
                    n = _NodeFor((string) Clipboard.GetData("nuub://item/copy"));
                }
                if(n == null) { return; }
                if(!((IItem) n.Tag)._IsAssignableTo((IItem) _TreeMain.SelectedNode.Tag)) { return; }

                IItem m = null;
                Cursor c = Cursor;
                Cursor = Cursors.WaitCursor;

                if(copy)
                {
                    m = ((IItem) n.Tag).CopyTo((IItem) _TreeMain.SelectedNode.Tag);   
                }
                else
                {
                    m = ((IItem) n.Tag).MoveTo((IItem) _TreeMain.SelectedNode.Tag);
                }

                if(m != null)
                {
                    _Refresh();
                    _SelectTreeNodeFor(m);
                }
                Cursor = c;

                _CancelCut();
            }
        }


        /// <summary>Exits the program.</summary>
        private void _MenuExit_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Menu "Delete" click.</summary>
        private void _MenuDelete_Click(object sender, EventArgs e)
        {
            if(_ActiveControl is TextBoxBase)
            {
                if(((TextBoxBase) _ActiveControl).SelectionLength == 0) { ((TextBoxBase) _ActiveControl).SelectionLength = 1; }
                ((TextBoxBase) _ActiveControl).SelectedText = "";
            }
            else if(_ActiveControl == _TreeMain)
            {
                if(_TreeMain.SelectedNode == null) return;
                if(_TreeMain.SelectedNode.Tag is Repository) return;

                if(MessageBox.Show("nuub::udiag.delete.text".Localize("Do you really want to delete the $(1) \"$(2)\"?").Replace("$(1)", ((IItem) _TreeMain.SelectedNode.Tag)._GetClassName()).Replace("$(2)", ((IItem) _TreeMain.SelectedNode.Tag).Name), "nuub::udiag.delete.caption".Localize("Delete"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ((IItem) _TreeMain.SelectedNode.Tag).Delete();

                    _Refresh();
                }
            }
        }


        /// <summary>Menu "Refresh" click.</summary>
        private void _MenuRefresh_Click(object sender, EventArgs e)
        {
            Program._Repository.Refresh();
            _Refresh();
        }


        /// <summary>Menu "Build" click.</summary>
        private void _MenuBuild_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;
            if(!((_TreeMain.SelectedNode.Tag is Repository) || (_TreeMain.SelectedNode.Tag is Solution) || (_TreeMain.SelectedNode.Tag is Package))) return;

            if(Program._Deploy != null)
            {
                if(Program._Deploy.Running)
                {
                    Program._Deploy.BringToFront();
                    return;
                }
                else
                {
                    Program._Deploy.Close();
                    Program._Deploy = null;
                }
            }

            if(Program._Build == null)
            {
                Program._Build = new FormBuild();
            }

            Program._Build.Target = ((IItem) _TreeMain.SelectedNode.Tag);
            Program._Build.Show();
            Program._Build.BringToFront();
        }


        /// <summary>Menu "Deploy" click.</summary>
        private void _MenuDeploy_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;
            if(!((_TreeMain.SelectedNode.Tag is Repository) || (_TreeMain.SelectedNode.Tag is Solution) || (_TreeMain.SelectedNode.Tag is Package))) return;

            if(Program._Build != null)
            {
                if(Program._Build.Running)
                {
                    Program._Build.BringToFront();
                    return;
                }
                else
                {
                    Program._Build.Close();
                    Program._Build = null;
                }
            }

            if(Program._Deploy == null)
            {
                Program._Deploy = new FormDeploy();
            }

            Program._Deploy.Target = ((IItem) _TreeMain.SelectedNode.Tag);
            Program._Deploy.Show();
            Program._Deploy.BringToFront();
        }


        /// <summary>Menu "Create New Repository" click.</summary>
        private void _MenuCreateRepository_Click(object sender, EventArgs e)
        {
            SaveFileDialog d = new SaveFileDialog();
            d.Filter = "nuub::udiag.filter.nuub".Localize("Nuub Repositories (.nuub)") + "|*.nuub|" + "nuub::udiag.filter.all".Localize("All Files") + "|*.*";
            
            if(d.ShowDialog() == DialogResult.OK)
            {
                if(_CheckSaveCancel()) { Program._Repository = Repository.Create(d.FileName); }
            }
        }


        /// <summary>Menu "Save Item" click.</summary>
        private void _MenuSaveItem_Click(object sender, EventArgs e)
        {
            if(_ShowingControl != null) { _ShowingControl.Save(); }
        }


        /// <summary>Menu "Reload item" click.</summary>
        private void _MenuReloadItem_Click(object sender, EventArgs e)
        {
            if(_ShowingControl != null) { _ShowingControl.Reload(); }
        }


        /// <summary>Menu "About" click.</summary>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAbout f = new FormAbout();
            f.ShowDialog();
        }


        /// <summary>Menu "Help" click.</summary>
        private void showHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("http://robbiblubber.lima-city.de/wiki.php?title=Help-Nuub");
        }

        
        /// <summary>After label edit.</summary>
        private void _TreeMain_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if(e.Label == null) return;

            _MenuDelete.Enabled = true;
            _ShowingControl.Save(e.Label);
            _TreeMain.LabelEdit = false;
        }


        /// <summary>Menu "Edit Label" click.</summary>
        private void _MenuEditLabel_Click(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null) return;

            _MenuDelete.Enabled = false;
            _TreeMain.LabelEdit = true;
            _TreeMain.SelectedNode.BeginEdit();
        }


        /// <summary>Menu "Start/Stop Logging" click.</summary>
        private void _MenuDebugStart_Click(object sender, EventArgs e)
        {
            DebugOp.Enabled = (!DebugOp.Enabled);
        }


        /// <summary>Menu "Save Dump" click.</summary>
        private void _MenuDebugSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(!DebugOp.Enabled) return;

                SaveFileDialog d = new SaveFileDialog();
                d.Filter = "debugx::udiag.filter.dump".Localize("Debug Dump Files") + " (*.debug.dump)|*.debug.dump|" + "debugx::udiag.filter.all".Localize("All Files") + "|*.*";

                if(d.ShowDialog() == DialogResult.OK)
                {
                    DebugOp.CreateDumpFile(d.FileName);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00260", ex); }
        }


        /// <summary>Menu "View Dump" click.</summary>
        private void _MenuDebugView_Click(object sender, EventArgs e)
        {
            try
            {
                if(!DebugOp.Enabled) return;

                DebugOp.ShowLog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00261", ex); }
        }


        /// <summary>Menu "Upload Dump" click.</summary>
        private void _MenuDebugUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if(!DebugOp.Enabled) return;

                DebugOp.UploadData("http://robbiblubber.lima-city.de/debug/Robbiblubber.Util.NET/");
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00262", ex); }
        }


        /// <summary>Menu "Debug" opening.</summary>
        private void _MenuDebug_DropDownOpening(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = (!(_MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = DebugOp.Enabled));
        }


        /// <summary>Menu "Debug" closed.</summary>
        private void _MenuDebug_DropDownClosed(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = _MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = true;
        }


        /// <summary>Menu "Settings" click.</summary>
        private void _MenuSettings_Click(object sender, EventArgs e)
        {
            FormSettings f = new FormSettings();
            f.ShowDialog();
        }


        /// <summary>Menu "File" opening.</summary>
        private void _MenuFile_DropDownOpening(object sender, EventArgs e)
        {
            if(_TreeMain.SelectedNode == null)
            {
                _MenuAdd.Enabled = false;
            }
            else if(_TreeMain.SelectedNode.Tag is CopyJob)
            {
                _MenuAdd.Enabled = false;
            }
            else if(_TreeMain.SelectedNode.Tag is PkgDependency)
            {
                _MenuAdd.Enabled = false;
            }
            else if(_TreeMain.SelectedNode.Tag is NuDependency)
            {
                _MenuAdd.Enabled = false;
            }
            else if(_TreeMain.SelectedNode.Tag is FwDependency)
            {
                _MenuAdd.Enabled = false;
            }
        }


        /// <summary>Menu "File" closed.</summary>
        private void _MenuFile_DropDownClosed(object sender, EventArgs e)
        {
            _MenuAdd.Enabled = true;
        }


        /// <summary>Menu "Add" opening.</summary>
        private void _MenuAdd_DropDownOpening(object sender, EventArgs e)
        {
            _CmenuAddSolution.Visible = _MenuAddSolution.Visible = (_TreeMain.SelectedNode.Tag is Repository);
            _CmenuAddPackage.Visible = _MenuAddPackage.Visible = (_TreeMain.SelectedNode.Tag is Solution);
            _CmenuAddFolder.Visible = _MenuAddFolder.Visible = ((_TreeMain.SelectedNode.Tag is Package) || (_TreeMain.SelectedNode.Tag is Folder));
            _CmenuAddCopyJob.Visible = _MenuAddCopyJob.Visible = (_TreeMain.SelectedNode.Tag is Folder);
            _CmenuAddPkgDependency.Visible = _MenuAddPkgDependency.Visible = (_TreeMain.SelectedNode.Tag is Package);
            _CmenuAddNuDependency.Visible = _MenuAddNuDependency.Visible = (_TreeMain.SelectedNode.Tag is Package);
            _CmenuAddFwDependency.Visible = _MenuAddFwDependency.Visible = (_TreeMain.SelectedNode.Tag is Package);
        }
        

        /// <summary>Menu "Add" closing.</summary>
        private void _MenuAdd_DropDownClosed(object sender, EventArgs e)
        {
            _MenuAddSolution.Visible = _MenuAddPackage.Visible = _MenuAddFolder.Visible = _MenuAddCopyJob.Visible =
                                       _MenuAddPkgDependency.Visible = _MenuAddNuDependency.Visible = _MenuAddFwDependency.Visible = true;
        }


        /// <summary>Context menu opening.</summary>
        private void _CmenuTree_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(_TreeMain.SelectedNode == null)
            {
                _CmenuAdd.Enabled = false;
            }
            else if(_TreeMain.SelectedNode.Tag is CopyJob)
            {
                _CmenuAdd.Enabled = false;
            }
            else if(_TreeMain.SelectedNode.Tag is PkgDependency)
            {
                _CmenuAdd.Enabled = false;
            }
            else if(_TreeMain.SelectedNode.Tag is NuDependency)
            {
                _CmenuAdd.Enabled = false;
            }
            else if(_TreeMain.SelectedNode.Tag is FwDependency)
            {
                _CmenuAdd.Enabled = false;
            }

            _MenuUndo.Enabled = false;
            _MenuCut.Enabled = _MenuCopy.Enabled = _MenuDelete.Enabled = ((_TreeMain.SelectedNode != null) && (!(_TreeMain.SelectedNode.Tag is Repository)));
            _MenuPaste.Enabled = _CanPasteIntoTree();

            _CmenuTreeBlank1.Visible = _CmenuBuild.Visible = _CmenuDeploy.Visible = ((_TreeMain.SelectedNode != null)) && 
                                                                                     ((_TreeMain.SelectedNode.Tag is Repository) || (_TreeMain.SelectedNode.Tag is Solution) || (_TreeMain.SelectedNode.Tag is Package));
        }


        /// <summary>Menu "Edit" opening.</summary>
        private void _MenuEdit_DropDownOpening(object sender, EventArgs e)
        {
            if(_ActiveControl is TextBoxBase)
            {
                _MenuUndo.Enabled = ((TextBoxBase) _ActiveControl).CanUndo;
                _MenuCut.Enabled = _MenuCopy.Enabled = _MenuDelete.Enabled = true;
                _MenuPaste.Enabled = (!string.IsNullOrEmpty(Clipboard.GetText(TextDataFormat.Text)));
            }
            else if(_ActiveControl == _TreeMain)
            {
                _MenuUndo.Enabled = false;
                _MenuCut.Enabled = _MenuCopy.Enabled = _MenuDelete.Enabled = ((_TreeMain.SelectedNode != null) && (!(_TreeMain.SelectedNode.Tag is Repository)));
                _MenuPaste.Enabled = _CanPasteIntoTree();
            }
            else
            {
                _MenuUndo.Enabled = _MenuCut.Enabled = _MenuCopy.Enabled = _MenuPaste.Enabled = _MenuDelete.Enabled = false;
            }

            _MenuItems.Visible = false;
        }


        /// <summary>Menu "Edit" closed.</summary>
        private void _MenuEdit_DropDownClosed(object sender, EventArgs e)
        {
            _MenuUndo.Enabled = _MenuCut.Enabled = _MenuCopy.Enabled = _MenuPaste.Enabled = _MenuDelete.Enabled = true;
            _MenuItems.Visible = true;
        }


        /// <summary>Menu "Build" opening.</summary>
        private void _MenuBuildOrDeploy_DropDownOpening(object sender, EventArgs e)
        {
            _MenuBuild.Enabled = _MenuDeploy.Enabled = ((_TreeMain.SelectedNode != null)) &&
                                                        ((_TreeMain.SelectedNode.Tag is Repository) || (_TreeMain.SelectedNode.Tag is Solution) || (_TreeMain.SelectedNode.Tag is Package));
        }


        /// <summary>Menu "Build" closed.</summary>
        private void _MenuBuildOrDeploy_DropDownClosed(object sender, EventArgs e)
        {
            _MenuBuild.Enabled = _MenuDeploy.Enabled = true;
        }


        /// <summary>Menu "Purge Output Directory" click.</summary>
        private void _MenuPurgeOutput_Click(object sender, EventArgs e)
        {
            List<string> left = new List<string>();

            foreach(string i in Directory.GetDirectories(Program._Settings.OutputPath))
            {
                try
                {
                    Directory.Delete(i, true);
                }
                catch(Exception) { left.Add(Path.GetFileName(i) + @"\"); }
            }

            foreach(string i in Directory.GetFiles(Program._Settings.OutputPath))
            {
                try
                {
                    File.Delete(i);
                }
                catch(Exception) { left.Add(Path.GetFileName(i)); }
            }

            if(left.Count > 0)
            {
                string m = "Failed to delete:";
                foreach(string i in left) { m += "\r\n      " + i; }

                MessageBox.Show(m, "Purge Output Directory", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
    }
}
