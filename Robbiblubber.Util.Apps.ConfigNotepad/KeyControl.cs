﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Robbiblubber.Util.Localization;



namespace Robbiblubber.Util.Apps.ConfigNotepad
{
    /// <summary>Find control.</summary>
    internal partial class KeyControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>OK flag.</summary>
        private bool _OK = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public KeyControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the control pin state.</summary>
        public bool Pinned
        {
            get { return false; }
            set {}
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         
        /// <summary>Moves the current selection in the text box to the Clipboard.</summary>
        public void Cut()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Cut(); }
        }


        /// <summary>Copies the current selection in the text box to the Clipboard.</summary>
        public void Copy()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Copy(); }
        }


        /// <summary>Replaces the current selection in the text box with the contents of the Clipboard.</summary>
        public void Paste()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Paste(); }
        }


        /// <summary>Selects all text.</summary>
        public void SelectAll()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).SelectAll(); }
        }


        /// <summary>Deselects all text.</summary>
        public void DeselectAll()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).DeselectAll(); }
        }


        /// <summary>Shows the control and returns a dialog result.</summary>
        /// <returns>Returns the dialog result.</returns>
        public DialogResult ShowDialog()
        {
            Show();

            while(Visible)
            {
                Application.DoEvents();
            }

            return (_OK ? DialogResult.OK : DialogResult.Cancel);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Control                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the control text.</summary>
        [PasswordPropertyText]
        public override string Text
        {
            get { return _TextKey.Text; }
            set { _TextKey.Text = value; }
        }


        /// <summary>Shows the control.</summary>
        public new void Show()
        {
            base.Show();
            Focus();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Close" click.</summary>
        private void _ButtonClose_Click(object sender, EventArgs e)
        {
            _OK = false;
            Visible = false;
        }


        /// <summary>Button "Find" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            _OK = true;
            Visible = false;
        }


        /// <summary>Control key down.</summary>
        private void KeyControl_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if(_ButtonOK.Enabled) { _ButtonOK_Click(null, null); }
            }
            else if(e.KeyCode == Keys.Escape) { _ButtonClose_Click(null, null); }
        }
    }
}
