﻿namespace Robbiblubber.Util.Apps.ConfigNotepad
{
    partial class FormEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditor));
            this._MenuMain = new System.Windows.Forms.MenuStrip();
            this._MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuNew = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileSeparator0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuPageSetup = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuPrint = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFileSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuRecent = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuUndo = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditSeparator0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuFind = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFindNext = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuReplace = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGoTo = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFormat = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuWordWrap = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFont = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuConvert = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHex = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHexEncode = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHexDecode = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuBase64 = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuB64Encode = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuB64Decode = new System.Windows.Forms.ToolStripMenuItem();
            this.gZipHexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGzCompress = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGzCompressRaw = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGzCompressHex = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGzCompressB64 = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGzDecompress = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGzDecompressRaw = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGzDecompressHex = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuGzDecompressB64 = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFunnelCake = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFunnelEncrypt = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFunnelEncryptRaw = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFunnelEncryptHex = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFunnelEncryptB64 = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFunnelDecrypt = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFunnelDecryptRaw = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFunnelDecryptHex = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFunnelDecryptB64 = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTools = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStart = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStop = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebugSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugUpload = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuShowHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._TextEditor = new System.Windows.Forms.TextBox();
            this._Printer = new System.Drawing.Printing.PrintDocument();
            this._TimerStart = new System.Windows.Forms.Timer(this.components);
            this._GotoSearch = new Robbiblubber.Util.Controls.GoToControl();
            this._ReplaceSearch = new Robbiblubber.Util.Controls.ReplaceControl();
            this._FindSearch = new Robbiblubber.Util.Controls.FindControl();
            this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._ControlKey = new Robbiblubber.Util.Apps.ConfigNotepad.KeyControl();
            this._MenuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // _MenuMain
            // 
            this._MenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFile,
            this._MenuEdit,
            this._MenuFormat,
            this._MenuConvert,
            this._MenuTools,
            this._MenuHelp});
            this._MenuMain.Location = new System.Drawing.Point(0, 0);
            this._MenuMain.Name = "_MenuMain";
            this._MenuMain.Padding = new System.Windows.Forms.Padding(7, 3, 0, 3);
            this._MenuMain.Size = new System.Drawing.Size(1022, 25);
            this._MenuMain.TabIndex = 0;
            // 
            // _MenuFile
            // 
            this._MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuNew,
            this._MenuOpen,
            this._MenuSave,
            this._MenuSaveAs,
            this._MenuFileSeparator0,
            this._MenuPageSetup,
            this._MenuPrint,
            this._MenuFileSeparator1,
            this._MenuRecent,
            this._MenuExit});
            this._MenuFile.Name = "_MenuFile";
            this._MenuFile.Size = new System.Drawing.Size(37, 19);
            this._MenuFile.Tag = "cfgnpd::menu.file";
            this._MenuFile.Text = "&File";
            // 
            // _MenuNew
            // 
            this._MenuNew.Image = ((System.Drawing.Image)(resources.GetObject("_MenuNew.Image")));
            this._MenuNew.Name = "_MenuNew";
            this._MenuNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._MenuNew.Size = new System.Drawing.Size(215, 22);
            this._MenuNew.Tag = "cfgnpd::menu.new";
            this._MenuNew.Text = "&New";
            this._MenuNew.Click += new System.EventHandler(this._MenuNew_Click);
            // 
            // _MenuOpen
            // 
            this._MenuOpen.Image = ((System.Drawing.Image)(resources.GetObject("_MenuOpen.Image")));
            this._MenuOpen.Name = "_MenuOpen";
            this._MenuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._MenuOpen.Size = new System.Drawing.Size(215, 22);
            this._MenuOpen.Tag = "cfgnpd::menu.open";
            this._MenuOpen.Text = "&Open";
            this._MenuOpen.Click += new System.EventHandler(this._MenuOpen_Click);
            // 
            // _MenuSave
            // 
            this._MenuSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSave.Image")));
            this._MenuSave.Name = "_MenuSave";
            this._MenuSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._MenuSave.Size = new System.Drawing.Size(215, 22);
            this._MenuSave.Tag = "cfgnpd::menu.save";
            this._MenuSave.Text = "&Save";
            this._MenuSave.Click += new System.EventHandler(this._MenuSave_Click);
            // 
            // _MenuSaveAs
            // 
            this._MenuSaveAs.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSaveAs.Image")));
            this._MenuSaveAs.Name = "_MenuSaveAs";
            this._MenuSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this._MenuSaveAs.Size = new System.Drawing.Size(215, 22);
            this._MenuSaveAs.Tag = "cfgnpd::menu.saveas";
            this._MenuSaveAs.Text = "Save &As...";
            this._MenuSaveAs.Click += new System.EventHandler(this._MenuSaveAs_Click);
            // 
            // _MenuFileSeparator0
            // 
            this._MenuFileSeparator0.Name = "_MenuFileSeparator0";
            this._MenuFileSeparator0.Size = new System.Drawing.Size(212, 6);
            // 
            // _MenuPageSetup
            // 
            this._MenuPageSetup.Image = ((System.Drawing.Image)(resources.GetObject("_MenuPageSetup.Image")));
            this._MenuPageSetup.Name = "_MenuPageSetup";
            this._MenuPageSetup.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.P)));
            this._MenuPageSetup.Size = new System.Drawing.Size(215, 22);
            this._MenuPageSetup.Tag = "cfgnpd::menu.pagesetup";
            this._MenuPageSetup.Text = "Page Set&up...";
            this._MenuPageSetup.Click += new System.EventHandler(this._MenuPageSetup_Click);
            // 
            // _MenuPrint
            // 
            this._MenuPrint.Image = ((System.Drawing.Image)(resources.GetObject("_MenuPrint.Image")));
            this._MenuPrint.Name = "_MenuPrint";
            this._MenuPrint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this._MenuPrint.Size = new System.Drawing.Size(215, 22);
            this._MenuPrint.Tag = "cfgnpd::menu.print";
            this._MenuPrint.Text = "&Print...";
            this._MenuPrint.Click += new System.EventHandler(this._MenuPrint_Click);
            // 
            // _MenuFileSeparator1
            // 
            this._MenuFileSeparator1.Name = "_MenuFileSeparator1";
            this._MenuFileSeparator1.Size = new System.Drawing.Size(212, 6);
            // 
            // _MenuRecent
            // 
            this._MenuRecent.Name = "_MenuRecent";
            this._MenuRecent.Size = new System.Drawing.Size(215, 22);
            this._MenuRecent.Tag = "cfgnpd::menu.recent";
            this._MenuRecent.Text = "&Recent Files";
            // 
            // _MenuExit
            // 
            this._MenuExit.Image = ((System.Drawing.Image)(resources.GetObject("_MenuExit.Image")));
            this._MenuExit.Name = "_MenuExit";
            this._MenuExit.Size = new System.Drawing.Size(215, 22);
            this._MenuExit.Tag = "cfgnpd::menu.exit";
            this._MenuExit.Text = "E&xit";
            this._MenuExit.Click += new System.EventHandler(this._MenuExit_Click);
            // 
            // _MenuEdit
            // 
            this._MenuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuUndo,
            this._MenuEditSeparator0,
            this._MenuCut,
            this._MenuCopy,
            this._MenuPaste,
            this._MenuDelete,
            this._MenuEditSeparator1,
            this._MenuFind,
            this._MenuFindNext,
            this._MenuReplace,
            this._MenuGoTo,
            this._MenuEditSeparator2,
            this._MenuSelectAll});
            this._MenuEdit.Name = "_MenuEdit";
            this._MenuEdit.Size = new System.Drawing.Size(39, 19);
            this._MenuEdit.Tag = "cfgnpd::menu.edit";
            this._MenuEdit.Text = "&Edit";
            // 
            // _MenuUndo
            // 
            this._MenuUndo.Image = ((System.Drawing.Image)(resources.GetObject("_MenuUndo.Image")));
            this._MenuUndo.Name = "_MenuUndo";
            this._MenuUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this._MenuUndo.Size = new System.Drawing.Size(167, 22);
            this._MenuUndo.Tag = "cfgnpd::menu.undo";
            this._MenuUndo.Text = "&Undo";
            this._MenuUndo.Click += new System.EventHandler(this._MenuUndo_Click);
            // 
            // _MenuEditSeparator0
            // 
            this._MenuEditSeparator0.Name = "_MenuEditSeparator0";
            this._MenuEditSeparator0.Size = new System.Drawing.Size(164, 6);
            // 
            // _MenuCut
            // 
            this._MenuCut.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCut.Image")));
            this._MenuCut.Name = "_MenuCut";
            this._MenuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this._MenuCut.Size = new System.Drawing.Size(167, 22);
            this._MenuCut.Tag = "cfgnpd::menu.cut";
            this._MenuCut.Text = "Cu&t";
            this._MenuCut.Click += new System.EventHandler(this._MenuCut_Click);
            // 
            // _MenuCopy
            // 
            this._MenuCopy.Image = ((System.Drawing.Image)(resources.GetObject("_MenuCopy.Image")));
            this._MenuCopy.Name = "_MenuCopy";
            this._MenuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._MenuCopy.Size = new System.Drawing.Size(167, 22);
            this._MenuCopy.Tag = "cfgnpd::menu.copy";
            this._MenuCopy.Text = "&Copy";
            this._MenuCopy.Click += new System.EventHandler(this._MenuCopy_Click);
            // 
            // _MenuPaste
            // 
            this._MenuPaste.Image = ((System.Drawing.Image)(resources.GetObject("_MenuPaste.Image")));
            this._MenuPaste.Name = "_MenuPaste";
            this._MenuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this._MenuPaste.Size = new System.Drawing.Size(167, 22);
            this._MenuPaste.Tag = "cfgnpd::menu.paste";
            this._MenuPaste.Text = "&Paste";
            this._MenuPaste.Click += new System.EventHandler(this._MenuPaste_Click);
            // 
            // _MenuDelete
            // 
            this._MenuDelete.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDelete.Image")));
            this._MenuDelete.Name = "_MenuDelete";
            this._MenuDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Delete)));
            this._MenuDelete.Size = new System.Drawing.Size(167, 22);
            this._MenuDelete.Tag = "cfgnpd::menu.delete";
            this._MenuDelete.Text = "&Delete";
            this._MenuDelete.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // _MenuEditSeparator1
            // 
            this._MenuEditSeparator1.Name = "_MenuEditSeparator1";
            this._MenuEditSeparator1.Size = new System.Drawing.Size(164, 6);
            // 
            // _MenuFind
            // 
            this._MenuFind.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFind.Image")));
            this._MenuFind.Name = "_MenuFind";
            this._MenuFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this._MenuFind.Size = new System.Drawing.Size(167, 22);
            this._MenuFind.Tag = "cfgnpd::menu.find";
            this._MenuFind.Text = "&Find...";
            this._MenuFind.Click += new System.EventHandler(this._MenuFind_Click);
            // 
            // _MenuFindNext
            // 
            this._MenuFindNext.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFindNext.Image")));
            this._MenuFindNext.Name = "_MenuFindNext";
            this._MenuFindNext.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this._MenuFindNext.Size = new System.Drawing.Size(167, 22);
            this._MenuFindNext.Tag = "cfgnpd::menu.findnext";
            this._MenuFindNext.Text = "Find &Next";
            this._MenuFindNext.Click += new System.EventHandler(this._MenuFindNext_Click);
            // 
            // _MenuReplace
            // 
            this._MenuReplace.Image = ((System.Drawing.Image)(resources.GetObject("_MenuReplace.Image")));
            this._MenuReplace.Name = "_MenuReplace";
            this._MenuReplace.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this._MenuReplace.Size = new System.Drawing.Size(167, 22);
            this._MenuReplace.Tag = "cfgnpd::menu.replace";
            this._MenuReplace.Text = "&Replace...";
            this._MenuReplace.Click += new System.EventHandler(this._MenuReplace_Click);
            // 
            // _MenuGoTo
            // 
            this._MenuGoTo.Name = "_MenuGoTo";
            this._MenuGoTo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this._MenuGoTo.Size = new System.Drawing.Size(167, 22);
            this._MenuGoTo.Tag = "cfgnpd::menu.goto";
            this._MenuGoTo.Text = "&Go To...";
            this._MenuGoTo.Click += new System.EventHandler(this._MenuGoTo_Click);
            // 
            // _MenuEditSeparator2
            // 
            this._MenuEditSeparator2.Name = "_MenuEditSeparator2";
            this._MenuEditSeparator2.Size = new System.Drawing.Size(164, 6);
            // 
            // _MenuSelectAll
            // 
            this._MenuSelectAll.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSelectAll.Image")));
            this._MenuSelectAll.Name = "_MenuSelectAll";
            this._MenuSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this._MenuSelectAll.Size = new System.Drawing.Size(167, 22);
            this._MenuSelectAll.Tag = "cfgnpd::menu.selectall";
            this._MenuSelectAll.Text = "Select &All";
            this._MenuSelectAll.Click += new System.EventHandler(this._MenuSelectAll_Click);
            // 
            // _MenuFormat
            // 
            this._MenuFormat.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuWordWrap,
            this._MenuFont});
            this._MenuFormat.Name = "_MenuFormat";
            this._MenuFormat.Size = new System.Drawing.Size(57, 19);
            this._MenuFormat.Tag = "cfgnpd::menu.format";
            this._MenuFormat.Text = "F&ormat";
            // 
            // _MenuWordWrap
            // 
            this._MenuWordWrap.Name = "_MenuWordWrap";
            this._MenuWordWrap.Size = new System.Drawing.Size(134, 22);
            this._MenuWordWrap.Tag = "cfgnpd::menu.wordwrap";
            this._MenuWordWrap.Text = "&Word Wrap";
            this._MenuWordWrap.Click += new System.EventHandler(this._MenuWordWrap_Click);
            // 
            // _MenuFont
            // 
            this._MenuFont.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFont.Image")));
            this._MenuFont.Name = "_MenuFont";
            this._MenuFont.Size = new System.Drawing.Size(134, 22);
            this._MenuFont.Tag = "cfgnpd::menu.font";
            this._MenuFont.Text = "&Font...";
            this._MenuFont.Click += new System.EventHandler(this._MenuFont_Click);
            // 
            // _MenuConvert
            // 
            this._MenuConvert.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuHex,
            this._MenuBase64,
            this.gZipHexToolStripMenuItem,
            this._MenuFunnelCake});
            this._MenuConvert.Name = "_MenuConvert";
            this._MenuConvert.Size = new System.Drawing.Size(61, 19);
            this._MenuConvert.Tag = "cfgnpd::menu.convert";
            this._MenuConvert.Text = "&Convert";
            // 
            // _MenuHex
            // 
            this._MenuHex.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuHexEncode,
            this._MenuHexDecode});
            this._MenuHex.Image = ((System.Drawing.Image)(resources.GetObject("_MenuHex.Image")));
            this._MenuHex.Name = "_MenuHex";
            this._MenuHex.Size = new System.Drawing.Size(180, 22);
            this._MenuHex.Tag = "cfgnpd::menu.hex";
            this._MenuHex.Text = "&Hexadecimal";
            // 
            // _MenuHexEncode
            // 
            this._MenuHexEncode.Image = ((System.Drawing.Image)(resources.GetObject("_MenuHexEncode.Image")));
            this._MenuHexEncode.Name = "_MenuHexEncode";
            this._MenuHexEncode.ShortcutKeys = ((System.Windows.Forms.Keys)((((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.H)));
            this._MenuHexEncode.Size = new System.Drawing.Size(211, 22);
            this._MenuHexEncode.Tag = "cfgnpd::menu.encode";
            this._MenuHexEncode.Text = "&Encode";
            this._MenuHexEncode.Click += new System.EventHandler(this._MenuHexEncode_Click);
            // 
            // _MenuHexDecode
            // 
            this._MenuHexDecode.Image = ((System.Drawing.Image)(resources.GetObject("_MenuHexDecode.Image")));
            this._MenuHexDecode.Name = "_MenuHexDecode";
            this._MenuHexDecode.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.H)));
            this._MenuHexDecode.Size = new System.Drawing.Size(211, 22);
            this._MenuHexDecode.Tag = "cfgnpd::menu.decode";
            this._MenuHexDecode.Text = "&Decode";
            this._MenuHexDecode.Click += new System.EventHandler(this._MenuHexDecode_Click);
            // 
            // _MenuBase64
            // 
            this._MenuBase64.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuB64Encode,
            this._MenuB64Decode});
            this._MenuBase64.Image = ((System.Drawing.Image)(resources.GetObject("_MenuBase64.Image")));
            this._MenuBase64.Name = "_MenuBase64";
            this._MenuBase64.Size = new System.Drawing.Size(180, 22);
            this._MenuBase64.Tag = "cfgnpd::menu.base64";
            this._MenuBase64.Text = "&Base64";
            // 
            // _MenuB64Encode
            // 
            this._MenuB64Encode.Image = ((System.Drawing.Image)(resources.GetObject("_MenuB64Encode.Image")));
            this._MenuB64Encode.Name = "_MenuB64Encode";
            this._MenuB64Encode.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.B)));
            this._MenuB64Encode.Size = new System.Drawing.Size(186, 22);
            this._MenuB64Encode.Tag = "cfgnpd::menu.encode";
            this._MenuB64Encode.Text = "&Encode";
            this._MenuB64Encode.Click += new System.EventHandler(this._MenuB64Encode_Click);
            // 
            // _MenuB64Decode
            // 
            this._MenuB64Decode.Image = ((System.Drawing.Image)(resources.GetObject("_MenuB64Decode.Image")));
            this._MenuB64Decode.Name = "_MenuB64Decode";
            this._MenuB64Decode.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.B)));
            this._MenuB64Decode.Size = new System.Drawing.Size(186, 22);
            this._MenuB64Decode.Tag = "cfgnpd::menu.decode";
            this._MenuB64Decode.Text = "&Decode";
            this._MenuB64Decode.Click += new System.EventHandler(this._MenuB64Decode_Click);
            // 
            // gZipHexToolStripMenuItem
            // 
            this.gZipHexToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuGzCompress,
            this._MenuGzDecompress});
            this.gZipHexToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("gZipHexToolStripMenuItem.Image")));
            this.gZipHexToolStripMenuItem.Name = "gZipHexToolStripMenuItem";
            this.gZipHexToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.gZipHexToolStripMenuItem.Tag = "cfgnpd::menu.gz";
            this.gZipHexToolStripMenuItem.Text = "&GZip";
            // 
            // _MenuGzCompress
            // 
            this._MenuGzCompress.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuGzCompressRaw,
            this._MenuGzCompressHex,
            this._MenuGzCompressB64});
            this._MenuGzCompress.Image = ((System.Drawing.Image)(resources.GetObject("_MenuGzCompress.Image")));
            this._MenuGzCompress.Name = "_MenuGzCompress";
            this._MenuGzCompress.Size = new System.Drawing.Size(180, 22);
            this._MenuGzCompress.Tag = "cfgnpd::menu.compress";
            this._MenuGzCompress.Text = "&Compress";
            // 
            // _MenuGzCompressRaw
            // 
            this._MenuGzCompressRaw.Image = ((System.Drawing.Image)(resources.GetObject("_MenuGzCompressRaw.Image")));
            this._MenuGzCompressRaw.Name = "_MenuGzCompressRaw";
            this._MenuGzCompressRaw.ShortcutKeys = ((System.Windows.Forms.Keys)((((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.R)));
            this._MenuGzCompressRaw.Size = new System.Drawing.Size(239, 22);
            this._MenuGzCompressRaw.Tag = "cfgnpd::menu.raw";
            this._MenuGzCompressRaw.Text = "&Raw";
            this._MenuGzCompressRaw.Click += new System.EventHandler(this._MenuGzCompressRaw_Click);
            // 
            // _MenuGzCompressHex
            // 
            this._MenuGzCompressHex.Image = ((System.Drawing.Image)(resources.GetObject("_MenuGzCompressHex.Image")));
            this._MenuGzCompressHex.Name = "_MenuGzCompressHex";
            this._MenuGzCompressHex.ShortcutKeys = ((System.Windows.Forms.Keys)((((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.X)));
            this._MenuGzCompressHex.Size = new System.Drawing.Size(239, 22);
            this._MenuGzCompressHex.Tag = "cfgnpd::menu.hex";
            this._MenuGzCompressHex.Text = "&Hexadecimal";
            this._MenuGzCompressHex.Click += new System.EventHandler(this._MenuGzCompressHex_Click);
            // 
            // _MenuGzCompressB64
            // 
            this._MenuGzCompressB64.Image = ((System.Drawing.Image)(resources.GetObject("_MenuGzCompressB64.Image")));
            this._MenuGzCompressB64.Name = "_MenuGzCompressB64";
            this._MenuGzCompressB64.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.K)));
            this._MenuGzCompressB64.Size = new System.Drawing.Size(239, 22);
            this._MenuGzCompressB64.Tag = "cfgnpd::menu.base64";
            this._MenuGzCompressB64.Text = "&Base64";
            this._MenuGzCompressB64.Click += new System.EventHandler(this._MenuGzCompressB64_Click);
            // 
            // _MenuGzDecompress
            // 
            this._MenuGzDecompress.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuGzDecompressRaw,
            this._MenuGzDecompressHex,
            this._MenuGzDecompressB64});
            this._MenuGzDecompress.Image = ((System.Drawing.Image)(resources.GetObject("_MenuGzDecompress.Image")));
            this._MenuGzDecompress.Name = "_MenuGzDecompress";
            this._MenuGzDecompress.Size = new System.Drawing.Size(180, 22);
            this._MenuGzDecompress.Tag = "cfgnpd::menu.decompress";
            this._MenuGzDecompress.Text = "&Decompress";
            // 
            // _MenuGzDecompressRaw
            // 
            this._MenuGzDecompressRaw.Image = ((System.Drawing.Image)(resources.GetObject("_MenuGzDecompressRaw.Image")));
            this._MenuGzDecompressRaw.Name = "_MenuGzDecompressRaw";
            this._MenuGzDecompressRaw.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
            this._MenuGzDecompressRaw.Size = new System.Drawing.Size(207, 22);
            this._MenuGzDecompressRaw.Tag = "cfgnpd::menu.raw";
            this._MenuGzDecompressRaw.Text = "&Raw";
            this._MenuGzDecompressRaw.Click += new System.EventHandler(this._MenuGzDecompressRaw_Click);
            // 
            // _MenuGzDecompressHex
            // 
            this._MenuGzDecompressHex.Image = ((System.Drawing.Image)(resources.GetObject("_MenuGzDecompressHex.Image")));
            this._MenuGzDecompressHex.Name = "_MenuGzDecompressHex";
            this._MenuGzDecompressHex.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.X)));
            this._MenuGzDecompressHex.Size = new System.Drawing.Size(207, 22);
            this._MenuGzDecompressHex.Tag = "cfgnpd::menu.hex";
            this._MenuGzDecompressHex.Text = "&Hexadecimal";
            this._MenuGzDecompressHex.Click += new System.EventHandler(this._MenuGzDecompressHex_Click);
            // 
            // _MenuGzDecompressB64
            // 
            this._MenuGzDecompressB64.Image = ((System.Drawing.Image)(resources.GetObject("_MenuGzDecompressB64.Image")));
            this._MenuGzDecompressB64.Name = "_MenuGzDecompressB64";
            this._MenuGzDecompressB64.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
            this._MenuGzDecompressB64.Size = new System.Drawing.Size(207, 22);
            this._MenuGzDecompressB64.Tag = "cfgnpd::menu.base64";
            this._MenuGzDecompressB64.Text = "&Base64";
            this._MenuGzDecompressB64.Click += new System.EventHandler(this._MenuGzDecompressB64_Click);
            // 
            // _MenuFunnelCake
            // 
            this._MenuFunnelCake.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFunnelEncrypt,
            this._MenuFunnelDecrypt});
            this._MenuFunnelCake.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFunnelCake.Image")));
            this._MenuFunnelCake.Name = "_MenuFunnelCake";
            this._MenuFunnelCake.Size = new System.Drawing.Size(180, 22);
            this._MenuFunnelCake.Tag = "cfgnpd::menu.funnelcake";
            this._MenuFunnelCake.Text = "&FunnelCake";
            // 
            // _MenuFunnelEncrypt
            // 
            this._MenuFunnelEncrypt.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFunnelEncryptRaw,
            this._MenuFunnelEncryptHex,
            this._MenuFunnelEncryptB64});
            this._MenuFunnelEncrypt.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFunnelEncrypt.Image")));
            this._MenuFunnelEncrypt.Name = "_MenuFunnelEncrypt";
            this._MenuFunnelEncrypt.Size = new System.Drawing.Size(180, 22);
            this._MenuFunnelEncrypt.Tag = "cfgnpd::menu.encrypt";
            this._MenuFunnelEncrypt.Text = "&Encrypt";
            // 
            // _MenuFunnelEncryptRaw
            // 
            this._MenuFunnelEncryptRaw.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFunnelEncryptRaw.Image")));
            this._MenuFunnelEncryptRaw.Name = "_MenuFunnelEncryptRaw";
            this._MenuFunnelEncryptRaw.ShortcutKeys = ((System.Windows.Forms.Keys)((((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.L)));
            this._MenuFunnelEncryptRaw.Size = new System.Drawing.Size(238, 22);
            this._MenuFunnelEncryptRaw.Tag = "cfgnpd::menu.raw";
            this._MenuFunnelEncryptRaw.Text = "&Raw";
            this._MenuFunnelEncryptRaw.Click += new System.EventHandler(this._MenuFunnelEncryptRaw_Click);
            // 
            // _MenuFunnelEncryptHex
            // 
            this._MenuFunnelEncryptHex.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFunnelEncryptHex.Image")));
            this._MenuFunnelEncryptHex.Name = "_MenuFunnelEncryptHex";
            this._MenuFunnelEncryptHex.ShortcutKeys = ((System.Windows.Forms.Keys)((((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.F)));
            this._MenuFunnelEncryptHex.Size = new System.Drawing.Size(238, 22);
            this._MenuFunnelEncryptHex.Tag = "cfgnpd::menu.hex";
            this._MenuFunnelEncryptHex.Text = "&Hexadecimal";
            this._MenuFunnelEncryptHex.Click += new System.EventHandler(this._MenuFunnelEncryptHex_Click);
            // 
            // _MenuFunnelEncryptB64
            // 
            this._MenuFunnelEncryptB64.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFunnelEncryptB64.Image")));
            this._MenuFunnelEncryptB64.Name = "_MenuFunnelEncryptB64";
            this._MenuFunnelEncryptB64.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.L)));
            this._MenuFunnelEncryptB64.Size = new System.Drawing.Size(238, 22);
            this._MenuFunnelEncryptB64.Tag = "cfgnpd::menu.base64";
            this._MenuFunnelEncryptB64.Text = "&Base64";
            this._MenuFunnelEncryptB64.Click += new System.EventHandler(this._MenuFunnelEncryptB64_Click);
            // 
            // _MenuFunnelDecrypt
            // 
            this._MenuFunnelDecrypt.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFunnelDecryptRaw,
            this._MenuFunnelDecryptHex,
            this._MenuFunnelDecryptB64});
            this._MenuFunnelDecrypt.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFunnelDecrypt.Image")));
            this._MenuFunnelDecrypt.Name = "_MenuFunnelDecrypt";
            this._MenuFunnelDecrypt.Size = new System.Drawing.Size(180, 22);
            this._MenuFunnelDecrypt.Tag = "cfgnpd::menu.decrypt";
            this._MenuFunnelDecrypt.Text = "&Decrypt";
            // 
            // _MenuFunnelDecryptRaw
            // 
            this._MenuFunnelDecryptRaw.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFunnelDecryptRaw.Image")));
            this._MenuFunnelDecryptRaw.Name = "_MenuFunnelDecryptRaw";
            this._MenuFunnelDecryptRaw.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.L)));
            this._MenuFunnelDecryptRaw.Size = new System.Drawing.Size(206, 22);
            this._MenuFunnelDecryptRaw.Tag = "cfgnpd::menu.raw";
            this._MenuFunnelDecryptRaw.Text = "&Raw";
            this._MenuFunnelDecryptRaw.Click += new System.EventHandler(this._MenuFunnelDecryptRaw_Click);
            // 
            // _MenuFunnelDecryptHex
            // 
            this._MenuFunnelDecryptHex.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFunnelDecryptHex.Image")));
            this._MenuFunnelDecryptHex.Name = "_MenuFunnelDecryptHex";
            this._MenuFunnelDecryptHex.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.F)));
            this._MenuFunnelDecryptHex.Size = new System.Drawing.Size(206, 22);
            this._MenuFunnelDecryptHex.Tag = "cfgnpd::menu.hex";
            this._MenuFunnelDecryptHex.Text = "&Hexadecimal";
            this._MenuFunnelDecryptHex.Click += new System.EventHandler(this._MenuFunnelDecryptHex_Click);
            // 
            // _MenuFunnelDecryptB64
            // 
            this._MenuFunnelDecryptB64.Image = ((System.Drawing.Image)(resources.GetObject("_MenuFunnelDecryptB64.Image")));
            this._MenuFunnelDecryptB64.Name = "_MenuFunnelDecryptB64";
            this._MenuFunnelDecryptB64.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this._MenuFunnelDecryptB64.Size = new System.Drawing.Size(206, 22);
            this._MenuFunnelDecryptB64.Tag = "cfgnpd::menu.base64";
            this._MenuFunnelDecryptB64.Text = "&Base64";
            this._MenuFunnelDecryptB64.Click += new System.EventHandler(this._MenuFunnelDecryptB64_Click);
            // 
            // _MenuTools
            // 
            this._MenuTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebug,
            this._MenuSettings});
            this._MenuTools.Name = "_MenuTools";
            this._MenuTools.Size = new System.Drawing.Size(46, 19);
            this._MenuTools.Tag = "cfgnpd::menu.tools";
            this._MenuTools.Text = "&Tools";
            // 
            // _MenuDebug
            // 
            this._MenuDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebugStart,
            this._MenuDebugStop,
            this._MenuDebugBlank0,
            this._MenuDebugSave,
            this._MenuDebugView,
            this._MenuDebugUpload});
            this._MenuDebug.Name = "_MenuDebug";
            this._MenuDebug.Size = new System.Drawing.Size(125, 22);
            this._MenuDebug.Tag = "debugx::menu.debug";
            this._MenuDebug.Text = "&Debug";
            this._MenuDebug.DropDownClosed += new System.EventHandler(this._MenuDebug_DropDownClosed);
            this._MenuDebug.DropDownOpening += new System.EventHandler(this._MenuDebug_DropDownOpening);
            // 
            // _MenuDebugStart
            // 
            this._MenuDebugStart.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStart.Image")));
            this._MenuDebugStart.Name = "_MenuDebugStart";
            this._MenuDebugStart.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStart.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStart.Tag = "debugx::menu.startlogging";
            this._MenuDebugStart.Text = "Start &Logging";
            this._MenuDebugStart.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugStop
            // 
            this._MenuDebugStop.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStop.Image")));
            this._MenuDebugStop.Name = "_MenuDebugStop";
            this._MenuDebugStop.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStop.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStop.Tag = "debugx::menu.stoplogging";
            this._MenuDebugStop.Text = "Stop &Logging";
            this._MenuDebugStop.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugBlank0
            // 
            this._MenuDebugBlank0.Name = "_MenuDebugBlank0";
            this._MenuDebugBlank0.Size = new System.Drawing.Size(219, 6);
            // 
            // _MenuDebugSave
            // 
            this._MenuDebugSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugSave.Image")));
            this._MenuDebugSave.Name = "_MenuDebugSave";
            this._MenuDebugSave.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D)));
            this._MenuDebugSave.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugSave.Tag = "debugx::menu.savedump";
            this._MenuDebugSave.Text = "&Save Dump...";
            this._MenuDebugSave.Click += new System.EventHandler(this._MenuDebugSave_Click);
            // 
            // _MenuDebugView
            // 
            this._MenuDebugView.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugView.Image")));
            this._MenuDebugView.Name = "_MenuDebugView";
            this._MenuDebugView.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.V)));
            this._MenuDebugView.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugView.Tag = "debugx::menu.viewdump";
            this._MenuDebugView.Text = "&View Dump...";
            this._MenuDebugView.Click += new System.EventHandler(this._MenuDebugView_Click);
            // 
            // _MenuDebugUpload
            // 
            this._MenuDebugUpload.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugUpload.Image")));
            this._MenuDebugUpload.Name = "_MenuDebugUpload";
            this._MenuDebugUpload.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.U)));
            this._MenuDebugUpload.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugUpload.Tag = "debugx::menu.uploaddump";
            this._MenuDebugUpload.Text = "&Upload Dump...";
            this._MenuDebugUpload.Click += new System.EventHandler(this._MenuDebugUpload_Click);
            // 
            // _MenuSettings
            // 
            this._MenuSettings.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSettings.Image")));
            this._MenuSettings.Name = "_MenuSettings";
            this._MenuSettings.Size = new System.Drawing.Size(125, 22);
            this._MenuSettings.Tag = "cfgnpd::menu.settings";
            this._MenuSettings.Text = "&Settings...";
            this._MenuSettings.Click += new System.EventHandler(this._MenuSettings_Click);
            // 
            // _MenuHelp
            // 
            this._MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuShowHelp,
            this._MenuAbout});
            this._MenuHelp.Name = "_MenuHelp";
            this._MenuHelp.Size = new System.Drawing.Size(44, 19);
            this._MenuHelp.Tag = "cfgnpd::menu.help";
            this._MenuHelp.Text = "&Help";
            // 
            // _MenuShowHelp
            // 
            this._MenuShowHelp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuShowHelp.Image")));
            this._MenuShowHelp.Name = "_MenuShowHelp";
            this._MenuShowHelp.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this._MenuShowHelp.Size = new System.Drawing.Size(159, 22);
            this._MenuShowHelp.Tag = "cfgnpd::menu.showhelp";
            this._MenuShowHelp.Text = "Show &Help...";
            this._MenuShowHelp.Click += new System.EventHandler(this._MenuShowHelp_Click);
            // 
            // _MenuAbout
            // 
            this._MenuAbout.Name = "_MenuAbout";
            this._MenuAbout.Size = new System.Drawing.Size(159, 22);
            this._MenuAbout.Tag = "cfgnpd::menu.about";
            this._MenuAbout.Text = "&About...";
            this._MenuAbout.Click += new System.EventHandler(this._MenuAbout_Click);
            // 
            // _TextEditor
            // 
            this._TextEditor.AcceptsReturn = true;
            this._TextEditor.AcceptsTab = true;
            this._TextEditor.AllowDrop = true;
            this._TextEditor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._TextEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this._TextEditor.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TextEditor.HideSelection = false;
            this._TextEditor.Location = new System.Drawing.Point(0, 25);
            this._TextEditor.Multiline = true;
            this._TextEditor.Name = "_TextEditor";
            this._TextEditor.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._TextEditor.Size = new System.Drawing.Size(1022, 519);
            this._TextEditor.TabIndex = 0;
            this._TextEditor.WordWrap = false;
            // 
            // _Printer
            // 
            this._Printer.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this._Printer_PrintPage);
            // 
            // _TimerStart
            // 
            this._TimerStart.Tick += new System.EventHandler(this._TimerStart_Tick);
            // 
            // _GotoSearch
            // 
            this._GotoSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._GotoSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._GotoSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._GotoSearch.Line = 1;
            this._GotoSearch.Location = new System.Drawing.Point(537, 29);
            this._GotoSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._GotoSearch.Name = "_GotoSearch";
            this._GotoSearch.Pinned = false;
            this._GotoSearch.Size = new System.Drawing.Size(452, 125);
            this._GotoSearch.TabIndex = 9;
            this._GotoSearch.Visible = false;
            this._GotoSearch.GoTo += new System.EventHandler(this._GotoSearch_GoTo);
            this._GotoSearch.LineChanged += new Robbiblubber.Util.Controls.GoToFeedbackEventHandler(this._GotoSearch_LineChanged);
            // 
            // _ReplaceSearch
            // 
            this._ReplaceSearch.AllowReplace = true;
            this._ReplaceSearch.AllowReplaceAll = true;
            this._ReplaceSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._ReplaceSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ReplaceSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ReplaceSearch.Location = new System.Drawing.Point(537, 29);
            this._ReplaceSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._ReplaceSearch.Name = "_ReplaceSearch";
            this._ReplaceSearch.Pinned = false;
            this._ReplaceSearch.ReplaceText = "";
            this._ReplaceSearch.SearchText = "";
            this._ReplaceSearch.Size = new System.Drawing.Size(452, 181);
            this._ReplaceSearch.TabIndex = 7;
            this._ReplaceSearch.Visible = false;
            this._ReplaceSearch.SearchTextChanged += new System.EventHandler(this._ReplaceSearch_SearchTextChanged);
            this._ReplaceSearch.Search += new System.EventHandler(this._FindSearch_Search);
            this._ReplaceSearch.Replace += new System.EventHandler(this._ReplaceSearch_Replace);
            this._ReplaceSearch.ReplaceAll += new System.EventHandler(this._ReplaceSearch_ReplaceAll);
            // 
            // _FindSearch
            // 
            this._FindSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._FindSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FindSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._FindSearch.Location = new System.Drawing.Point(537, 29);
            this._FindSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._FindSearch.Name = "_FindSearch";
            this._FindSearch.Pinned = false;
            this._FindSearch.SearchText = "";
            this._FindSearch.Size = new System.Drawing.Size(452, 125);
            this._FindSearch.TabIndex = 6;
            this._FindSearch.Visible = false;
            this._FindSearch.Search += new System.EventHandler(this._FindSearch_Search);
            // 
            // _ControlKey
            // 
            this._ControlKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ControlKey.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ControlKey.Location = new System.Drawing.Point(537, 29);
            this._ControlKey.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._ControlKey.Name = "_ControlKey";
            this._ControlKey.Pinned = false;
            this._ControlKey.Size = new System.Drawing.Size(452, 125);
            this._ControlKey.TabIndex = 8;
            this._ControlKey.Visible = false;
            // 
            // FormEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 544);
            this.Controls.Add(this._GotoSearch);
            this.Controls.Add(this._ControlKey);
            this.Controls.Add(this._ReplaceSearch);
            this.Controls.Add(this._FindSearch);
            this.Controls.Add(this._TextEditor);
            this.Controls.Add(this._MenuMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._MenuMain;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormEditor";
            this.Text = "ConfigNotepad";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormEditor_FormClosing);
            this._MenuMain.ResumeLayout(false);
            this._MenuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _MenuMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuFile;
        private System.Windows.Forms.ToolStripMenuItem _MenuNew;
        private System.Windows.Forms.ToolStripMenuItem _MenuOpen;
        private System.Windows.Forms.ToolStripMenuItem _MenuSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuSaveAs;
        private System.Windows.Forms.ToolStripSeparator _MenuFileSeparator0;
        private System.Windows.Forms.ToolStripMenuItem _MenuPageSetup;
        private System.Windows.Forms.ToolStripMenuItem _MenuPrint;
        private System.Windows.Forms.ToolStripSeparator _MenuFileSeparator1;
        private System.Windows.Forms.ToolStripMenuItem _MenuExit;
        private System.Windows.Forms.ToolStripMenuItem _MenuEdit;
        private System.Windows.Forms.ToolStripMenuItem _MenuUndo;
        private System.Windows.Forms.ToolStripSeparator _MenuEditSeparator0;
        private System.Windows.Forms.ToolStripMenuItem _MenuCut;
        private System.Windows.Forms.ToolStripMenuItem _MenuCopy;
        private System.Windows.Forms.ToolStripMenuItem _MenuPaste;
        private System.Windows.Forms.ToolStripMenuItem _MenuDelete;
        private System.Windows.Forms.ToolStripSeparator _MenuEditSeparator1;
        private System.Windows.Forms.ToolStripMenuItem _MenuFind;
        private System.Windows.Forms.ToolStripMenuItem _MenuFindNext;
        private System.Windows.Forms.ToolStripMenuItem _MenuReplace;
        private System.Windows.Forms.ToolStripMenuItem _MenuGoTo;
        private System.Windows.Forms.ToolStripSeparator _MenuEditSeparator2;
        private System.Windows.Forms.ToolStripMenuItem _MenuSelectAll;
        private System.Windows.Forms.ToolStripMenuItem _MenuFormat;
        private System.Windows.Forms.ToolStripMenuItem _MenuWordWrap;
        private System.Windows.Forms.ToolStripMenuItem _MenuFont;
        private System.Windows.Forms.ToolStripMenuItem _MenuHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuShowHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuAbout;
        private System.Windows.Forms.TextBox _TextEditor;
        private System.Drawing.Printing.PrintDocument _Printer;
        private Controls.FindControl _FindSearch;
        private Controls.ReplaceControl _ReplaceSearch;
        private KeyControl _ControlKey;
        private Controls.GoToControl _GotoSearch;
        private System.Windows.Forms.Timer _TimerStart;
        private System.Windows.Forms.ToolStripMenuItem _MenuConvert;
        private System.Windows.Forms.ToolStripMenuItem _MenuHex;
        private System.Windows.Forms.ToolStripMenuItem _MenuHexEncode;
        private System.Windows.Forms.ToolStripMenuItem _MenuHexDecode;
        private System.Windows.Forms.ToolStripMenuItem _MenuBase64;
        private System.Windows.Forms.ToolStripMenuItem _MenuB64Encode;
        private System.Windows.Forms.ToolStripMenuItem _MenuB64Decode;
        private System.Windows.Forms.ToolStripMenuItem _MenuFunnelCake;
        private System.Windows.Forms.ToolStripMenuItem _MenuFunnelEncrypt;
        private System.Windows.Forms.ToolStripMenuItem _MenuFunnelDecrypt;
        private System.Windows.Forms.ToolStripMenuItem gZipHexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _MenuGzCompress;
        private System.Windows.Forms.ToolStripMenuItem _MenuGzCompressRaw;
        private System.Windows.Forms.ToolStripMenuItem _MenuGzCompressHex;
        private System.Windows.Forms.ToolStripMenuItem _MenuGzCompressB64;
        private System.Windows.Forms.ToolStripMenuItem _MenuGzDecompress;
        private System.Windows.Forms.ToolStripMenuItem _MenuGzDecompressRaw;
        private System.Windows.Forms.ToolStripMenuItem _MenuGzDecompressHex;
        private System.Windows.Forms.ToolStripMenuItem _MenuGzDecompressB64;
        private System.Windows.Forms.ToolStripMenuItem _MenuFunnelEncryptRaw;
        private System.Windows.Forms.ToolStripMenuItem _MenuFunnelEncryptHex;
        private System.Windows.Forms.ToolStripMenuItem _MenuFunnelEncryptB64;
        private System.Windows.Forms.ToolStripMenuItem _MenuFunnelDecryptRaw;
        private System.Windows.Forms.ToolStripMenuItem _MenuFunnelDecryptHex;
        private System.Windows.Forms.ToolStripMenuItem _MenuFunnelDecryptB64;
        private System.Windows.Forms.ToolStripMenuItem _MenuRecent;
        private System.Windows.Forms.ToolStripMenuItem _MenuTools;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebug;
        private System.Windows.Forms.ToolStripMenuItem _MenuSettings;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStart;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStop;
        private System.Windows.Forms.ToolStripSeparator _MenuDebugBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugView;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugUpload;
        private System.Windows.Forms.ToolTip ToolTip;
    }
}

