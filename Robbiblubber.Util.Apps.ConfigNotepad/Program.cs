﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Apps.ConfigNotepad
{
    /// <summary>Program class.</summary>
    internal static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Splash window.</summary>
        internal static FormSplash __Splash;

        /// <summary>Main window.</summary>
        internal static FormEditor __Main;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Main entry point                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        /// <param name="argv">Arguments.</param>
        [STAThread]
        internal static void Main(string[] argv)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            __Splash = new FormSplash();
            __Splash.Show();
            Application.DoEvents();

            string file = null;

            foreach(string i in argv) { file = i; }

            if(file == null)
            {
                __Main = new FormEditor();
            }
            else
            {
                __Main = new FormEditor(file, true);
            }

            __Splash.DelayClose();

            Application.Run(__Main);
        }
    }
}
