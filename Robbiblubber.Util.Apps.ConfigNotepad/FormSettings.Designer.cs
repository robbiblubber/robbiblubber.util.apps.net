﻿namespace Robbiblubber.Util.Apps.ConfigNotepad
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._IlistSettings = new System.Windows.Forms.ImageList(this.components);
            this._LcboLanguage = new Robbiblubber.Util.Localization.Controls.LocaleComboBox();
            this._LabelLanguage = new System.Windows.Forms.Label();
            this._ChkDebug = new System.Windows.Forms.CheckBox();
            this._LabelEncoding = new System.Windows.Forms.Label();
            this._CboEncoding = new Robbiblubber.Util.Controls.RichComboBox();
            this._LabelCustom = new System.Windows.Forms.Label();
            this._TextCustom = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(357, 228);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(125, 28);
            this._ButtonCancel.TabIndex = 5;
            this._ButtonCancel.Tag = "cfgnpd::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(226, 228);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(125, 28);
            this._ButtonOK.TabIndex = 4;
            this._ButtonOK.Tag = "cfgnpd::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _IlistSettings
            // 
            this._IlistSettings.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistSettings.ImageStream")));
            this._IlistSettings.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistSettings.Images.SetKeyName(0, "ascii");
            this._IlistSettings.Images.SetKeyName(1, "utf8");
            this._IlistSettings.Images.SetKeyName(2, "custom");
            // 
            // _LcboLanguage
            // 
            this._LcboLanguage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LcboLanguage.Location = new System.Drawing.Point(38, 101);
            this._LcboLanguage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LcboLanguage.Name = "_LcboLanguage";
            this._LcboLanguage.Size = new System.Drawing.Size(444, 31);
            this._LcboLanguage.TabIndex = 2;
            this._LcboLanguage.Value = null;
            // 
            // _LabelLanguage
            // 
            this._LabelLanguage.AutoSize = true;
            this._LabelLanguage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLanguage.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLanguage.Location = new System.Drawing.Point(35, 82);
            this._LabelLanguage.Name = "_LabelLanguage";
            this._LabelLanguage.Size = new System.Drawing.Size(135, 13);
            this._LabelLanguage.TabIndex = 2;
            this._LabelLanguage.Tag = "cfgnpd::udiag.settings.language";
            this._LabelLanguage.Text = "User Interface &Language:";
            // 
            // _ChkDebug
            // 
            this._ChkDebug.AutoSize = true;
            this._ChkDebug.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._ChkDebug.Location = new System.Drawing.Point(38, 170);
            this._ChkDebug.Name = "_ChkDebug";
            this._ChkDebug.Size = new System.Drawing.Size(372, 21);
            this._ChkDebug.TabIndex = 3;
            this._ChkDebug.Tag = "debugx::settings.showdebug";
            this._ChkDebug.Text = "   Enable &debug features (show Debug menu in menu strip)";
            this._ChkDebug.UseVisualStyleBackColor = true;
            // 
            // _LabelEncoding
            // 
            this._LabelEncoding.AutoSize = true;
            this._LabelEncoding.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelEncoding.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelEncoding.Location = new System.Drawing.Point(35, 28);
            this._LabelEncoding.Name = "_LabelEncoding";
            this._LabelEncoding.Size = new System.Drawing.Size(81, 13);
            this._LabelEncoding.TabIndex = 0;
            this._LabelEncoding.Tag = "cfgnpd::udiag.settings.encoding";
            this._LabelEncoding.Text = "&Text Encoding:";
            // 
            // _CboEncoding
            // 
            this._CboEncoding.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._CboEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CboEncoding.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._CboEncoding.ImageList = this._IlistSettings;
            this._CboEncoding.Location = new System.Drawing.Point(38, 44);
            this._CboEncoding.Name = "_CboEncoding";
            this._CboEncoding.Size = new System.Drawing.Size(197, 26);
            this._CboEncoding.TabIndex = 0;
            this._CboEncoding.SelectedIndexChanged += new System.EventHandler(this._CboEncoding_SelectedIndexChanged);
            // 
            // _LabelCustom
            // 
            this._LabelCustom.AutoSize = true;
            this._LabelCustom.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelCustom.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelCustom.Location = new System.Drawing.Point(270, 30);
            this._LabelCustom.Name = "_LabelCustom";
            this._LabelCustom.Size = new System.Drawing.Size(101, 13);
            this._LabelCustom.TabIndex = 1;
            this._LabelCustom.Tag = "cfgnpd::udiag.settings.custom";
            this._LabelCustom.Text = "Custom &Encoding:";
            // 
            // _TextCustom
            // 
            this._TextCustom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextCustom.Location = new System.Drawing.Point(273, 47);
            this._TextCustom.Name = "_TextCustom";
            this._TextCustom.Size = new System.Drawing.Size(209, 25);
            this._TextCustom.TabIndex = 1;
            this._TextCustom.TextChanged += new System.EventHandler(this._TextCustom_TextChanged);
            // 
            // FormSettings
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(534, 283);
            this.Controls.Add(this._TextCustom);
            this.Controls.Add(this._LabelCustom);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._LcboLanguage);
            this.Controls.Add(this._LabelLanguage);
            this.Controls.Add(this._ChkDebug);
            this.Controls.Add(this._LabelEncoding);
            this.Controls.Add(this._CboEncoding);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSettings";
            this.Tag = "cfgnpd::udiag.settings.caption";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.ImageList _IlistSettings;
        private Robbiblubber.Util.Localization.Controls.LocaleComboBox _LcboLanguage;
        private System.Windows.Forms.Label _LabelLanguage;
        private System.Windows.Forms.CheckBox _ChkDebug;
        private System.Windows.Forms.Label _LabelEncoding;
        private Controls.RichComboBox _CboEncoding;
        private System.Windows.Forms.Label _LabelCustom;
        private System.Windows.Forms.TextBox _TextCustom;
    }
}