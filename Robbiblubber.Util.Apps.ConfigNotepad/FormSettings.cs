﻿using System;
using System.Text;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.ConfigNotepad
{
    /// <summary>This class implements the settings window.</summary>
    internal partial class FormSettings: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormSettings()
        {
            InitializeComponent();

            if(Locale.Locales.Count == 0)
            {
                _LabelLanguage.Visible = _LcboLanguage.Visible = false;
            }

            this.Localize();

            _CboEncoding.Items.Add(new RichComboItem("cfgnpd::udiag.ascii".Localize("ASCII"), "ascii"));
            _CboEncoding.Items.Add(new RichComboItem("cfgnpd::udiag.utf8".Localize("UTF-8"), "utf8"));
            _CboEncoding.Items.Add(new RichComboItem("cfgnpd::udiag.custom".Localize("Custom"), "custom"));

            _LabelCustom.Visible = _TextCustom.Visible = false;
            if((SelectedEncoding = Program.__Main.__Encoding) == Encoding.ASCII)
            {
                _CboEncoding.SelectedIndex = 0;
            }
            else if(SelectedEncoding == Encoding.UTF8)
            {
                _CboEncoding.SelectedIndex = 1;
            }
            else
            {
                _CboEncoding.SelectedIndex = 2;
                _LabelCustom.Visible = _TextCustom.Visible = true;
                _TextCustom.Text = SelectedEncoding.WebName;
            }

            _ChkDebug.Checked = Program.__Main.__ShowDebug;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the selected encoding.</summary>
        public Encoding SelectedEncoding
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Selected encoding changed.</summary>
        private void _CboEncoding_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(_CboEncoding.SelectedIndex)
            {
                case 0:
                    SelectedEncoding = Encoding.ASCII;
                    _LabelCustom.Visible = _TextCustom.Visible = false;
                    _ButtonOK.Enabled = true;
                    break;
                case 1:
                    SelectedEncoding = Encoding.UTF8;
                    _LabelCustom.Visible = _TextCustom.Visible = false;
                    _ButtonOK.Enabled = true;
                    break;
                case 2:
                    _LabelCustom.Visible = _TextCustom.Visible = true;
                    _TextCustom_TextChanged(sender, e);
                    break;
            }
        }


        /// <summary>Custom encoding text changed.</summary>
        private void _TextCustom_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Encoding enc = Encoding.GetEncoding(_TextCustom.Text);

                if(enc != null)
                {
                    SelectedEncoding = enc;
                    _ButtonOK.Enabled = true;
                    return;
                }
            }
            catch(Exception) {}

            _ButtonOK.Enabled = false;
        }


        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            if(_LcboLanguage.Value != Locale.Selected)
            {
                Locale.Selected = _LcboLanguage.Value;
                Locale.SaveSelection("Robbiblubber.Util.NET");

                Program.__Main.Localize();
            }

            Program.__Main.__Encoding = SelectedEncoding;
            Program.__Main.__ShowDebug = _ChkDebug.Checked;
            Program.__Main.__SaveSettings();
        }
    }
}
