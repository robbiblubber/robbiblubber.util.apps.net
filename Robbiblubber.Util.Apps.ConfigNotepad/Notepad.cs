﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Util.Apps.ConfigNotepad
{
    /// <summary>This class publishes a method to start the editor programmatically.</summary>
    public static class Notepad
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Opens a document with ConfigNotepad.</summary>
        /// <param name="fileName">File name.</param>
        public static void EditDocument(string fileName)
        {
            new FormEditor(fileName, true).Show();
        }


        /// <summary>Shows a text with ConfigNotepad.</summary>
        /// <param name="text">Text.</param>
        public static void EditText(string text)
        {
            new FormEditor(text, false).Show();
        }
    }
}
