﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util.Coding;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.ConfigNotepad
{
    /// <summary>Editor window class.</summary>
    internal partial class FormEditor: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Encryption key.</summary>
        [PasswordPropertyText]
        private string _Key = "";

        /// <summary>File name.</summary>
        private string _FileName = null;

        /// <summary>Original text.</summary>
        private string _OriginalText = "";

        /// <summary>Print string.</summary>
        private string _PrintString;

        /// <summary>Recent file list.</summary>
        private RecentFileList _RecentFiles;

        /// <summary>Selected encoding,</summary>
        internal Encoding __Encoding = Encoding.UTF8;

        /// <summary>Show debug menu.</summary>
        private bool _ShowDebug = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormEditor()
        {
            InitializeComponent();

            Text = "cfgnpd::udiag.untitled".Localize("Untitled") + " - ConfigNotepad";
            PathOp.Initialize("robbiblubber.org/ConfigNotepad");

            _RecentFiles = new RecentFileList(PathOp.UserConfigurationPath + @"\recent.files", 6);
            _UpdateRecentFiles();

            LayoutOp.Initialize(PathOp.UserConfigurationPath + @"\application.layout");
            LayoutOp.RestoreLayout(this);

            __LoadSettings();

            Locale.UsingPrefixes("cfgnpd", "utctrl", "debugx");
            Locale.LoadSelection("Robbiblubber.Util.NET");
            this.Localize();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="textOrFile">Text or file.</param>
        /// <param name="fromFile">Indicating if the first parameter is a file name.</param>
        public FormEditor(string textOrFile, bool fromFile = false) : this()
        {
            _TimerStart.Tag = new List<object>();

            ((List<object>) _TimerStart.Tag).Add(textOrFile);
            ((List<object>) _TimerStart.Tag).Add(fromFile);

            _TimerStart.Enabled = true;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the file name.</summary>
        public string FileName
        {
            get { return _FileName; }
            set
            {
                Text = Path.GetFileName((_FileName = value)) + " - ConfigNotepad";
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal properties                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the debug menu is shown.</summary>
        internal bool __ShowDebug
        {
            get { return _ShowDebug; }
            set { _ShowDebug = _MenuDebug.Visible = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads the application settings.</summary>
        internal void __LoadSettings()
        {
            try
            {
                Ddp cfg = Ddp.Load(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\robbiblubber.org\confignotepad\application.settings");
                _TextEditor.Font = FontOp.Deserialize(cfg.GetString("/editor/font", _TextEditor.Font.Serialize()));
                __Encoding = Encoding.GetEncoding(cfg.GetString("/editor/encoding", Encoding.UTF8.WebName));
                __ShowDebug = cfg.GetBoolean("/settings/debug", false);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00200", ex); }
        }


        /// <summary>Saves the application settings.</summary>
        internal void __SaveSettings()
        {
            try
            {
                DdpFile cfg = Ddp.Load(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + @"\robbiblubber.org\confignotepad\application.settings");
                cfg.SetValue("/editor/font", _TextEditor.Font.Serialize());
                cfg.SetValue("/editor/encoding", __Encoding.WebName);
                cfg.SetValue("/settings/debug", __ShowDebug);
                cfg.Save();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00201", ex); }
        }


        /// <summary>Updates recent files list.</summary>
        private void _UpdateRecentFiles()
        {
            try
            {
                _MenuRecent.DropDownItems.Clear();

                foreach(string i in _RecentFiles)
                {
                    ToolStripMenuItem m = new ToolStripMenuItem(Path.GetFileName(i), Resources.document, new EventHandler(_MenuRecent_Click));
                    m.Tag = i;
                    m.ToolTipText = i;

                    if(_FileName != null) { m.Enabled = (Path.GetFullPath(i) != Path.GetFullPath(_FileName)); }

                    _MenuRecent.DropDownItems.Add(m);
                }

                _MenuRecent.Enabled = (_MenuRecent.DropDownItems.Count > 0);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00202", ex); }
        }


        /// <summary>Checks for unsaved changed an asks the user to save.</summary>
        /// <returns>Returns TRUE if unser wants to continue, otherwise returns FALSE.</returns>
        private bool _CheckSaveCancel()
        {
            try
            {
                if(_OriginalText != _TextEditor.Text)
                {
                    string mtext = "cfgnpd::udiag.savechanges.text".Localize("The current file contains unsaved changes. Do you want to save before continuing?");
                    string mcaption = "cfgnpd::udiag.savechanges.caption".Localize("Save Changes");
                    DialogResult d = MessageBox.Show(mtext, mcaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                    if(d == DialogResult.Cancel) { return false; }
                    if(d == DialogResult.Yes) { return _Save(); }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00203", ex); }

            return true;
        }


        /// <summary>Loads a file.</summary>
        /// <param name="filename">File name.</param>
        private void _Open(string filename)
        {
            try
            {
                _TextEditor.Text = _OriginalText = File.ReadAllText(filename, __Encoding);
                FileName = filename;

                _RecentFiles.Add(_FileName);
                _UpdateRecentFiles();
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00804", ex);
                MessageBox.Show("cfgnpd::udiag.open.fail".Localize("Failed to open.") + " " + 
                               ((ex is FileNotFoundException) ? "cfgnpd::udiag.open.fnf".Localize("File not found.") : "cfgnpd::udiag.open.read".Localize("Unable to read file.")),
                               "cfgnpd::udiag.open.caption".Localize("Open"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Saves the file.</summary>
        /// <returns>Returns TRUE if the file has been successfully saved, otherwise returns FALSE.</returns>
        private bool _Save()
        {
            if(string.IsNullOrWhiteSpace(FileName))
            {
                return _SaveAs();
            }

            try
            {
                File.WriteAllText(FileName, _TextEditor.Text, __Encoding);

                _RecentFiles.Add(FileName);
                _UpdateRecentFiles();

                _OriginalText = _TextEditor.Text;
                return true;
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00805", ex);
                MessageBox.Show("cfgnpd::udiag.save.fail".Localize("Failed to save.") + " " + ex.Message, "cfgnpd::udiag.save.caption".Localize("Save"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return false;
        }


        /// <summary>Names and saves the file.</summary>
        /// <returns>Returns TRUE if the file has been successfully saved, otherwise returns FALSE.</returns>
        private bool _SaveAs()
        {
            try
            {
                SaveFileDialog d = new SaveFileDialog();
                d.Filter = "cfgnpd::udiag.filter.all".Localize("All Files") + "|*.*";

                if(d.ShowDialog() == DialogResult.OK)
                {
                    FileName = d.FileName;
                    return _Save();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00204", ex); }

            return false;
        }


        /// <summary>Finds a text.</summary>
        /// <param name="f">String to find.</param>
        private void _Find(string f)
        {
            try
            {
                f = f.ToLower();
                int v = -1;

                try
                {
                    v = _TextEditor.Text.ToLower().IndexOf(f, _TextEditor.SelectionStart + 1);
                }
                catch(Exception) {}

                if(v < 0)
                {
                    v = _TextEditor.Text.ToLower().IndexOf(f);
                    if(v > _TextEditor.SelectionStart) { v = -1; }
                }

                if(v < 0)
                {
                    MessageBox.Show("cfgnpd::udiag.find.nomatch".Localize("No matches."), "cfgnpd::udiag.find.caption".Localize("Find"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    _TextEditor.SelectionStart = v;
                    _TextEditor.SelectionLength = f.Length;
                    _TextEditor.ScrollToCaret();
                }

                if(_FindSearch.Visible)
                {
                    _FindSearch.Focus();
                }
                else
                {
                    _TextEditor.Focus();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00205", ex); }
        }


        /// <summary>Shows the key dialog.</summary>
        /// <returns>Returns TRUE if a key has been entered, otherwise returns FALSE.</returns>
        private bool _GetKey()
        {
            try
            {
                UserControl f = null;

                if(_GotoSearch.Visible && _GotoSearch.Pinned)
                {
                    f = _GotoSearch;
                }
                else if(_FindSearch.Visible && _FindSearch.Pinned)
                {
                    f = _FindSearch;
                }
                else if(_ReplaceSearch.Visible && _ReplaceSearch.Pinned)
                {
                    f = _ReplaceSearch;
                }
                _GotoSearch.Hide();
                _FindSearch.Hide();
                _ReplaceSearch.Hide();

                bool rval = (_ControlKey.ShowDialog() == DialogResult.OK);

                if(f != null)
                {
                    f.Show();
                    f.BringToFront();
                }

                return rval;
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00206", ex); }

            return false;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Menu "New" click.</summary>
        private void _MenuNew_Click(object sender, EventArgs e)
        {
            if(_CheckSaveCancel())
            {
                _FileName = null;
                Text = "cfgnpd::udiag.untitled".Localize("Untitled") + " - ConfigNotepad";
                _TextEditor.Text = _OriginalText = _Key = "";
                FileName = null;
            }
        }


        /// <summary>Menu "Open" click.</summary>
        private void _MenuOpen_Click(object sender, EventArgs e)
        {
            if(!_CheckSaveCancel()) return;

            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "cfgnpd::udiag.filter.all".Localize("All files") + "|*.*";

            if(d.ShowDialog() == DialogResult.OK)
            {
                _Open(d.FileName);
            }
        }

        
        /// <summary>Recent file menu item click.</summary>
        private void _MenuRecent_Click(object sender, EventArgs e)
        {
            if(!_CheckSaveCancel()) return;
            _Open((string) ((ToolStripMenuItem) sender).Tag);
        }


        /// <summary>Menu "Save as" click.</summary>
        private void _MenuSaveAs_Click(object sender, EventArgs e)
        {
            _SaveAs();
        }


        /// <summary>Menu "Save" click.</summary>
        private void _MenuSave_Click(object sender, EventArgs e)
        {
            _Save();
        }


        /// <summary>Menu "Exit" click.</summary>
        private void _MenuExit_Click(object sender, EventArgs e)
        {
            try
            {
                Close();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00207", ex); }
        }


        /// <summary>Menu "Undo" click.</summary>
        private void _MenuUndo_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Undo();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00208", ex); }
        }


        /// <summary>Printing page.</summary>
        private void _Printer_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            try
            {
                int c, l;
                _PrintString = _TextEditor.Text;

                e.Graphics.MeasureString(_PrintString, _TextEditor.Font, e.MarginBounds.Size, StringFormat.GenericTypographic, out c, out l);
                e.Graphics.DrawString(_PrintString, _TextEditor.Font, Brushes.Black, e.MarginBounds, StringFormat.GenericTypographic);

                _PrintString = _PrintString.Substring(c);

                e.HasMorePages = (_PrintString.Length > 0);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00209", ex); }
        }


        /// <summary>Menu "Print" click.</summary>
        private void _MenuPrint_Click(object sender, EventArgs e)
        {
            try
            {
                PrintDialog d = new PrintDialog();
                d.PrinterSettings = _Printer.PrinterSettings;
                d.UseEXDialog = true;

                if(d.ShowDialog() == DialogResult.OK)
                {
                    _Printer.PrinterSettings = d.PrinterSettings;
                    _Printer.Print();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00210", ex); }
        }


        /// <summary>Menu "Page setup" click.</summary>
        private void _MenuPageSetup_Click(object sender, EventArgs e)
        {
            try
            {
                PageSetupDialog d = new PageSetupDialog();
                d.PrinterSettings = _Printer.PrinterSettings;
                d.PageSettings = _Printer.DefaultPageSettings;

                if(d.ShowDialog() == DialogResult.OK)
                {
                    _Printer.PrinterSettings = d.PrinterSettings;
                    _Printer.DefaultPageSettings = d.PageSettings;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00211", ex); }
        }


        /// <summary>Menu "Cut" click.</summary>
        private void _MenuCut_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Cut();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00212", ex); }
        }


        /// <summary>Menu "Copy" click.</summary>
        private void _MenuCopy_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Copy();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00213", ex); }
        }


        /// <summary>Menu "Paste" click.</summary>
        private void _MenuPaste_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Paste();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00214", ex); }
        }


        /// <summary>Menu "Delete" click.</summary>
        private void _MenuDelete_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.SelectedText = "";
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00215", ex); }
        }


        /// <summary>Menu "Select all" click.</summary>
        private void _MenuSelectAll_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.SelectAll();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00216", ex); }
        }


        /// <summary>Menu "Go to" click.</summary>
        private void _MenuGoTo_Click(object sender, EventArgs e)
        {
            try
            {
                _FindSearch.Hide();
                _ReplaceSearch.Hide();

                _GotoSearch.Line = (_TextEditor.GetLineFromCharIndex(_TextEditor.SelectionStart) + 1);
                _GotoSearch.Show();
                _GotoSearch.BringToFront();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00217", ex); }
        }


        /// <summary>Menu "Word wrap" click.</summary>
        private void _MenuWordWrap_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.WordWrap = _MenuWordWrap.Checked = (!_MenuWordWrap.Checked);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00218", ex); }
        }


        /// <summary>Menu "Font" click.</summary>
        private void _MenuFont_Click(object sender, EventArgs e)
        {
            try
            {
                FontDialog d = new FontDialog();
                d.Font = _TextEditor.Font;

                if(d.ShowDialog() == DialogResult.OK)
                {
                    _TextEditor.Font = d.Font;
                    __SaveSettings();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00219", ex); }
        }


        /// <summary>Form closing.</summary>
        private void FormEditor_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                e.Cancel = (!_CheckSaveCancel());
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00220", ex); }
        }


        /// <summary>Menu "About" click.</summary>
        private void _MenuAbout_Click(object sender, EventArgs e)
        {
            try
            {
                FormAbout f = new FormAbout();

                f.ShowDialog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00221", ex); }
        }


        /// <summary>Menu "Help" click.</summary>
        private void _MenuShowHelp_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("http://robbiblubber.lina-city.de/wiki.php?title=Help-CfgNpd");
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00222", ex); }
        }


        /// <summary>Menu "Find" click.</summary>
        private void _MenuFind_Click(object sender, EventArgs e)
        {
            try
            {
                _GotoSearch.Hide();
                _ControlKey.Hide();
                _ReplaceSearch.Hide();
                _FindSearch.Show();
                _FindSearch.BringToFront();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00223", ex); }
        }


        /// <summary>Menu "Find next" click.</summary>
        private void _MenuFindNext_Click(object sender, EventArgs e)
        {
            try
            {
                if(string.IsNullOrEmpty(_FindSearch.SearchText))
                {
                    _MenuFind_Click(sender, e);
                }
                else
                {
                    _Find(FindControl.CurrentSearchText);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00224", ex); }
        }


        /// <summary>Menu "Replace" click.</summary>
        private void _MenuReplace_Click(object sender, EventArgs e)
        {
            try
            {
                _GotoSearch.Hide();
                _ControlKey.Hide();
                _FindSearch.Hide();
                _ReplaceSearch_SearchTextChanged(null, null);
                _ReplaceSearch.Show();
                _ReplaceSearch.BringToFront();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00225", ex); }
        }

        
        /// <summary>Replace search text changed.</summary>

        private void _ReplaceSearch_SearchTextChanged(object sender, EventArgs e)
        {
            try
            {
                if(string.IsNullOrEmpty(_ReplaceSearch.SearchText)) { _ReplaceSearch.AllowReplaceAll = _ReplaceSearch.AllowReplace = false; return; }
                _ReplaceSearch.AllowReplaceAll = true;

                _ReplaceSearch.AllowReplace = (_TextEditor.SelectedText.ToLower() == _ReplaceSearch.SearchText.ToLower());
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00226", ex); }
        }


        /// <summary>Replace all requested.</summary>
        private void _ReplaceSearch_ReplaceAll(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = _TextEditor.Text.ReplaceAll(_ReplaceSearch.SearchText, _ReplaceSearch.ReplaceText, false);
                _ReplaceSearch_SearchTextChanged(null, null);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00227", ex); }
        }


        /// <summary>Replace requested.</summary>
        private void _ReplaceSearch_Replace(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.SelectedText = _ReplaceSearch.ReplaceText;
                _FindSearch_Search(null, null);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00228", ex); }
        }


        /// <summary>Search requested.</summary>
        private void _FindSearch_Search(object sender, EventArgs e)
        {
            try
            {
                _Find(FindControl.CurrentSearchText);
                _ReplaceSearch_SearchTextChanged(null, null);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00229", ex); }
        }


        /// <summary>Goto line changed.</summary>
        private void _GotoSearch_LineChanged(object sender, GoToFeedbackEventArgs e)
        {
            try
            {
                e.Allowed = ((e.Line > 0) && (e.Line <= _TextEditor.Lines.Length));
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00230", ex); }
        }


        /// <summary>Goto event.</summary>
        private void _GotoSearch_GoTo(object sender, EventArgs e)
        {
            try
            {
                int tar = _TextEditor.GetFirstCharIndexFromLine(_GotoSearch.Line - 1);

                if(tar > -1)
                {
                    _TextEditor.Select(tar, 0);
                }
                else
                {
                    _TextEditor.Select(_TextEditor.Text.Length, 0);
                }
                _TextEditor.Focus();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00231", ex); }
        }


        /// <summary>Timer tick.</summary>
        private void _TimerStart_Tick(object sender, EventArgs e)
        {
            try
            {
                _TimerStart.Enabled = false;

                bool fromFile = true;
                string text = "";

                foreach(object i in ((List<object>) _TimerStart.Tag))
                {
                    if(i is string)
                    {
                        text = ((string) i);
                    }
                    else if(i is bool) { fromFile = ((bool) i); }
                }

                if(fromFile)
                {
                    _Open(text);
                }
                else
                {
                    _TextEditor.Text = text;
                }

                _TimerStart.Tag = null;
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00232", ex); }
        }


        /// <summary>Menu "Decompress (Base64)" click.</summary>
        private void _MenuGzDecompressB64_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = GZip.Decompress(_TextEditor.Text, Base64.Instance);
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00830", ex);
                MessageBox.Show("cfgnpd::udiag.decompress.fail".Localize("Failed to decompress text. Probably the text is not compressed or encoded in the selected way."), "cfgnpd::udiag.decompress.caption".Localize("Decompress"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Compress (Base64)" click.</summary>
        private void _MenuGzCompressB64_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = GZip.Compress(_TextEditor.Text, Base64.Instance);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00831", ex); }
        }


        /// <summary>Menu "Encrypt" click.</summary>
        private void _MenuAESEncryptB64_Click(object sender, EventArgs e)
        {}


        /// <summary>Menu "Decrypt" click.</summary>
        private void _MenuAESDecryptB64_Click(object sender, EventArgs e)
        {}


        /// <summary>Menu "Hex encode" click.</summary>
        private void _MenuHexEncode_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = _TextEditor.Text.ToHex();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00834", ex); }
        }


        /// <summary>Menu "Hex decode" click.</summary>
        private void _MenuHexDecode_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = Hex.ToString(_TextEditor.Text);
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00835", ex);
                MessageBox.Show("cfgnpd::decode.fail".Localize("Failed to decode text. Make sure the source text is a correctly encoded expression."), "cfgnpd::decode.caption".Localize("Decode"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Base64 encode" click.</summary>
        private void _MenuB64Encode_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = _TextEditor.Text.ToBase64();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00836", ex); }
        }


        /// <summary>Menu "Base64 decode" click.</summary>
        private void _MenuB64Decode_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = Base64.ToString(_TextEditor.Text);
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00837", ex);
                MessageBox.Show("cfgnpd::decode.fail".Localize("Failed to decode text. Make sure the source text is a correctly encoded expression."), "cfgnpd::decode.caption".Localize("Decode"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Compress (raw)" click.</summary>
        private void _MenuGzCompressRaw_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = GZip.Compress(_TextEditor.Text, PlainText.Instance);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00840", ex); }
        }


        /// <summary>Menu "Compress (hex)" click.</summary>
        private void _MenuGzCompressHex_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = GZip.Compress(_TextEditor.Text, Hex.Instance);
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00841", ex); }
        }


        /// <summary>Menu "Decompress (raw)" click.</summary>
        private void _MenuGzDecompressRaw_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = GZip.Decompress(_TextEditor.Text, PlainText.Instance);
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00843", ex);
                MessageBox.Show("cfgnpd::decompress.fail".Localize("Failed to decompress text. Probably the text is not compressed or encoded in the selected way."), "cfgnpd::decompress.caption".Localize("Decompress"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Decompress (hex)" click.</summary>
        private void _MenuGzDecompressHex_Click(object sender, EventArgs e)
        {
            try
            {
                _TextEditor.Text = GZip.Decompress(_TextEditor.Text, Hex.Instance);
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00844", ex);
                MessageBox.Show("cfgnpd::decompress.fail".Localize("Failed to decompress text. Probably the text is not compressed or encoded in the selected way."), "cfgnpd::decompress.caption".Localize("Decompress"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Encrypt FunnelCake (raw)" click.</summary>
        private void _MenuFunnelEncryptRaw_Click(object sender, EventArgs e)
        {
            try
            {
                if(_GetKey())
                {
                    _TextEditor.Text = FunnelCake.Encrypt(_TextEditor.Text, (_Key = _ControlKey.Text), PlainText.Instance);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00250", ex); }
        }


        /// <summary>Menu "Encrypt FunnelCake (hex)" click.</summary>
        private void _MenuFunnelEncryptHex_Click(object sender, EventArgs e)
        {
            try
            {
                if(_GetKey())
                {
                    _TextEditor.Text = FunnelCake.Encrypt(_TextEditor.Text, (_Key = _ControlKey.Text), Hex.Instance);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00251", ex); }
        }


        /// <summary>Menu "Encrypt FunnelCake (Base64)" click.</summary>
        private void _MenuFunnelEncryptB64_Click(object sender, EventArgs e)
        {
            try
            {
                if(_GetKey())
                {
                    _TextEditor.Text = FunnelCake.Encrypt(_TextEditor.Text, (_Key = _ControlKey.Text), Base64.Instance);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00242", ex); }
        }


        /// <summary>Menu "Decrypt FunnelCake (raw)" click.</summary>
        private void _MenuFunnelDecryptRaw_Click(object sender, EventArgs e)
        {
            try
            {
                if(_GetKey())
                {
                    _TextEditor.Text = FunnelCake.Decrypt(_TextEditor.Text, (_Key = _ControlKey.Text), PlainText.Instance);
                }
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00854", ex);
                MessageBox.Show("cfgnpd::decrypt.fail".Localize("Failed to decrypt text. Possibly the text is not encrypted or you haven't provided the correct key."), "cfgnpd::decrypt.caption".Localize("Decrypt"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Decrypt FunnelCake (hex)" click.</summary>
        private void _MenuFunnelDecryptHex_Click(object sender, EventArgs e)
        {
            try
            {
                if(_GetKey())
                {
                    _TextEditor.Text = FunnelCake.Decrypt(_TextEditor.Text, (_Key = _ControlKey.Text), Hex.Instance);
                }
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00855", ex);
                MessageBox.Show("cfgnpd::decrypt.fail".Localize("Failed to decrypt text. Possibly the text is not encrypted or you haven't provided the correct key."), "cfgnpd::decrypt.caption".Localize("Decrypt"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Decrypt FunnelCake (Base64)" click.</summary>
        private void _MenuFunnelDecryptB64_Click(object sender, EventArgs e)
        {
            try
            {
                if(_GetKey())
                {
                    _TextEditor.Text = FunnelCake.Decrypt(_TextEditor.Text, (_Key = _ControlKey.Text), Base64.Instance);
                }
            }
            catch(Exception ex)
            {
                DebugOp.Dump("CFGNPD00856", ex);
                MessageBox.Show("cfgnpd::decrypt.fail".Localize("Failed to decrypt text. Possibly the text is not encrypted or you haven't provided the correct key."), "cfgnpd::decrypt.caption".Localize("Decrypt"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>Menu "Settings" click.</summary>
        private void _MenuSettings_Click(object sender, EventArgs e)
        {
            try
            {
                FormSettings f = new FormSettings();
                f.ShowDialog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00258", ex); }
        }


        /// <summary>Menu "Start/Stop Logging" click.</summary>
        private void _MenuDebugStart_Click(object sender, EventArgs e)
        {
            DebugOp.Enabled = (!DebugOp.Enabled);
        }


        /// <summary>Menu "Save Dump" click.</summary>
        private void _MenuDebugSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(!DebugOp.Enabled) return;

                SaveFileDialog d = new SaveFileDialog();
                d.Filter = "debugx::udiag.filter.dump".Localize("Debug Dump Files") + " (*.debug.dump)|*.debug.dump|" + "debugx::udiag.filter.all".Localize("All Files") + "|*.*";

                if(d.ShowDialog() == DialogResult.OK)
                {
                    DebugOp.CreateDumpFile(d.FileName);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00260", ex); }
        }


        /// <summary>Menu "View Dump" click.</summary>
        private void _MenuDebugView_Click(object sender, EventArgs e)
        {
            try
            {
                if(!DebugOp.Enabled) return;

                DebugOp.ShowLog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00261", ex); }
        }


        /// <summary>Menu "Upload Dump" click.</summary>
        private void _MenuDebugUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if(!DebugOp.Enabled) return;

                DebugOp.UploadData("http://robbiblubber.lima-city.de/debug/Robbiblubber.Util.NET/");
            }
            catch(Exception ex) { DebugOp.DumpMessage("CFGNPD00262", ex); }
        }


        /// <summary>Menu "Debug" opening.</summary>
        private void _MenuDebug_DropDownOpening(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = (!(_MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = DebugOp.Enabled));
        }


        /// <summary>Menu "Debug" closed.</summary>
        private void _MenuDebug_DropDownClosed(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = _MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = true;
        }
    }
}