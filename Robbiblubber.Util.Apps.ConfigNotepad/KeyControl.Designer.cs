﻿namespace Robbiblubber.Util.Apps.ConfigNotepad
{
    partial class KeyControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KeyControl));
            this._LabelKey = new System.Windows.Forms.Label();
            this._TextKey = new System.Windows.Forms.TextBox();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._CheckPin = new System.Windows.Forms.CheckBox();
            this._ButtonClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _LabelKey
            // 
            this._LabelKey.AutoSize = true;
            this._LabelKey.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelKey.Location = new System.Drawing.Point(13, 29);
            this._LabelKey.Name = "_LabelKey";
            this._LabelKey.Size = new System.Drawing.Size(85, 13);
            this._LabelKey.TabIndex = 0;
            this._LabelKey.Tag = "cfgnpd::udiag.enctyptionkey";
            this._LabelKey.Text = "Encryption &Key:";
            // 
            // _TextKey
            // 
            this._TextKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextKey.Location = new System.Drawing.Point(16, 45);
            this._TextKey.Name = "_TextKey";
            this._TextKey.PasswordChar = '*';
            this._TextKey.Size = new System.Drawing.Size(423, 25);
            this._TextKey.TabIndex = 0;
            this._TextKey.UseSystemPasswordChar = true;
            this._TextKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            // 
            // _ButtonOK
            // 
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(314, 85);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(125, 28);
            this._ButtonOK.TabIndex = 1;
            this._ButtonOK.Tag = "utctrl::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            this._ButtonOK.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            // 
            // _CheckPin
            // 
            this._CheckPin.Appearance = System.Windows.Forms.Appearance.Button;
            this._CheckPin.Enabled = false;
            this._CheckPin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._CheckPin.Image = ((System.Drawing.Image)(resources.GetObject("_CheckPin.Image")));
            this._CheckPin.Location = new System.Drawing.Point(390, 7);
            this._CheckPin.Name = "_CheckPin";
            this._CheckPin.Size = new System.Drawing.Size(23, 23);
            this._CheckPin.TabIndex = 0;
            this._CheckPin.TabStop = false;
            this._CheckPin.Tag = "||utctrl::common.button.pin";
            this._CheckPin.UseVisualStyleBackColor = true;
            this._CheckPin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            // 
            // _ButtonClose
            // 
            this._ButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonClose.Image = ((System.Drawing.Image)(resources.GetObject("_ButtonClose.Image")));
            this._ButtonClose.Location = new System.Drawing.Point(416, 7);
            this._ButtonClose.Margin = new System.Windows.Forms.Padding(1);
            this._ButtonClose.Name = "_ButtonClose";
            this._ButtonClose.Size = new System.Drawing.Size(23, 23);
            this._ButtonClose.TabIndex = 0;
            this._ButtonClose.TabStop = false;
            this._ButtonClose.Tag = "||utctrl::common.button.close";
            this._ButtonClose.UseVisualStyleBackColor = true;
            this._ButtonClose.Click += new System.EventHandler(this._ButtonClose_Click);
            this._ButtonClose.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            // 
            // KeyControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this._CheckPin);
            this.Controls.Add(this._ButtonClose);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._TextKey);
            this.Controls.Add(this._LabelKey);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "KeyControl";
            this.Size = new System.Drawing.Size(452, 125);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelKey;
        private System.Windows.Forms.TextBox _TextKey;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.Button _ButtonClose;
        private System.Windows.Forms.CheckBox _CheckPin;
    }
}
