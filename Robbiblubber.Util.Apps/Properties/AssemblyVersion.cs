﻿using System.Reflection;



[assembly: AssemblyCompany("robbiblubber.org")]
[assembly: AssemblyProduct("robbiblubber.org Utility Suite")]
[assembly: AssemblyCopyright("© 2021 robbiblubber.org")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("3.0.0")]
[assembly: AssemblyFileVersion("3.0.0")]
