﻿namespace Robbiblubber.Util.Apps.LocaleEditor
{
    partial class FormAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAbout));
            this._ButtonClose = new System.Windows.Forms.Button();
            this._LabelVersion = new System.Windows.Forms.Label();
            this._LabelName = new System.Windows.Forms.Label();
            this._LabelRobbiblubber = new System.Windows.Forms.Label();
            this._LabelIcon = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _ButtonClose
            // 
            this._ButtonClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonClose.Location = new System.Drawing.Point(800, 100);
            this._ButtonClose.Name = "_ButtonClose";
            this._ButtonClose.Size = new System.Drawing.Size(64, 33);
            this._ButtonClose.TabIndex = 9;
            this._ButtonClose.Text = "Close";
            this._ButtonClose.UseVisualStyleBackColor = true;
            // 
            // _LabelVersion
            // 
            this._LabelVersion.AutoSize = true;
            this._LabelVersion.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelVersion.Location = new System.Drawing.Point(522, 248);
            this._LabelVersion.Name = "_LabelVersion";
            this._LabelVersion.Size = new System.Drawing.Size(104, 21);
            this._LabelVersion.TabIndex = 8;
            this._LabelVersion.Text = "Version 1.0.0";
            // 
            // _LabelName
            // 
            this._LabelName.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelName.Image = ((System.Drawing.Image)(resources.GetObject("_LabelName.Image")));
            this._LabelName.Location = new System.Drawing.Point(280, 171);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(390, 127);
            this._LabelName.TabIndex = 7;
            // 
            // _LabelRobbiblubber
            // 
            this._LabelRobbiblubber.Image = ((System.Drawing.Image)(resources.GetObject("_LabelRobbiblubber.Image")));
            this._LabelRobbiblubber.Location = new System.Drawing.Point(446, 20);
            this._LabelRobbiblubber.Name = "_LabelRobbiblubber";
            this._LabelRobbiblubber.Size = new System.Drawing.Size(213, 30);
            this._LabelRobbiblubber.TabIndex = 6;
            // 
            // _LabelIcon
            // 
            this._LabelIcon.Image = ((System.Drawing.Image)(resources.GetObject("_LabelIcon.Image")));
            this._LabelIcon.Location = new System.Drawing.Point(27, 33);
            this._LabelIcon.Name = "_LabelIcon";
            this._LabelIcon.Size = new System.Drawing.Size(249, 257);
            this._LabelIcon.TabIndex = 5;
            // 
            // FormAbout
            // 
            this.AcceptButton = this._ButtonClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(246)))), ((int)(((byte)(250)))));
            this.CancelButton = this._ButtonClose;
            this.ClientSize = new System.Drawing.Size(684, 361);
            this.Controls.Add(this._ButtonClose);
            this.Controls.Add(this._LabelVersion);
            this.Controls.Add(this._LabelName);
            this.Controls.Add(this._LabelRobbiblubber);
            this.Controls.Add(this._LabelIcon);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAbout";
            this.Text = "About LEd";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button _ButtonClose;
        private System.Windows.Forms.Label _LabelVersion;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.Label _LabelRobbiblubber;
        private System.Windows.Forms.Label _LabelIcon;
    }
}