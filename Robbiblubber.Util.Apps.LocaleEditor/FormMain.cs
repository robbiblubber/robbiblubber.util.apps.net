﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;
using Robbiblubber.Util.Debug;
using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.LocaleEditor
{
    /// <summary>Main form class.</summary>
    public partial class FormMain: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Current view.</summary>
        private ViewState _View;

        /// <summary>Current definition.</summary>
        private IDefinition _Def = null;

        /// <summary>Binding source.</summary>
        private BindingSource _Source = null;

        /// <summary>Updating flag.</summary>
        private bool _Updating = false;

        /// <summary>Recent file list.</summary>
        private RecentFileList _RecentFiles;

        /// <summary>Show debug flag.</summary>
        private bool _ShowDebug = false;

        /// <summary>Grid editing control.</summary>
        private TextBoxBase _InternalCurrentlyEditingCell = null;

        /// <summary>Initial source.</summary>
        private string _InitialSource;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormMain(string file)
        {
            InitializeComponent();

            PathOp.Initialize("robbiblubber.org/LEd");

            _LoadSettings();

            _RecentFiles = new RecentFileList(PathOp.UserConfigurationPath + @"\recent.files", 6);
            _UpdateRecentFiles();

            LayoutOp.Initialize(PathOp.UserConfigurationPath + @"\application.layout");
            LayoutOp.RestoreLayout(this);

            View = ViewState.EMPTY;
            FormMain_Resize(null, null);

            Locale.UsingPrefixes("ledx", "utctrl", "debugx");
            Locale.LoadSelection("Robbiblubber.Util.NET");
            this.Localize();

            if((file != null) && File.Exists(file))
            {
                _Def = TLocale.Load(file);
                _Update();

                _RecentFiles.Add(file);
                _UpdateRecentFiles();
            }
            _SetCaption();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the current view.</summary>
        public ViewState View
        {
            get { return _View; }
            set
            {
                try
                {
                    _View = value;

                    _PanelEmpty.Visible = (_View == ViewState.EMPTY);

                    if(_Def == null)
                    {
                        _PanelProperties.Visible = _PanelLocaleProperties.Visible = false;
                    }
                    else
                    {
                        _PanelProperties.Visible = (_Def.IsTable && (_View == ViewState.PROPERTIES));
                        _PanelLocaleProperties.Visible = ((!_Def.IsTable) && (_View == ViewState.PROPERTIES));
                    }
                    _TextSource.Visible = (_View == ViewState.SOURCE);
                    _GridData.Visible = (_View == ViewState.DATA);

                    if(_View == ViewState.SOURCE)
                    {
                        _TextSource.Focus();
                    }
                }
                catch(Exception ex) { DebugOp.DumpMessage("LEDX00100", ex); }
            }
        }


        /// <summary>Gets if the debug menu is shown.</summary>
        public bool ShowDebug
        {
            get { return _ShowDebug; }
            set { _ShowDebug = _MenuDebug.Visible = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the current edit control.</summary>
        private TextBoxBase _CurrentlyEditingCell
        {
            get
            {
                if(_InternalCurrentlyEditingCell != null)
                {
                    try
                    {
                        if(_InternalCurrentlyEditingCell.IsDisposed)
                        {
                            _InternalCurrentlyEditingCell = null;
                        }
                        else { _InternalCurrentlyEditingCell.GetType(); }
                    }
                    catch(Exception) { _InternalCurrentlyEditingCell = null; }
                }

                return _InternalCurrentlyEditingCell;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the window caption.</summary>
        /// <param name="modified">Modified flag.</param>
        private void _SetCaption(bool modified)
        {
            if(_Def == null)
            {
                Text = "LEd";
            }
            else if(_Def.FileName == null)
            {
                Text = "ledx::file.unnamed".Localize("Unnamed") + "* - LEd";
            }
            else
            {
                Text = Path.GetFileName(_Def.FileName) + (modified ? "* - LEd" : " - LEd");
            }
        }


        /// <summary>Sets the window caption.</summary>
        private void _SetCaption()
        {
            _SetCaption((_Def == null) ? false : _Def.Modified);
        }


        /// <summary>Checks if object is saved and asks for save or cancel if necessary.</summary>
        /// <returns>Returns TRUE if the user decides to continue, FALSE if action should be cancelled.</returns>
        private bool _CheckSaveCancel()
        {
            if(_Def == null) return true;

            try
            {
                if(View == ViewState.SOURCE)
                {
                    _Def.Source = _TextSource.Text;
                }

                if(_Def.Modified)
                {
                    string mtext = Locale.Lookup("ledx::udiag.savechanges.text", "The current file contains unsaved changes. Do you want to save these changes before continuing?");
                    string mcaption = Locale.Lookup("ledx:udiag.savechanges.caption", "Save changes");

                    DialogResult d = MessageBox.Show(mtext, mcaption, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

                    if(d == DialogResult.Cancel)
                    {
                        return false;
                    }
                    if(d == DialogResult.Yes)
                    {
                        return _Save();
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00101", ex); }

            return true;
        }


        /// <summary>Saves the current locale definition.</summary>
        /// <returns>Returns TRUE when the locale definition has been saved, otherwise returns FALSE.</returns>
        private bool _Save()
        {
            try
            {
                if(string.IsNullOrEmpty(_Def.FileName))
                {
                    return _SaveAs();
                }

                if(View == ViewState.SOURCE)
                {
                    _Def.Source = _TextSource.Text;
                }

                try
                {
                    _Def.Save();

                    _RecentFiles.Add(_Def.FileName);
                    _UpdateRecentFiles();

                    Text = Path.GetFileName(_Def.FileName) + " - LEd";
                }
                catch(Exception ex)
                {
                    DebugOp.Dump("LEDX00102", ex);

                    string mtext = Locale.Lookup("ledx::udiag.saveerr.text", "The file could not be saved.");
                    string mcaption = Locale.Lookup("ledx:udiag.saveerr.caption", "Error");

                    MessageBox.Show(mtext + "\r\n" + ex.Message, mcaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00103", ex); }

            _SetCaption();

            return true;
        }

        double f(int x, int t)
        {
            return Math.Sin(t);
        }

        private bool _SaveAs()
        {
            SaveFileDialog d = new SaveFileDialog();

            try
            {
                if(_Def.IsTable)
                {
                    string m0 = Locale.Lookup("ledx::udiag.filter.btab", "Encoded Locale Tables");
                    string m1 = Locale.Lookup("ledx::udiag.filter.ltab", "Plain-Text Locale Tables");
                    string m2 = Locale.Lookup("ledx::udiag.filter.ctab", "Compressed Locale Tables");
                    string m3 = Locale.Lookup("ledx::udiag.filter.phil", "PHP-Implemented Tables");
                    string m4 = Locale.Lookup("ledx::udiag.filter.all", "All Files");

                    d.Filter = m0 + " (*.lsbtab)|*.lsbtab|" + m1 + " (*.lstab)|*.lstab|" + m2 + " (*.lsctab)|*.lsctab|" + m3 + " (*.phil7)|*.phil7|" + m4 + "|*.*";
                }
                else
                {
                    string m0 = Locale.Lookup("ledx::udiag.filter.ldef", "Locale Definitions");
                    string m1 = Locale.Lookup("ledx::udiag.filter.ldefinfo", "Plain-Text Locale Definitions");
                    string m2 = Locale.Lookup("ledx::udiag.filter.ldefphil", "PHP-Implemented Local Definitions");
                    string m3 = Locale.Lookup("ledx::udiag.filter.all", "All Files");

                    d.Filter = m0 + " (*.locale)|*.locale|" + m1 + " (*.locale.information)|*.locale.information|" + m2 + " (*.locale.phil)|*.locale.phil|" + m3 + "|*.*";
                }

                if(d.ShowDialog() == DialogResult.OK)
                {
                    _Def.FileName = d.FileName;

                    if(View == ViewState.SOURCE)
                    {
                        _Def.Source = _TextSource.Text;
                    }

                    try
                    {
                        _Def.Save();

                        _RecentFiles.Add(_Def.FileName);
                        _UpdateRecentFiles();
                        _SetCaption();

                        Text = Path.GetFileName(_Def.FileName) + " - LEd";

                        return true;
                    }
                    catch(Exception ex)
                    {
                        DebugOp.Dump("LEDX00104", ex);

                        string mtext = Locale.Lookup("ledx::udiag.saveerr.text", "The file could not be saved.");
                        string mcaption = Locale.Lookup("ledx:udiag.saveerr.caption", "Error");

                        MessageBox.Show(mtext + "\r\n" + ex.Message, mcaption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00105", ex); }
            
            return false;
        }


        /// <summary>Attaches a locale to the window.</summary>
        private void _Update()
        {
            _Updating = true;

            try
            {
                if(_Def.IsTable)
                {
                    int w0 = 240;
                    int w1 = _GridData.Width - w0 - 43;

                    if(_GridData.Columns.Count > 0)
                    {
                        w0 = _GridData.Columns[0].Width;
                        w1 = _GridData.Columns[1].Width;
                    }

                    _Source = new BindingSource();
                    _Source.DataSource = _Def.Data;

                    _GridData.DataSource = _Source;

                    _GridData.Columns[0].HeaderText = "Key";
                    _GridData.Columns[0].HeaderCell.Style.Font = new Font(Font, FontStyle.Bold);
                    _GridData.Columns[0].Width = w0;
                    _GridData.Columns[1].HeaderText = "Expression";
                    _GridData.Columns[1].HeaderCell.Style.Font = new Font(Font, FontStyle.Bold);
                    _GridData.Columns[1].Width = w1;

                    _TextName.Text = _Def.Properties.Name;
                    _TextVersion.Text = _Def.Properties.Version;
                    _TextPrefixes.Text = ((TTable.TProperties) _Def.Properties).Prefixes;
                    _TextApplication.Text = _Def.Properties.Application;
                    _TextAuthor.Text = _Def.Properties.Author;
                    _TextKey.Text = _Def.Properties.Key;
                    _TextMeta.Text = _Def.Properties.Meta;

                    if(string.IsNullOrEmpty(_Def.Properties.Key) && (_Def.Data.Count == 0))
                    {
                        View = ViewState.PROPERTIES;
                    }
                    else if(View == ViewState.EMPTY)
                    {
                        View = ViewState.DATA;
                    }
                    else if(View == ViewState.SOURCE)
                    {
                        _TextSource.Text = _Def.Source;
                    }
                    else
                    {
                        View = View;
                    }
                }
                else
                {
                    _TextLocaleName.Text = _Def.Properties.Name;
                    _TextLocaleVersion.Text = _Def.Properties.Version;
                    _TextLocaleBase.Text = ((TLocale.TProperties) _Def.Properties).Base;
                    _TextLocaleIcon.Text = ((TLocale.TProperties) _Def.Properties).IconFile;
                    _TextLocaleSmallIcon.Text = ((TLocale.TProperties) _Def.Properties).SmallIconFile;
                    _CheckDefault.Checked = ((TLocale.TProperties) _Def.Properties).IsDefault;
                    _TextLocaleApplication.Text = _Def.Properties.Application;
                    _TextLocaleAuthor.Text = _Def.Properties.Author;
                    _TextLocaleKey.Text = _Def.Properties.Key;
                    _TextLocaleMeta.Text = _Def.Properties.Meta;

                    if(string.IsNullOrEmpty(_Def.Properties.Key))
                    {
                        View = ViewState.PROPERTIES;
                    }
                    else if(View == ViewState.SOURCE)
                    {
                        _TextSource.Text = _Def.Source;
                    }
                    else
                    {
                        View = ViewState.PROPERTIES;
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00106", ex); }

            _Updating = false;
        }


        /// <summary>Finds text in source.</summary>
        /// <param name="f">String to find.</param>
        private void _FindSource(string f)
        {
            try
            {
                f = f.ToLower();
                int v = -1;

                try
                {
                    v = _TextSource.Text.ToLower().IndexOf(f, _TextSource.SelectionStart + 1);
                }
                catch(Exception) {}

                if(v < 0)
                {
                    v = _TextSource.Text.ToLower().IndexOf(f);
                    if(v > _TextSource.SelectionStart) { v = -1; }
                }

                if(v < 0)
                {
                    string mtext = Locale.Lookup("ledx::udiag.find.nomatches.text", "No matches");
                    string mcap = Locale.Lookup("ledx::udiag.find.nomatches.caption", "Find");

                    MessageBox.Show(mtext, mcap, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    _TextSource.SelectionStart = v;
                    _TextSource.SelectionLength = f.Length;
                    _TextSource.ScrollToCaret();
                }

                if(_FindSearch.Visible)
                {
                    _FindSearch.Focus();
                }
                else
                {
                    _TextSource.Focus();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00108", ex); }
        }


        /// <summary>Updates recent files list.</summary>
        private void _UpdateRecentFiles()
        {
            try
            {
                _MenuRecent.DropDownItems.Clear();

                foreach(string i in _RecentFiles)
                {
                    ToolStripMenuItem m = new ToolStripMenuItem(Path.GetFileName(i), ((i.ToLower().EndsWith(".locale") || i.ToLower().EndsWith(".locale.information") || i.ToLower().EndsWith(".locale.phil")) ? Resources.localedef : Resources.localetable), new EventHandler(_MenuRecent_Click));
                    m.Tag = i;
                    m.ToolTipText = i;

                    if((_Def != null) && (_Def.FileName != null)) { m.Enabled = (Path.GetFullPath(i) != Path.GetFullPath(_Def.FileName)); }

                    _MenuRecent.DropDownItems.Add(m);
                }

                _MenuRecent.Enabled = (_MenuRecent.DropDownItems.Count > 0);
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00109", ex); }
        }


        /// <summary>Saves settings.</summary>
        private void _SaveSettings()
        {
            try
            {
                DdpFile cfg = Ddp.Load(PathOp.UserSettingsFile);
                cfg.SetBoolean("/settings/debug", ShowDebug);
                cfg.Save();
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00110", ex); }
        }


        /// <summary>Loads settings.</summary>
        private void _LoadSettings()
        {
            try
            {
                Ddp cfg = Ddp.Load(PathOp.UserSettingsFile);
                ShowDebug = cfg.GetBoolean("/settings/debug");
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00111", ex); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Recent file menu item clicked.</summary>
        private void _MenuRecent_Click(object sender, EventArgs e)
        {
            try
            {
                if(_CheckSaveCancel())
                {
                    _Def = TLocale.Load((string) ((ToolStripMenuItem) sender).Tag);
                    _Update();

                    _RecentFiles.Add((string) ((ToolStripMenuItem) sender).Tag);
                    _UpdateRecentFiles();
                    _SetCaption();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00112", ex); }
        }


        /// <summary>Menu "About" click.</summary>
        private void _MenuAbout_Click(object sender, System.EventArgs e)
        {
            try
            {
                FormAbout f = new FormAbout();
                f.ShowDialog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00113", ex); }
        }


        /// <summary>Menu "Tools" opening.</summary>
        private void _MenuTools_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                _MenuImport.Enabled = ((_Def != null) && _Def.IsTable);
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00114", ex); }
        }


        /// <summary>Menu "Tools" closed.</summary>
        private void _MenuTools_DropDownClosed(object sender, EventArgs e)
        {
            _MenuImport.Enabled = true;
        }


        /// <summary>Menu "Edit" opening.</summary>
        private void _MenuEdit_DropDownOpening(object sender, EventArgs e)
        {
            try
            {
                if(View == ViewState.EMPTY)
                {
                    _MenuUndo.Enabled = false;
                    _MenuSelectAll.Enabled = _MenuDeselectAll.Enabled = _MenuCut.Enabled = _MenuCopy.Enabled = _MenuPaste.Enabled = _MenuDelete.Enabled = false;
                    _MenuFind.Enabled = _MenuFindNext.Enabled = _MenuReplace.Enabled = false;
                }
                else if(View == ViewState.PROPERTIES)
                {
                    if(!(ActiveControl is TextBox))
                    {
                        _MenuUndo.Enabled = false;
                        _MenuSelectAll.Enabled = _MenuDeselectAll.Enabled = _MenuCut.Enabled = _MenuCopy.Enabled = _MenuPaste.Enabled = _MenuDelete.Enabled = false;
                        _MenuFind.Enabled = _MenuFindNext.Enabled = _MenuReplace.Enabled = false;
                    }
                    else
                    {
                        _MenuUndo.Enabled = ((TextBox) ActiveControl).CanUndo;

                        _MenuDeselectAll.Enabled = _MenuDelete.Enabled = _MenuCut.Enabled = _MenuCopy.Enabled = (((TextBox) ActiveControl).SelectionLength > 0);
                        _MenuPaste.Enabled = Clipboard.ContainsText();

                        _MenuSelectAll.Enabled = (((TextBox) ActiveControl).SelectionLength < ((TextBox) ActiveControl).TextLength);

                        _MenuFind.Enabled = _MenuFindNext.Enabled = _MenuReplace.Enabled = false;
                    }
                }
                else if(View == ViewState.SOURCE)
                {
                    _MenuUndo.Enabled = _TextSource.CanUndo;

                    _MenuDeselectAll.Enabled = _MenuDelete.Enabled = _MenuCut.Enabled = _MenuCopy.Enabled = (_TextSource.SelectionLength > 0);
                    _MenuPaste.Enabled = Clipboard.ContainsText();

                    _MenuSelectAll.Enabled = (_TextSource.SelectionLength < _TextSource.TextLength);

                    _MenuFind.Enabled = (!_FindSearch.Visible);
                    _MenuFindNext.Enabled = _MenuReplace.Enabled = true;
                }
                else if(View == ViewState.DATA)
                {
                    if(_GridData.IsCurrentCellInEditMode)
                    {
                        _MenuUndo.Enabled = (_CurrentlyEditingCell.CanUndo);
                        _MenuCut.Enabled = _MenuCopy.Enabled = _MenuDelete.Enabled = (_CurrentlyEditingCell.SelectionLength > 0);
                        _MenuPaste.Enabled = Clipboard.ContainsText();
                        _MenuSelectAll.Enabled = _MenuDeselectAll.Enabled = true;

                        _MenuFind.Enabled = (!_FindSearch.Visible);
                        _MenuFindNext.Enabled = _MenuReplace.Enabled = true;
                    }
                    else
                    {
                        _MenuUndo.Enabled = false;

                        _MenuSelectAll.Enabled = (!_GridData.AreAllCellsSelected(true));
                        _MenuDeselectAll.Enabled = true;

                        _MenuCut.Enabled = _MenuCopy.Enabled = _MenuPaste.Enabled = (_GridData.SelectedCells.Count == 1);
                        _MenuDelete.Enabled = ((_GridData.SelectedCells.Count == 1) || (_GridData.SelectionMode == DataGridViewSelectionMode.RowHeaderSelect));
                        _MenuPaste.Enabled = _MenuPaste.Enabled && Clipboard.ContainsText();

                        _MenuFind.Enabled = (!_FindSearch.Visible);
                        _MenuFindNext.Enabled = _MenuReplace.Enabled = true;
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00115", ex); }
        }


        /// <summary>Menu "Edit" closed.</summary>
        private void _MenuEdit_DropDownClosed(object sender, EventArgs e)
        {
            _MenuUndo.Enabled = _MenuSelectAll.Enabled = _MenuDeselectAll.Enabled = _MenuCut.Enabled = _MenuCopy.Enabled = _MenuPaste.Enabled = _MenuDelete.Enabled =
                                _MenuFind.Enabled = _MenuFindNext.Enabled = true;
        }


        /// <summary>Menu "File" opening.</summary>
        private void _MenuFile_DropDownOpening(object sender, System.EventArgs e)
        {
            try
            {
                _MenuSave.Enabled = ((_Def != null) && _Def.Modified);
                _MenuSaveAs.Enabled = _MenuClose.Enabled = (_Def != null);
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00116", ex); }
        }


        /// <summary>Menu "File" closed.</summary>
        private void _MenuFile_DropDownClosed(object sender, EventArgs e)
        {
            _MenuSave.Enabled = _MenuSaveAs.Enabled = _MenuClose.Enabled = true;
        }


        /// <summary>Menu "View" opening.</summary>
        private void _MenuView_DropDownOpening(object sender, System.EventArgs e)
        {
            try
            {
                _MenuViewTable.Enabled = ((_Def != null) && (View != ViewState.DATA) && _Def.IsTable);
                _MenuViewSource.Enabled = ((_Def != null) && (View != ViewState.SOURCE));
                _MenuViewProperties.Enabled = ((_Def != null) && (View != ViewState.PROPERTIES));
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00117", ex); }
        }


        /// <summary>Menu "View" closed.</summary>
        private void _MenuView_DropDownClosed(object sender, EventArgs e)
        {
            _MenuViewTable.Enabled = _MenuViewSource.Enabled = _MenuViewProperties.Enabled = true;
        }


        /// <summary>Menu "Table" click.</summary>
        private void _MenuViewTable_Click(object sender, System.EventArgs e)
        {
            try
            {
                if(_Def == null) return;
                if(!_Def.IsTable) return;

                if(View == ViewState.SOURCE)
                {
                    _Def.Source = _TextSource.Text;
                    _Update();
                }

                View = ViewState.DATA;
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00118", ex); }
        }


        /// <summary>Menu "Properties" click.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MenuViewProperties_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Def == null) return;

                if(View == ViewState.SOURCE)
                {
                    _Def.Source = _TextSource.Text;
                    _Update();
                }

                View = ViewState.PROPERTIES;
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00119", ex); }
        }


        /// <summary>Menu "Source" click.</summary>
        private void _MenuViewSource_Click(object sender, System.EventArgs e)
        {
            try
            {
                if(_Def == null) return;

                _InitialSource = _Def.SavedSource;
                _TextSource.Text = _Def.Source;
                _TextSource.SelectionStart = _TextSource.SelectionLength = 0;

                View = ViewState.SOURCE;
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00120", ex); }
        }


        /// <summary>Menu "New Table" click.</summary>
        private void _MenuNewTable_Click(object sender, System.EventArgs e)
        {
            try
            {
                if(_CheckSaveCancel())
                {
                    _Def = new TTable();
                    _Update();
                    _UpdateRecentFiles();
                    _SetCaption();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00121", ex); }
        }


        /// <summary>Menu "New Definition" click.</summary>
        private void _MenuNewDefinition_Click(object sender, EventArgs e)
        {
            try
            {
                if(_CheckSaveCancel())
                {
                    _Def = new TLocale();
                    _Update();
                    _UpdateRecentFiles();
                    _SetCaption();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00122", ex); }
        }


        /// <summary>Menu "Open" click.</summary>
        private void _MenuOpen_Click(object sender, EventArgs e)
        {
            try
            {
                if(_CheckSaveCancel())
                {
                    OpenFileDialog d = new OpenFileDialog();

                    string m0 = Locale.Lookup("ledx::udiag.filter.ltab", "Locale Tables");
                    string m1 = Locale.Lookup("ledx::udiag.filter.ldef", "Locale Definitions");
                    string m2 = Locale.Lookup("ledx::udiag.filter.all", "All Files");

                    d.Filter = m0 + " (*.lstab, *.lsbtab, *.lsctab, *.phil7)|*.lstab;*.lsbtab;*.lsctab;*.phil7|" + m1 + " (*.locale)|*.locale;*.locale.information;*.locale.phil|" + m2 + "|*.*";

                    if(d.ShowDialog() == DialogResult.OK)
                    {
                        _Def = TLocale.Load(d.FileName);
                        _Update();

                        _RecentFiles.Add(d.FileName);
                        _UpdateRecentFiles();
                        _SetCaption();

                        Text = Path.GetFileName(_Def.FileName) + " - LEd";
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00123", ex); }
        }


        /// <summary>Menu "Close" click.</summary>
        private void _MenuClose_Click(object sender, EventArgs e)
        {
            if(_Def == null) return;

            try
            {
                if(_CheckSaveCancel())
                {
                    _GridData.DataSource = _Source = null;
                    _Def = null;

                    Text = "LEd";
                    View = ViewState.EMPTY;
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00124", ex); }
        }


        /// <summary>Menu "Save" click.</summary>
        private void _MenuSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Def == null) return;

                if(View == ViewState.SOURCE)
                {
                    _Def.Source = _TextSource.Text;
                    _Update();
                }

                if(_Def.Modified)
                {
                    _Save();
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00125", ex); }
        }


        /// <summary>Menu "Save as" click.</summary>
        private void _MenuSaveAs_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Def == null) return;
                _SaveAs();
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00126", ex); }
        }


        /// <summary>Menu "Exit" click.</summary>
        private void _MenuExit_Click(object sender, EventArgs e)
        {
            try
            {
                Close();
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00127", ex); }
        }


        /// <summary>Menu "Cut" click.</summary>
        private void _MenuCut_Click(object sender, EventArgs e)
        {
            try
            {
                if(ActiveControl == _FindSearch)
                {
                    _FindSearch.Cut();
                }
                else if(ActiveControl == _ReplaceSearch)
                {
                    _ReplaceSearch.Cut();
                }
                else if(View == ViewState.PROPERTIES)
                {
                    if(ActiveControl is TextBox)
                    {
                        ((TextBox) ActiveControl).Cut();
                    }
                }
                else if(View == ViewState.SOURCE)
                {
                    _TextSource.Cut();
                }
                else if(View == ViewState.DATA)
                {
                    try
                    {
                        if(_GridData.IsCurrentCellInEditMode)
                        {
                            _CurrentlyEditingCell.Cut();
                        }
                        else
                        {
                            Clipboard.SetText((string) _GridData.SelectedCells[0].Value);

                            _GridData.NotifyCurrentCellDirty(true);
                            _GridData.SelectedCells[0].Value = "";
                            _GridData.EndEdit();
                        }
                    }
                    catch(Exception) {}
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00128", ex); }
        }



        /// <summary>Menu "Copy" click-</summary>
        private void _MenuCopy_Click(object sender, EventArgs e)
        {
            try
            {
                if(ActiveControl == _FindSearch)
                {
                    _FindSearch.Copy();
                }
                else if(ActiveControl == _ReplaceSearch)
                {
                    _ReplaceSearch.Copy();
                }
                else if(View == ViewState.PROPERTIES)
                {
                    if(ActiveControl is TextBox)
                    {
                        ((TextBox) ActiveControl).Copy();
                    }
                }
                else if(View == ViewState.SOURCE)
                {
                    _TextSource.Copy();
                }
                else if(View == ViewState.DATA)
                {
                    try
                    {
                        if(_GridData.IsCurrentCellInEditMode)
                        {
                            _CurrentlyEditingCell.Copy();
                        }
                        else
                        {
                            Clipboard.SetText((string) _GridData.SelectedCells[0].Value);
                        }
                    }
                    catch(Exception) {}
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00129", ex); }
        }


        /// <summary>Menu "Paste" click.</summary>
        private void _MenuPaste_Click(object sender, EventArgs e)
        {
            try
            {
                if(ActiveControl == _FindSearch)
                {
                    _FindSearch.Paste();
                }
                else if(ActiveControl == _ReplaceSearch)
                {
                    _ReplaceSearch.Paste();
                }
                else if(View == ViewState.PROPERTIES)
                {
                    if(ActiveControl is TextBox)
                    {
                        ((TextBox) ActiveControl).Paste();
                    }
                }
                else if(View == ViewState.SOURCE)
                {
                    _TextSource.Paste();
                }
                else if(View == ViewState.DATA)
                {
                    try
                    {
                        if(_GridData.IsCurrentCellInEditMode)
                        {
                            _CurrentlyEditingCell.Paste();
                        }
                        else
                        {
                            _GridData.NotifyCurrentCellDirty(true);
                            _GridData.SelectedCells[0].Value = Clipboard.GetText();
                            _GridData.EndEdit();
                        }
                    }
                    catch(Exception) {}
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00130", ex); }
        }


        /// <summary>Menu "Select all" click.</summary>
        private void _MenuSelectAll_Click(object sender, EventArgs e)
        {
            try
            {
                if(ActiveControl == _FindSearch)
                {
                    _FindSearch.SelectAll();
                }
                else if(ActiveControl == _ReplaceSearch)
                {
                    _ReplaceSearch.SelectAll();
                }
                else if(View == ViewState.PROPERTIES)
                {
                    if(ActiveControl is TextBox)
                    {
                        ((TextBox) ActiveControl).SelectAll();
                    }
                }
                else if(View == ViewState.SOURCE)
                {
                    _TextSource.SelectAll();
                }
                else if(View == ViewState.DATA)
                {
                    try
                    {
                        if(_GridData.IsCurrentCellInEditMode)
                        {
                            _CurrentlyEditingCell.SelectAll();
                        }
                        _GridData.SelectAll();
                    }
                    catch(Exception) {}
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00131", ex); }
        }


        /// <summary>Menu "Deselect all" click.</summary>
        private void _MenuDeselectAll_Click(object sender, EventArgs e)
        {
            try
            {
                if(ActiveControl == _FindSearch)
                {
                    _FindSearch.DeselectAll();
                }
                else if(ActiveControl == _ReplaceSearch)
                {
                    _ReplaceSearch.DeselectAll();
                }
                else if(View == ViewState.PROPERTIES)
                {
                    if(ActiveControl is TextBox)
                    {
                        ((TextBox) ActiveControl).DeselectAll();
                    }
                }
                else if(View == ViewState.SOURCE)
                {
                    _TextSource.DeselectAll();
                }
                else if(View == ViewState.DATA)
                {
                    try
                    {
                        if(_GridData.IsCurrentCellInEditMode)
                        {
                            _CurrentlyEditingCell.DeselectAll();
                        }
                        else
                        {
                            _GridData.ClearSelection();
                        }
                    }
                    catch(Exception) {}
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00132", ex); }
        }


        /// <summary>Menu "Delete" click.</summary>
        private void _MenuDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if(View == ViewState.PROPERTIES)
                {
                    if(ActiveControl is TextBox)
                    {
                        if(((TextBox) ActiveControl).SelectionLength == 0) { ((TextBox) ActiveControl).SelectionLength = 1; }
                        ((TextBox) ActiveControl).SelectedText = "";
                    }
                }
                else if(View == ViewState.SOURCE)
                {
                    if(_TextSource.SelectionLength == 0) { _TextSource.SelectionLength = 1; }
                    _TextSource.SelectedText = "";
                }
                else if(View == ViewState.DATA)
                {
                    try
                    {
                        if(_GridData.IsCurrentCellInEditMode)
                        {
                            if(_CurrentlyEditingCell.SelectionLength == 0) { _CurrentlyEditingCell.SelectionLength = 1; }
                            _CurrentlyEditingCell.SelectedText = "";
                        }
                        else if(_GridData.SelectionMode == DataGridViewSelectionMode.RowHeaderSelect)
                        {
                            if(_GridData.SelectedRows.Count > 0)
                            {
                                if(MessageBox.Show("ledx::udiag.delrows.text".Localize("Do you really want to delete all selected rows?"), "ledx::udiag.delrows.caption".Localize("Delete"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                                {
                                    foreach(DataGridViewRow i in _GridData.SelectedRows)
                                    {
                                        _GridData.Rows.RemoveAt(i.Index);
                                    }
                                }
                            }
                            else
                            {
                                _GridData.NotifyCurrentCellDirty(true);
                                _GridData.SelectedCells[0].Value = "";
                                _GridData.EndEdit();
                            }
                        }
                    }
                    catch(Exception) {}
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00133", ex); }
        }


        /// <summary>Menu "Undo" click.</summary>
        private void _MenuUndo_Click(object sender, EventArgs e)
        {
            try
            {
                if(View == ViewState.PROPERTIES)
                {
                    if(ActiveControl is TextBox) { if(((TextBox) ActiveControl).CanUndo) ((TextBox) ActiveControl).Undo(); }
                }
                else if(View == ViewState.SOURCE)
                {
                    if(_TextSource.CanUndo) _TextSource.Undo();
                }
                else if(_GridData.IsCurrentCellInEditMode)
                {
                    try
                    {
                        if(_CurrentlyEditingCell.CanUndo) { _CurrentlyEditingCell.Undo(); }
                    }
                    catch(Exception) {}
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00134", ex); }
        }


        /// <summary>Menu "Find" click.</summary>
        private void _MenuFind_Click(object sender, EventArgs e)
        {
            try
            {
                _ReplaceSearch.Hide();
                _FindSearch.Show();
                _FindSearch.BringToFront();
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00135", ex); }
        }


        /// <summary>Menu "Replace" click.</summary>
        private void _MenuReplace_Click(object sender, EventArgs e)
        {
            try
            {
                _FindSearch.Hide();
                _ReplaceSearch_SearchTextChanged(null, null);
                _ReplaceSearch.Show();
                _ReplaceSearch.BringToFront();
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00136", ex); }
        }


        /// <summary>Menu "Find next" click.</summary>
        private void _MenuFindNext_Click(object sender, EventArgs e)
        {
            try
            {
                if(string.IsNullOrEmpty(_FindSearch.SearchText))
                {
                    _MenuFind_Click(sender, e);
                }
                else if(View == ViewState.DATA)
                {
                    _GridData.Find(FindControl.CurrentSearchText);
                }
                else if(View == ViewState.SOURCE)
                {
                    _FindSource(FindControl.CurrentSearchText);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00137", ex); }
        }


        /// <summary>Menu "Show help" click.</summary>
        private void _MenuShowHelp_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("http://robbiblubber.org/wiki.php?title=LEd%20Help");
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00138", ex); }
        }


        /// <summary>Menu "Import" click,</summary>
        private void _MenuImport_Click(object sender, EventArgs e)
        {
            try
            {
                if(_Def == null) return;
                if(!_Def.IsTable) return;

                OpenFileDialog d = new OpenFileDialog();

                string m0 = Locale.Lookup("ledx::udiag.filter.ldef", "Locale Definitions");
                string m1 = Locale.Lookup("ledx::udiag.filter.all", "All Files");

                d.Filter = m0 + " (*.lstab, *.lsctab)|*.lstab;*.lsctab|" + m1 + "|*.*";

                if(d.ShowDialog() == DialogResult.OK)
                {
                    int cnt = 0;
                    if(View == ViewState.DATA)
                    {
                        _GridData.DataSource = _Source = null;
                    }

                    try
                    {
                        TTable imp = (TTable) TLocale.Load(d.FileName);

                        foreach(TData i in imp.Data)
                        {
                            if(!((TTable) _Def).ContainsDefinition(i))
                            {
                                _Def.Data.Add(i);
                                cnt++;
                            }
                        }

                        if(View == ViewState.DATA)
                        {
                            _Update();
                        }
                        else if(View == ViewState.SOURCE)
                        {
                            _TextSource.Text = _Def.Source;
                        }
                    }
                    catch(Exception ex) { DebugOp.Dump("LEDX00140", ex); }

                    if(cnt == 0)
                    {
                        string mtext = Locale.Lookup("udiag.import.nothing", "Nothing imported.");
                        string mcap = Locale.Lookup("udiag.import.caption", "Import");
                        MessageBox.Show(mtext, mcap, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        string mtext = Locale.Lookup("udiag.import.nothing", "$(N) records successfully imported.").Replace("$(N)", cnt.ToString());
                        string mcap = Locale.Lookup("udiag.import.caption", "Import");
                        MessageBox.Show(mtext, mcap, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00139", ex); }
        }


        /// <summary>Table row validating.</summary>
        private void _GridData_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            try
            {
                if(e.RowIndex == (_GridData.RowCount - 1))
                {
                    if(string.IsNullOrWhiteSpace((string) _GridData.Rows[e.RowIndex].Cells[0].Value) && string.IsNullOrWhiteSpace((string) _GridData.Rows[e.RowIndex].Cells[1].Value))
                    {
                        return;
                    }
                }

                if(string.IsNullOrWhiteSpace((string) _GridData.Rows[e.RowIndex].Cells[0].Value))
                {
                    string mtext = Locale.Lookup("ledx::udiag.invalidkey.empty", "The key must not be empty.");
                    string mcap = Locale.Lookup("ledx::udiag.invalidkey.caption", "Invalid Key");

                    MessageBox.Show(mtext, mcap, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    e.Cancel = true;
                    return;
                }

                for(int i = 0; i < _GridData.RowCount; i++)
                {
                    if(i == e.RowIndex) continue;

                    if(((string) _GridData.Rows[i].Cells[0].Value) == ((string) _GridData.Rows[e.RowIndex].Cells[0].Value))
                    {
                        string mtext = Locale.Lookup("ledx::udiag.invalidkey.exists", "The key $(K) has already been defined.").Replace("$(K)", _GridData.Rows[e.RowIndex].Cells[0].Value.ToString());
                        string mcap = Locale.Lookup("ledx::udiag.invalidkey.caption", "Invalid Key");

                        MessageBox.Show(mtext, mcap, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        e.Cancel = true;
                        return;
                    }
                }
            }
            catch(Exception ex) { DebugOp.Dump("LEDX00141", ex); }

            _SetCaption();
        }


        /// <summary>Search requested.</summary>
        private void _FindSearch_Search(object sender, EventArgs e)
        {
            try
            {
                if(View == ViewState.SOURCE)
                {
                    _FindSource(FindControl.CurrentSearchText);
                }
                else
                {
                    _GridData.Find(FindControl.CurrentSearchText);
                }

                _ReplaceSearch_SearchTextChanged(null, null);
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00142", ex); }
        }


        /// <summary>Replace requested.</summary>
        private void _ReplaceSearch_Replace(object sender, EventArgs e)
        {
            try
            {
                if(View == ViewState.SOURCE)
                {
                    _TextSource.SelectedText = _ReplaceSearch.ReplaceText;
                }
                else if(View == ViewState.DATA)
                {
                    _GridData.NotifyCurrentCellDirty(true);
                    _GridData.CurrentCell.Value = ((string) _GridData.CurrentCell.Value).ReplaceFirst(_ReplaceSearch.SearchText, _ReplaceSearch.ReplaceText, false);
                    _GridData.EndEdit();
                }

                _FindSearch_Search(null, null);
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00143", ex); }
        }


        /// <summary>Replace all requested.</summary>
        private void _ReplaceSearch_ReplaceAll(object sender, EventArgs e)
        {
            try
            {
                if(View == ViewState.SOURCE)
                {
                    _TextSource.Text = _TextSource.Text.ReplaceAll(_ReplaceSearch.SearchText, _ReplaceSearch.ReplaceText, false);
                    _ReplaceSearch_SearchTextChanged(null, null);
                }
                else if(View == ViewState.DATA)
                {
                    for(int i = 0; i < _GridData.RowCount; i++)
                    {
                        for(int j = 0; j < 2; j++)
                        {
                            if(_GridData[j, i].Value != null)
                            {
                                _GridData.NotifyCurrentCellDirty(true);
                                _GridData[j, i].Value = ((string) _GridData[j, i].Value).ReplaceAll(_ReplaceSearch.SearchText, _ReplaceSearch.ReplaceText, false);
                                _GridData.EndEdit();
                            }
                        }
                    }
                    _ReplaceSearch_SearchTextChanged(null, null);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00144", ex); }
        }


        /// <summary>Replace search text changed.</summary>

        private void _ReplaceSearch_SearchTextChanged(object sender, EventArgs e)
        {
            try
            {
                if(string.IsNullOrEmpty(_ReplaceSearch.SearchText)) { _ReplaceSearch.AllowReplaceAll = _ReplaceSearch.AllowReplace = false; return; }
                _ReplaceSearch.AllowReplaceAll = true;

                if(View == ViewState.DATA)
                {
                    if(_GridData.CurrentCell.Value == null) { _ReplaceSearch.AllowReplace = false; return; }
                    _ReplaceSearch.AllowReplace = ((string) _GridData.CurrentCell.Value).ToLower().Contains(_ReplaceSearch.SearchText.ToLower());
                }
                else
                {
                    _ReplaceSearch.AllowReplace = (_TextSource.SelectedText.ToLower() == _ReplaceSearch.SearchText.ToLower());
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00145", ex); }
        }



        /// <summary>Form closing.</summary>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                e.Cancel = (!_CheckSaveCancel());
                LayoutOp.SaveLayout(this);
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00146", ex); }
        }


        /// <summary>Property change event handler.</summary>
        private void _OnPropertyChange(object sender, EventArgs e)
        {
            try
            {
                if(_Updating) return;

                if(_Def != null)
                {
                    if(_Def.IsTable)
                    {
                        _Def.Properties.Name = _TextName.Text;
                        _Def.Properties.Version = _TextVersion.Text;
                        ((TTable.TProperties) _Def.Properties).Prefixes = _TextPrefixes.Text;
                        _Def.Properties.Author = _TextAuthor.Text;
                        _Def.Properties.Application = _TextApplication.Text;
                        _Def.Properties.Key = _TextKey.Text;
                        _Def.Properties.Meta = _TextMeta.Text;
                    }
                    else
                    {
                        _Def.Properties.Name = _TextLocaleName.Text;
                        _Def.Properties.Version = _TextLocaleVersion.Text;
                        ((TLocale.TProperties) _Def.Properties).Base = _TextLocaleBase.Text;
                        ((TLocale.TProperties) _Def.Properties).IconFile = _TextLocaleIcon.Text;
                        ((TLocale.TProperties) _Def.Properties).SmallIconFile = _TextLocaleSmallIcon.Text;
                        ((TLocale.TProperties) _Def.Properties).IsDefault = _CheckDefault.Checked;
                        _Def.Properties.Author = _TextLocaleAuthor.Text;
                        _Def.Properties.Application = _TextLocaleApplication.Text;
                        _Def.Properties.Key = _TextLocaleKey.Text;
                        _Def.Properties.Meta = _TextLocaleMeta.Text;
                    }
                }

                _SetCaption();
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00147", ex); }
        }


        /// <summary>Form resize.</summary>
        private void FormMain_Resize(object sender, EventArgs e)
        {
            try
            {
                _TextSource.Height = Height - 68;
                _TextSource.Width = Width - 21;

                _GridData.Height = Height - 68;
                _GridData.Width = Width - 21;
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00148", ex); }
        }


        /// <summary>Menu "Settings" click.</summary>
        private void _MenuSettings_Click(object sender, EventArgs e)
        {
            try
            {
                FormSettings f = new FormSettings();

                if(f.ShowDialog() == DialogResult.OK)
                {
                    ShowDebug = f.ShowDebug;
                    _SaveSettings();

                    if(f.SelectedLocale != Locale.Selected)
                    {
                        Locale.Selected = f.SelectedLocale;
                        Locale.SaveSelection("Robbiblubber.Util.NET");
                        this.Localize();
                    }
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00149", ex); }
        }


        /// <summary>Menu "Start/Stop Logging" click.</summary>
        private void _MenuDebugStart_Click(object sender, EventArgs e)
        {
            DebugOp.Enabled = (!DebugOp.Enabled);
        }


        /// <summary>Menu "Save Dump" click.</summary>
        private void _MenuDebugSave_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            try
            {
                SaveFileDialog d = new SaveFileDialog();
                d.Filter = "debugx::udiag.filter.dump".Localize("Debug Dump Files") + " (*.debug.dump)|*.debug.dump|" + "debugx::udiag.filter.all".Localize("All Files") + "|*.*";

                if(d.ShowDialog() == DialogResult.OK)
                {
                    DebugOp.CreateDumpFile(d.FileName);
                }
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00150", ex); }
        }


        /// <summary>Menu "View Dump" click.</summary>
        private void _MenuDebugView_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            try
            {
                DebugOp.ShowLog();
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00151", ex); }
        }


        /// <summary>Menu "Upload Dump" click.</summary>
        private void _MenuDebugUpload_Click(object sender, EventArgs e)
        {
            if(!DebugOp.Enabled) return;

            try
            {
                DebugOp.UploadData("http://robbiblubber.lima-city.de/debug/Robbiblubber.Util.NET/");
            }
            catch(Exception ex) { DebugOp.DumpMessage("LEDX00152", ex); }
        }


        /// <summary>Menu "Debug" opening.</summary>
        private void _MenuDebug_DropDownOpening(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = (!(_MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = DebugOp.Enabled));
        }


        /// <summary>Menu "Debug" closed.</summary>
        private void _MenuDebug_DropDownClosed(object sender, EventArgs e)
        {
            _MenuDebugStart.Visible = _MenuDebugStop.Visible = _MenuDebugSave.Visible = _MenuDebugView.Visible = _MenuDebugUpload.Visible = _MenuDebugBlank0.Visible = true;
        }


        /// <summary>Grid editing control showing.</summary>
        private void _GridData_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if(e.Control is TextBoxBase) { _InternalCurrentlyEditingCell = (TextBoxBase) e.Control; }
        }


        /// <summary>Source text changed.</summary>
        private void _TextSource_TextChanged(object sender, EventArgs e)
        {
            _SetCaption(_TextSource.Text != _InitialSource);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] ViewState                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>View state enum.</summary>
        public enum ViewState: int
        {
            /// <summary>Show empty workspace.</summary>
            EMPTY = 0,
            /// <summary>Show data editor.</summary>
            DATA = 1,
            /// <summary>Show source editor.</summary>
            SOURCE = 2,
            /// <summary>Show property editor.</summary>
            PROPERTIES = 3
        }
    }
}
