﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Robbiblubber.Util.Coding;



namespace Robbiblubber.Util.Apps.LocaleEditor
{
    /// <summary>Locale definition class.</summary>
    public class TLocale: IDefinition
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Properties.</summary>
        private TProperties _Properties = new TProperties();

        /// <summary>Saved source.</summary>
        private string _SavedSource = "*";

        /// <summary>Modified flag.</summary>
        private bool _Modified = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public TLocale()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        public TLocale(string fileName)
        {
            if(fileName.ToLower().EndsWith(".locale.phil"))
            {
                _FromPhil(fileName);
            }
            else
            {
                IEncoder enc = Base64.Instance;
                if(fileName.ToLower().EndsWith(".locale.information")) { enc = PlainText.Instance; }

                _FromCfg(Ddp.Load(fileName, enc));
            }

            FileName = fileName;
            _SavedSource = _ToCfg().Text;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads locale dafinition from file.</summary>
        /// <param name="filename">File name.</param>
        public static IDefinition Load(string filename)
        {
            if((filename.ToLower().EndsWith(".locale")) || (filename.ToLower().EndsWith(".locale.information")) || (filename.ToLower().EndsWith(".locale.phil")))
            {
                return new TLocale(filename);
            }

            return new TTable(filename);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the definition source.</summary>
        public string Source
        {
            get
            {
                return _ToCfg().Text;
            }

            set
            {
                _FromCfg(Ddp.Parse(value));
            }
        }


        /// <summary>Gets if the definition is a table.</summary>
        public bool IsTable
        {
            get { return false; }
        }


        /// <summary>Gets the definition data.</summary>
        public List<TData> Data
        {
            get { return null; }
        }


        /// <summary>Gets the definition properties</summary>
        public IProperties Properties
        {
            get { return _Properties; }
        }


        /// <summary>Gets or sets the file name for this instace.</summary>
        public string FileName
        {
            get; set;
        }


        /// <summary>Gets a value indicating if the object has been modified.</summary>
        public bool Modified
        {
            get
            {
                if(_Modified) { return true; }
                return (_Modified = (_SavedSource != _ToCfg().Text));
            }
        }


        /// <summary>Gets the saved source.</summary>
        public string SavedSource
        {
            get { return _SavedSource; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the object.</summary>
        /// <param name="filename">File name.</param>
        public void Save(string filename)
        {
            FileName = filename;
            Save();
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            Ddp cfg = _ToCfg();

            if(FileName.ToLower().EndsWith(".locale.phil"))
            {
                File.WriteAllText(FileName, _ToPhil());
            }
            else
            {
                IEncoder enc = Base64.Instance;
                if(FileName.ToLower().EndsWith(".locale.information")) { enc = PlainText.Instance; }

                cfg.Save(FileName, enc);
            }

            _SavedSource = cfg.Text;
            _Modified = false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a configuration file object for this instance.</summary>
        /// <returns>Configuration file.</returns>
        private Ddp _ToCfg()
        {
            Ddp rval = new Ddp();

            if(string.IsNullOrWhiteSpace(_Properties.Version)) { _Properties.Version = "1.0"; }
            rval.SetValue("/locale/version", _Properties.Version);

            if(!string.IsNullOrWhiteSpace(_Properties.Name)) { rval.SetValue("/locale/name", _Properties.Name); }
            if(!string.IsNullOrWhiteSpace(_Properties.Application)) { rval.SetValue("/locale/application", _Properties.Application); }
            if(!string.IsNullOrWhiteSpace(_Properties.Author)) { rval.SetValue("/locale/author", _Properties.Author); }
            if(!string.IsNullOrWhiteSpace(_Properties.Key)) { rval.SetValue("/locale/lang", _Properties.Key); }
            if(!string.IsNullOrWhiteSpace(_Properties.Meta)) { rval.SetValue("/locale/meta", _Properties.Meta); }
            if(!string.IsNullOrWhiteSpace(_Properties.Base)) { rval.SetValue("/locale/base", _Properties.Base); }
            if(!string.IsNullOrWhiteSpace(_Properties.IconFile)) { rval.SetValue("/locale/icon", _Properties.IconFile); }
            if(!string.IsNullOrWhiteSpace(_Properties.SmallIconFile)) { rval.SetValue("/locale/sicon", _Properties.SmallIconFile); }
            rval.SetValue("/locale/default", _Properties.IsDefault);

            return rval;
        }


        /// <summary>Imports a configuration file object into this instance.</summary>
        /// <param name="cfg">Configuration file.</param>
        private void _FromCfg(Ddp cfg)
        {
            _Properties.Version = cfg.GetString("/locale/version");
            _Properties.Name = cfg.GetString("/locale/name");
            _Properties.Application = cfg.GetString("/locale/application");
            _Properties.Author = cfg.GetString("/locale/author");
            _Properties.Key = cfg.GetString("/locale/lang");
            _Properties.Meta = cfg.GetString("/locale/meta");
            _Properties.Base = cfg.GetString("/locale/base");
            _Properties.IconFile = cfg.GetString("/locale/icon");
            _Properties.SmallIconFile = cfg.GetString("/locale/sicon");
            _Properties.IsDefault = cfg.GetBoolean("/locale/default");
        }


        /// <summary>Returns phil-file contents.</summary>
        /// <returns>PHP code.</returns>
        private string _ToPhil()
        {
            StringBuilder rval = new StringBuilder();

            rval.AppendLine(@"<?php namespace Robbiblubber\Util\Localization;");
            rval.AppendLine();
            rval.AppendLine("$this->__lfeed(");
            rval.AppendLine("'" + TTable.__MaskPhil(_Properties.Version) + "',");
            rval.AppendLine("'" + TTable.__MaskPhil(_Properties.Name) + "',");
            rval.AppendLine("'" + TTable.__MaskPhil(_Properties.Application) + "',");
            rval.AppendLine("'" + TTable.__MaskPhil(_Properties.Author) + "',");
            rval.AppendLine("'" + TTable.__MaskPhil(_Properties.Key) + "',");
            rval.AppendLine("'" + TTable.__MaskPhil(_Properties.Meta) + "',");
            rval.AppendLine("'" + TTable.__MaskPhil(_Properties.Base) + "',");
            rval.AppendLine("'" + TTable.__MaskPhil(_Properties.IconFile) + "',");
            rval.AppendLine("'" + TTable.__MaskPhil(_Properties.SmallIconFile) + "',");
            rval.AppendLine(_Properties.IsDefault ? "TRUE" : "FALSE");
            rval.AppendLine(");");
            rval.Append("?>");

            return rval.ToString();
        }


        /// <summary>Imports a PHP file into this instance.</summary>
        /// <param name="fileName">File name.</param>
        private void _FromPhil(string fileName)
        {
            string[] lines = File.ReadAllLines(fileName);

            _Properties.Version = lines[3].Substring(1, lines[3].Length - 3).Replace(@"\\", @"\").Replace("\'", @"'");
            _Properties.Name = lines[4].Substring(1, lines[4].Length - 3).Replace(@"\\", @"\").Replace("\'", @"'");
            _Properties.Application = lines[5].Substring(1, lines[5].Length - 3).Replace(@"\\", @"\").Replace("\'", @"'");
            _Properties.Author = lines[6].Substring(1, lines[6].Length - 3).Replace(@"\\", @"\").Replace("\'", @"'");
            _Properties.Key = lines[7].Substring(1, lines[7].Length - 3).Replace(@"\\", @"\").Replace("\'", @"'");
            _Properties.Meta = lines[8].Substring(1, lines[8].Length - 3).Replace(@"\\", @"\").Replace("\'", @"'");
            _Properties.Base = lines[9].Substring(1, lines[9].Length - 3).Replace(@"\\", @"\").Replace("\'", @"'");
            _Properties.IconFile = lines[10].Substring(1, lines[10].Length - 3).Replace(@"\\", @"\").Replace("\'", @"'");
            _Properties.SmallIconFile = lines[11].Substring(1, lines[11].Length - 3).Replace(@"\\", @"\").Replace("\'", @"'");
            _Properties.IsDefault = (lines[12].ToLower().Trim() == "true");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] TProperties                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class provides locale properties.</summary>
        public class TProperties: IProperties
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            public TProperties()
            {
                Version = "1.0";
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets or sets the locale definition name.</summary>
            public string Name { get; set; }

            /// <summary>Gets or sets the locale definition version.</summary>
            public string Version { get; set; }

            /// <summary>Gets or sets the locale definition application name.</summary>
            public string Application { get; set; }

            /// <summary>Gets or sets the locale definition author.</summary>
            public string Author { get; set; }

            /// <summary>Gets or sets the locale language key.</summary>
            public string Key { get; set; }

            /// <summary>Gets or sets the locale definition metadata.</summary>
            public string Meta { get; set; }

            /// <summary>Gets or sets the locale definition base keys.</summary>
            public string Base { get; set; }

            /// <summary>Gets or sets the locale definition icon file.</summary>
            public string IconFile { get; set; }

            /// <summary>Gets or sets the locale definition small icon file.</summary>
            public string SmallIconFile { get; set; }

            /// <summary>Gets or sets if the locale definition is default.</summary>
            public bool IsDefault { get; set; }
        }
    }
}
