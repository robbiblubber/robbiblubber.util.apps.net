﻿using System;
using Robbiblubber.Util.Debug;

namespace Robbiblubber.Util.Apps.LocaleEditor
{
    /// <summary>This class represents locale data.</summary>
    public class TData: IComparable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public TData()
        {
            Symbol = Expression = "";
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="symbol">Symbol.</param>
        /// <param name="expression">Expression.</param>
        public TData(string symbol, string expression)
        {
            Symbol = symbol;
            Expression = expression;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the data symbol.</summary>
        public string Symbol { get; set; }

        /// <summary>Gets or sets the data expression.</summary>
        public string Expression { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IComparable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares this instance to an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Comparison result.</returns>
        int IComparable.CompareTo(object obj)
        {
            try
            {
                if(obj is TData)
                {
                    return Symbol.CompareTo(((TData) obj).Symbol);
                }
                else if(obj is string)
                {
                    return Symbol.CompareTo((string) obj);
                }
            }
            catch(Exception ex) { DebugOp.Dump("LEDX00180", ex); }

            return 1;
        }
    }
}
