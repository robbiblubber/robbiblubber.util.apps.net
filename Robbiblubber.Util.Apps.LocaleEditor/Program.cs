﻿using System;
using System.Threading;
using System.Windows.Forms;



namespace Robbiblubber.Util.Apps.LocaleEditor
{
    /// <summary>Program class.</summary>
    static class Program
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal members                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Main form.</summary>
        internal static FormMain __Main;

        /// <summary>Splash form.</summary>
        internal static FormSplash __Splash;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // entry point                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The main entry point for the application.</summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            __Splash = new FormSplash();
            __Splash.Show();
            Application.DoEvents();

            Thread.Sleep(1600);

            string file = null;
            if(args.Length > 0) { file = args[0]; }

            __Main = new FormMain(file);
            __Splash.DelayClose();
            Application.Run(__Main);
        }
    }
}
