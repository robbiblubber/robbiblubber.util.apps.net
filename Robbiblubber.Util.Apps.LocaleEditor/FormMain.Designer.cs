﻿namespace Robbiblubber.Util.Apps.LocaleEditor
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this._MenuStripMain = new System.Windows.Forms.MenuStrip();
            this._MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuNew = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuNewTable = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuNewDefinition = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSeparatorFile0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuClose = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSeparatorFile1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuRecent = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuUndo = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSeaparatorEdit0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSaparatorEdit1 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDeselectAll = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuEditBlank2 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuFind = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuFindNext = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuReplace = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuViewTable = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuViewSource = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuViewBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuViewProperties = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuTools = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuImport = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuToolsBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStart = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugStop = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugBlank0 = new System.Windows.Forms.ToolStripSeparator();
            this._MenuDebugSave = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugView = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuDebugUpload = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuShowHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._MenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this._PanelProperties = new System.Windows.Forms.Panel();
            this._TextMeta = new System.Windows.Forms.TextBox();
            this._LabelMeta = new System.Windows.Forms.Label();
            this._TextPrefixes = new System.Windows.Forms.TextBox();
            this._LabelPrefixes = new System.Windows.Forms.Label();
            this._TextKey = new System.Windows.Forms.TextBox();
            this._LabelKey = new System.Windows.Forms.Label();
            this._TextAuthor = new System.Windows.Forms.TextBox();
            this._LabelAuthor = new System.Windows.Forms.Label();
            this._TextApplication = new System.Windows.Forms.TextBox();
            this._LabelApplication = new System.Windows.Forms.Label();
            this._TextVersion = new System.Windows.Forms.TextBox();
            this._LabelVersion = new System.Windows.Forms.Label();
            this._TextName = new System.Windows.Forms.TextBox();
            this._LabelName = new System.Windows.Forms.Label();
            this._GridData = new System.Windows.Forms.DataGridView();
            this._PanelEmpty = new System.Windows.Forms.Panel();
            this._TextSource = new System.Windows.Forms.TextBox();
            this._ToolTip = new System.Windows.Forms.ToolTip(this.components);
            this._PanelLocaleProperties = new System.Windows.Forms.Panel();
            this._CheckDefault = new System.Windows.Forms.CheckBox();
            this._TextLocaleSmallIcon = new System.Windows.Forms.TextBox();
            this._LabelLocaleSmallIcon = new System.Windows.Forms.Label();
            this._TextLocaleIcon = new System.Windows.Forms.TextBox();
            this._LabelLocaleIcon = new System.Windows.Forms.Label();
            this._TextLocaleMeta = new System.Windows.Forms.TextBox();
            this._LabelLocaleMeta = new System.Windows.Forms.Label();
            this._TextLocaleBase = new System.Windows.Forms.TextBox();
            this._LabelLocaleBase = new System.Windows.Forms.Label();
            this._TextLocaleKey = new System.Windows.Forms.TextBox();
            this._LabelLocaleKey = new System.Windows.Forms.Label();
            this._TextLocaleAuthor = new System.Windows.Forms.TextBox();
            this._LabelLocaleAuthor = new System.Windows.Forms.Label();
            this._TextLocaleApplication = new System.Windows.Forms.TextBox();
            this._LabelLocaleApplication = new System.Windows.Forms.Label();
            this._TextLocaleVersion = new System.Windows.Forms.TextBox();
            this._LabelLocaleVersion = new System.Windows.Forms.Label();
            this._TextLocaleName = new System.Windows.Forms.TextBox();
            this._LabelLocaleName = new System.Windows.Forms.Label();
            this._FindSearch = new Robbiblubber.Util.Controls.FindControl();
            this._ReplaceSearch = new Robbiblubber.Util.Controls.ReplaceControl();
            this._MenuStripMain.SuspendLayout();
            this._PanelProperties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._GridData)).BeginInit();
            this._PanelLocaleProperties.SuspendLayout();
            this.SuspendLayout();
            // 
            // _MenuStripMain
            // 
            this._MenuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuFile,
            this._MenuEdit,
            this._MenuView,
            this._MenuTools,
            this._MenuHelp});
            this._MenuStripMain.Location = new System.Drawing.Point(0, 0);
            this._MenuStripMain.Name = "_MenuStripMain";
            this._MenuStripMain.Size = new System.Drawing.Size(989, 24);
            this._MenuStripMain.TabIndex = 0;
            this._MenuStripMain.Text = "menuStrip1";
            // 
            // _MenuFile
            // 
            this._MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuNew,
            this._MenuOpen,
            this._MenuSave,
            this._MenuSaveAs,
            this._MenuSeparatorFile0,
            this._MenuClose,
            this._MenuSeparatorFile1,
            this._MenuRecent,
            this._MenuExit});
            this._MenuFile.Name = "_MenuFile";
            this._MenuFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._MenuFile.Size = new System.Drawing.Size(37, 20);
            this._MenuFile.Tag = "ledx::menu.file";
            this._MenuFile.Text = "&File";
            this._MenuFile.DropDownClosed += new System.EventHandler(this._MenuFile_DropDownClosed);
            this._MenuFile.DropDownOpening += new System.EventHandler(this._MenuFile_DropDownOpening);
            // 
            // _MenuNew
            // 
            this._MenuNew.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuNewTable,
            this._MenuNewDefinition});
            this._MenuNew.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.star0;
            this._MenuNew.Name = "_MenuNew";
            this._MenuNew.Size = new System.Drawing.Size(195, 22);
            this._MenuNew.Tag = "ledx::menu.new";
            this._MenuNew.Text = "&New";
            // 
            // _MenuNewTable
            // 
            this._MenuNewTable.Image = ((System.Drawing.Image)(resources.GetObject("_MenuNewTable.Image")));
            this._MenuNewTable.Name = "_MenuNewTable";
            this._MenuNewTable.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this._MenuNewTable.Size = new System.Drawing.Size(228, 22);
            this._MenuNewTable.Tag = "ledx::menu.newtab";
            this._MenuNewTable.Text = "&Table...";
            this._MenuNewTable.Click += new System.EventHandler(this._MenuNewTable_Click);
            // 
            // _MenuNewDefinition
            // 
            this._MenuNewDefinition.Image = ((System.Drawing.Image)(resources.GetObject("_MenuNewDefinition.Image")));
            this._MenuNewDefinition.Name = "_MenuNewDefinition";
            this._MenuNewDefinition.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this._MenuNewDefinition.Size = new System.Drawing.Size(228, 22);
            this._MenuNewDefinition.Tag = "ledx::menu.newloc";
            this._MenuNewDefinition.Text = "&Locale Information...";
            this._MenuNewDefinition.Click += new System.EventHandler(this._MenuNewDefinition_Click);
            // 
            // _MenuOpen
            // 
            this._MenuOpen.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.open1;
            this._MenuOpen.Name = "_MenuOpen";
            this._MenuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this._MenuOpen.Size = new System.Drawing.Size(195, 22);
            this._MenuOpen.Tag = "ledx::menu.open";
            this._MenuOpen.Text = "&Open...";
            this._MenuOpen.Click += new System.EventHandler(this._MenuOpen_Click);
            // 
            // _MenuSave
            // 
            this._MenuSave.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.save;
            this._MenuSave.Name = "_MenuSave";
            this._MenuSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this._MenuSave.Size = new System.Drawing.Size(195, 22);
            this._MenuSave.Tag = "ledx::menu.save";
            this._MenuSave.Text = "&Save";
            this._MenuSave.Click += new System.EventHandler(this._MenuSave_Click);
            // 
            // _MenuSaveAs
            // 
            this._MenuSaveAs.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.saveas;
            this._MenuSaveAs.Name = "_MenuSaveAs";
            this._MenuSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this._MenuSaveAs.Size = new System.Drawing.Size(195, 22);
            this._MenuSaveAs.Tag = "ledx::menu.saveas";
            this._MenuSaveAs.Text = "Save &As...";
            this._MenuSaveAs.Click += new System.EventHandler(this._MenuSaveAs_Click);
            // 
            // _MenuSeparatorFile0
            // 
            this._MenuSeparatorFile0.Name = "_MenuSeparatorFile0";
            this._MenuSeparatorFile0.Size = new System.Drawing.Size(192, 6);
            // 
            // _MenuClose
            // 
            this._MenuClose.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.close;
            this._MenuClose.Name = "_MenuClose";
            this._MenuClose.Size = new System.Drawing.Size(195, 22);
            this._MenuClose.Tag = "ledx::menu.close";
            this._MenuClose.Text = "&Close";
            this._MenuClose.Click += new System.EventHandler(this._MenuClose_Click);
            // 
            // _MenuSeparatorFile1
            // 
            this._MenuSeparatorFile1.Name = "_MenuSeparatorFile1";
            this._MenuSeparatorFile1.Size = new System.Drawing.Size(192, 6);
            // 
            // _MenuRecent
            // 
            this._MenuRecent.Name = "_MenuRecent";
            this._MenuRecent.Size = new System.Drawing.Size(195, 22);
            this._MenuRecent.Tag = "ledx::menu.recent";
            this._MenuRecent.Text = "&Recent Files";
            // 
            // _MenuExit
            // 
            this._MenuExit.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.exit1;
            this._MenuExit.Name = "_MenuExit";
            this._MenuExit.Size = new System.Drawing.Size(195, 22);
            this._MenuExit.Tag = "ledx::menu.exit";
            this._MenuExit.Text = "E&xit";
            this._MenuExit.Click += new System.EventHandler(this._MenuExit_Click);
            // 
            // _MenuEdit
            // 
            this._MenuEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuUndo,
            this._MenuSeaparatorEdit0,
            this._MenuCut,
            this._MenuCopy,
            this._MenuPaste,
            this._MenuDelete,
            this._MenuSaparatorEdit1,
            this._MenuSelectAll,
            this._MenuDeselectAll,
            this._MenuEditBlank2,
            this._MenuFind,
            this._MenuFindNext,
            this._MenuReplace});
            this._MenuEdit.Name = "_MenuEdit";
            this._MenuEdit.Size = new System.Drawing.Size(39, 20);
            this._MenuEdit.Tag = "ledx::menu.edit";
            this._MenuEdit.Text = "&Edit";
            this._MenuEdit.DropDownClosed += new System.EventHandler(this._MenuEdit_DropDownClosed);
            this._MenuEdit.DropDownOpening += new System.EventHandler(this._MenuEdit_DropDownOpening);
            // 
            // _MenuUndo
            // 
            this._MenuUndo.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.undo;
            this._MenuUndo.Name = "_MenuUndo";
            this._MenuUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this._MenuUndo.Size = new System.Drawing.Size(209, 22);
            this._MenuUndo.Tag = "ledx::menu.undo";
            this._MenuUndo.Text = "&Undo";
            this._MenuUndo.Click += new System.EventHandler(this._MenuUndo_Click);
            // 
            // _MenuSeaparatorEdit0
            // 
            this._MenuSeaparatorEdit0.Name = "_MenuSeaparatorEdit0";
            this._MenuSeaparatorEdit0.Size = new System.Drawing.Size(206, 6);
            // 
            // _MenuCut
            // 
            this._MenuCut.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.cut;
            this._MenuCut.Name = "_MenuCut";
            this._MenuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this._MenuCut.Size = new System.Drawing.Size(209, 22);
            this._MenuCut.Tag = "ledx::menu.cut";
            this._MenuCut.Text = "Cu&t";
            this._MenuCut.Click += new System.EventHandler(this._MenuCut_Click);
            // 
            // _MenuCopy
            // 
            this._MenuCopy.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.copy1;
            this._MenuCopy.Name = "_MenuCopy";
            this._MenuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this._MenuCopy.Size = new System.Drawing.Size(209, 22);
            this._MenuCopy.Tag = "ledx::menu.copy";
            this._MenuCopy.Text = "&Copy";
            this._MenuCopy.Click += new System.EventHandler(this._MenuCopy_Click);
            // 
            // _MenuPaste
            // 
            this._MenuPaste.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.paste1;
            this._MenuPaste.Name = "_MenuPaste";
            this._MenuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this._MenuPaste.Size = new System.Drawing.Size(209, 22);
            this._MenuPaste.Tag = "ledx::menu.paste";
            this._MenuPaste.Text = "&Paste";
            this._MenuPaste.Click += new System.EventHandler(this._MenuPaste_Click);
            // 
            // _MenuDelete
            // 
            this._MenuDelete.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.remove;
            this._MenuDelete.Name = "_MenuDelete";
            this._MenuDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this._MenuDelete.Size = new System.Drawing.Size(209, 22);
            this._MenuDelete.Tag = "ledx::menu.delete";
            this._MenuDelete.Text = "&Delete";
            this._MenuDelete.Click += new System.EventHandler(this._MenuDelete_Click);
            // 
            // _MenuSaparatorEdit1
            // 
            this._MenuSaparatorEdit1.Name = "_MenuSaparatorEdit1";
            this._MenuSaparatorEdit1.Size = new System.Drawing.Size(206, 6);
            // 
            // _MenuSelectAll
            // 
            this._MenuSelectAll.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.selectall;
            this._MenuSelectAll.Name = "_MenuSelectAll";
            this._MenuSelectAll.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this._MenuSelectAll.Size = new System.Drawing.Size(209, 22);
            this._MenuSelectAll.Tag = "ledx::menu.selectall";
            this._MenuSelectAll.Text = "Select &All";
            this._MenuSelectAll.Click += new System.EventHandler(this._MenuSelectAll_Click);
            // 
            // _MenuDeselectAll
            // 
            this._MenuDeselectAll.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.deselectall;
            this._MenuDeselectAll.Name = "_MenuDeselectAll";
            this._MenuDeselectAll.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.A)));
            this._MenuDeselectAll.Size = new System.Drawing.Size(209, 22);
            this._MenuDeselectAll.Tag = "ledx::menu.deselectall";
            this._MenuDeselectAll.Text = "&Deselect All";
            this._MenuDeselectAll.Click += new System.EventHandler(this._MenuDeselectAll_Click);
            // 
            // _MenuEditBlank2
            // 
            this._MenuEditBlank2.Name = "_MenuEditBlank2";
            this._MenuEditBlank2.Size = new System.Drawing.Size(206, 6);
            // 
            // _MenuFind
            // 
            this._MenuFind.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.find;
            this._MenuFind.Name = "_MenuFind";
            this._MenuFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this._MenuFind.Size = new System.Drawing.Size(209, 22);
            this._MenuFind.Tag = "ledx::menu.find";
            this._MenuFind.Text = "&Find...";
            this._MenuFind.Click += new System.EventHandler(this._MenuFind_Click);
            // 
            // _MenuFindNext
            // 
            this._MenuFindNext.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.findnext;
            this._MenuFindNext.Name = "_MenuFindNext";
            this._MenuFindNext.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this._MenuFindNext.Size = new System.Drawing.Size(209, 22);
            this._MenuFindNext.Tag = "ledx::menu.findnext";
            this._MenuFindNext.Text = "Find &Next";
            this._MenuFindNext.Click += new System.EventHandler(this._MenuFindNext_Click);
            // 
            // _MenuReplace
            // 
            this._MenuReplace.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.replace;
            this._MenuReplace.Name = "_MenuReplace";
            this._MenuReplace.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this._MenuReplace.Size = new System.Drawing.Size(209, 22);
            this._MenuReplace.Tag = "ledx::menu.replace";
            this._MenuReplace.Text = "&Replace...";
            this._MenuReplace.Click += new System.EventHandler(this._MenuReplace_Click);
            // 
            // _MenuView
            // 
            this._MenuView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuViewTable,
            this._MenuViewSource,
            this._MenuViewBlank0,
            this._MenuViewProperties});
            this._MenuView.Name = "_MenuView";
            this._MenuView.Size = new System.Drawing.Size(44, 20);
            this._MenuView.Tag = "ledx::menu.view";
            this._MenuView.Text = "&View";
            this._MenuView.DropDownClosed += new System.EventHandler(this._MenuView_DropDownClosed);
            this._MenuView.DropDownOpening += new System.EventHandler(this._MenuView_DropDownOpening);
            // 
            // _MenuViewTable
            // 
            this._MenuViewTable.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.table1;
            this._MenuViewTable.Name = "_MenuViewTable";
            this._MenuViewTable.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this._MenuViewTable.Size = new System.Drawing.Size(152, 22);
            this._MenuViewTable.Tag = "ledx::menu.table";
            this._MenuViewTable.Text = "&Table";
            this._MenuViewTable.Click += new System.EventHandler(this._MenuViewTable_Click);
            // 
            // _MenuViewSource
            // 
            this._MenuViewSource.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.code;
            this._MenuViewSource.Name = "_MenuViewSource";
            this._MenuViewSource.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this._MenuViewSource.Size = new System.Drawing.Size(152, 22);
            this._MenuViewSource.Tag = "ledx::menu.source";
            this._MenuViewSource.Text = "&Source";
            this._MenuViewSource.Click += new System.EventHandler(this._MenuViewSource_Click);
            // 
            // _MenuViewBlank0
            // 
            this._MenuViewBlank0.Name = "_MenuViewBlank0";
            this._MenuViewBlank0.Size = new System.Drawing.Size(149, 6);
            // 
            // _MenuViewProperties
            // 
            this._MenuViewProperties.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.properties1;
            this._MenuViewProperties.Name = "_MenuViewProperties";
            this._MenuViewProperties.ShortcutKeys = System.Windows.Forms.Keys.F12;
            this._MenuViewProperties.Size = new System.Drawing.Size(152, 22);
            this._MenuViewProperties.Tag = "ledx::menu.properties";
            this._MenuViewProperties.Text = "&Properties";
            this._MenuViewProperties.Click += new System.EventHandler(this._MenuViewProperties_Click);
            // 
            // _MenuTools
            // 
            this._MenuTools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuImport,
            this._MenuToolsBlank0,
            this._MenuDebug,
            this._MenuSettings});
            this._MenuTools.Name = "_MenuTools";
            this._MenuTools.Size = new System.Drawing.Size(46, 20);
            this._MenuTools.Tag = "ledx::menu.tools";
            this._MenuTools.Text = "&Tools";
            this._MenuTools.DropDownClosed += new System.EventHandler(this._MenuTools_DropDownClosed);
            this._MenuTools.DropDownOpening += new System.EventHandler(this._MenuTools_DropDownOpening);
            // 
            // _MenuImport
            // 
            this._MenuImport.Image = global::Robbiblubber.Util.Apps.LocaleEditor.Resources.import1;
            this._MenuImport.Name = "_MenuImport";
            this._MenuImport.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this._MenuImport.Size = new System.Drawing.Size(180, 22);
            this._MenuImport.Tag = "ledx::menu.import";
            this._MenuImport.Text = "&Import...";
            this._MenuImport.Click += new System.EventHandler(this._MenuImport_Click);
            // 
            // _MenuToolsBlank0
            // 
            this._MenuToolsBlank0.Name = "_MenuToolsBlank0";
            this._MenuToolsBlank0.Size = new System.Drawing.Size(177, 6);
            // 
            // _MenuDebug
            // 
            this._MenuDebug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuDebugStart,
            this._MenuDebugStop,
            this._MenuDebugBlank0,
            this._MenuDebugSave,
            this._MenuDebugView,
            this._MenuDebugUpload});
            this._MenuDebug.Name = "_MenuDebug";
            this._MenuDebug.Size = new System.Drawing.Size(180, 22);
            this._MenuDebug.Tag = "debugx::menu.debug";
            this._MenuDebug.Text = "&Debug";
            this._MenuDebug.DropDownClosed += new System.EventHandler(this._MenuDebug_DropDownClosed);
            this._MenuDebug.DropDownOpening += new System.EventHandler(this._MenuDebug_DropDownOpening);
            // 
            // _MenuDebugStart
            // 
            this._MenuDebugStart.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStart.Image")));
            this._MenuDebugStart.Name = "_MenuDebugStart";
            this._MenuDebugStart.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStart.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStart.Tag = "debugx::menu.startlogging";
            this._MenuDebugStart.Text = "Start &Logging";
            this._MenuDebugStart.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugStop
            // 
            this._MenuDebugStop.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugStop.Image")));
            this._MenuDebugStop.Name = "_MenuDebugStop";
            this._MenuDebugStop.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.B)));
            this._MenuDebugStop.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugStop.Tag = "debugx::menu.stoplogging";
            this._MenuDebugStop.Text = "Stop &Logging";
            this._MenuDebugStop.Click += new System.EventHandler(this._MenuDebugStart_Click);
            // 
            // _MenuDebugBlank0
            // 
            this._MenuDebugBlank0.Name = "_MenuDebugBlank0";
            this._MenuDebugBlank0.Size = new System.Drawing.Size(219, 6);
            // 
            // _MenuDebugSave
            // 
            this._MenuDebugSave.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugSave.Image")));
            this._MenuDebugSave.Name = "_MenuDebugSave";
            this._MenuDebugSave.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.D)));
            this._MenuDebugSave.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugSave.Tag = "debugx::menu.savedump";
            this._MenuDebugSave.Text = "&Save Dump...";
            this._MenuDebugSave.Click += new System.EventHandler(this._MenuDebugSave_Click);
            // 
            // _MenuDebugView
            // 
            this._MenuDebugView.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugView.Image")));
            this._MenuDebugView.Name = "_MenuDebugView";
            this._MenuDebugView.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.V)));
            this._MenuDebugView.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugView.Tag = "debugx::menu.viewdump";
            this._MenuDebugView.Text = "&View Dump...";
            this._MenuDebugView.Click += new System.EventHandler(this._MenuDebugView_Click);
            // 
            // _MenuDebugUpload
            // 
            this._MenuDebugUpload.Image = ((System.Drawing.Image)(resources.GetObject("_MenuDebugUpload.Image")));
            this._MenuDebugUpload.Name = "_MenuDebugUpload";
            this._MenuDebugUpload.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.U)));
            this._MenuDebugUpload.Size = new System.Drawing.Size(222, 22);
            this._MenuDebugUpload.Tag = "debugx::menu.uploaddump";
            this._MenuDebugUpload.Text = "&Upload Dump...";
            this._MenuDebugUpload.Click += new System.EventHandler(this._MenuDebugUpload_Click);
            // 
            // _MenuSettings
            // 
            this._MenuSettings.Image = ((System.Drawing.Image)(resources.GetObject("_MenuSettings.Image")));
            this._MenuSettings.Name = "_MenuSettings";
            this._MenuSettings.Size = new System.Drawing.Size(180, 22);
            this._MenuSettings.Tag = "ledx::menu.settings";
            this._MenuSettings.Text = "&Settings...";
            this._MenuSettings.Click += new System.EventHandler(this._MenuSettings_Click);
            // 
            // _MenuHelp
            // 
            this._MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._MenuShowHelp,
            this._MenuAbout});
            this._MenuHelp.Name = "_MenuHelp";
            this._MenuHelp.Size = new System.Drawing.Size(44, 20);
            this._MenuHelp.Tag = "ledx::menu.help";
            this._MenuHelp.Text = "&Help";
            // 
            // _MenuShowHelp
            // 
            this._MenuShowHelp.Image = ((System.Drawing.Image)(resources.GetObject("_MenuShowHelp.Image")));
            this._MenuShowHelp.Name = "_MenuShowHelp";
            this._MenuShowHelp.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this._MenuShowHelp.Size = new System.Drawing.Size(180, 22);
            this._MenuShowHelp.Tag = "ledx::menu.showhelp";
            this._MenuShowHelp.Text = "Show &Help...";
            this._MenuShowHelp.Click += new System.EventHandler(this._MenuShowHelp_Click);
            // 
            // _MenuAbout
            // 
            this._MenuAbout.Name = "_MenuAbout";
            this._MenuAbout.Size = new System.Drawing.Size(180, 22);
            this._MenuAbout.Tag = "ledx::menu.about";
            this._MenuAbout.Text = "About...";
            this._MenuAbout.Click += new System.EventHandler(this._MenuAbout_Click);
            // 
            // _PanelProperties
            // 
            this._PanelProperties.Controls.Add(this._TextMeta);
            this._PanelProperties.Controls.Add(this._LabelMeta);
            this._PanelProperties.Controls.Add(this._TextPrefixes);
            this._PanelProperties.Controls.Add(this._LabelPrefixes);
            this._PanelProperties.Controls.Add(this._TextKey);
            this._PanelProperties.Controls.Add(this._LabelKey);
            this._PanelProperties.Controls.Add(this._TextAuthor);
            this._PanelProperties.Controls.Add(this._LabelAuthor);
            this._PanelProperties.Controls.Add(this._TextApplication);
            this._PanelProperties.Controls.Add(this._LabelApplication);
            this._PanelProperties.Controls.Add(this._TextVersion);
            this._PanelProperties.Controls.Add(this._LabelVersion);
            this._PanelProperties.Controls.Add(this._TextName);
            this._PanelProperties.Controls.Add(this._LabelName);
            this._PanelProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PanelProperties.Location = new System.Drawing.Point(0, 24);
            this._PanelProperties.Name = "_PanelProperties";
            this._PanelProperties.Size = new System.Drawing.Size(989, 500);
            this._PanelProperties.TabIndex = 1;
            // 
            // _TextMeta
            // 
            this._TextMeta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextMeta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextMeta.Location = new System.Drawing.Point(155, 258);
            this._TextMeta.Name = "_TextMeta";
            this._TextMeta.Size = new System.Drawing.Size(757, 25);
            this._TextMeta.TabIndex = 6;
            this._TextMeta.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelMeta
            // 
            this._LabelMeta.AutoSize = true;
            this._LabelMeta.Location = new System.Drawing.Point(56, 261);
            this._LabelMeta.Name = "_LabelMeta";
            this._LabelMeta.Size = new System.Drawing.Size(67, 17);
            this._LabelMeta.TabIndex = 6;
            this._LabelMeta.Tag = "ledx::pdiag.meta";
            this._LabelMeta.Text = "&Metadata:";
            // 
            // _TextPrefixes
            // 
            this._TextPrefixes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextPrefixes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextPrefixes.Location = new System.Drawing.Point(155, 74);
            this._TextPrefixes.Name = "_TextPrefixes";
            this._TextPrefixes.Size = new System.Drawing.Size(757, 25);
            this._TextPrefixes.TabIndex = 1;
            this._TextPrefixes.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelPrefixes
            // 
            this._LabelPrefixes.AutoSize = true;
            this._LabelPrefixes.Location = new System.Drawing.Point(56, 77);
            this._LabelPrefixes.Name = "_LabelPrefixes";
            this._LabelPrefixes.Size = new System.Drawing.Size(56, 17);
            this._LabelPrefixes.TabIndex = 1;
            this._LabelPrefixes.Tag = "ledx::pdiag.prefixes";
            this._LabelPrefixes.Text = "&Prefixes:";
            // 
            // _TextKey
            // 
            this._TextKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextKey.Location = new System.Drawing.Point(155, 105);
            this._TextKey.Name = "_TextKey";
            this._TextKey.Size = new System.Drawing.Size(100, 25);
            this._TextKey.TabIndex = 2;
            this._TextKey.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelKey
            // 
            this._LabelKey.AutoSize = true;
            this._LabelKey.Location = new System.Drawing.Point(56, 108);
            this._LabelKey.Name = "_LabelKey";
            this._LabelKey.Size = new System.Drawing.Size(93, 17);
            this._LabelKey.TabIndex = 2;
            this._LabelKey.Tag = "ledx::pdiag.langkey";
            this._LabelKey.Text = "Language &Key:";
            // 
            // _TextAuthor
            // 
            this._TextAuthor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextAuthor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextAuthor.Location = new System.Drawing.Point(155, 227);
            this._TextAuthor.Name = "_TextAuthor";
            this._TextAuthor.Size = new System.Drawing.Size(757, 25);
            this._TextAuthor.TabIndex = 5;
            this._TextAuthor.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelAuthor
            // 
            this._LabelAuthor.AutoSize = true;
            this._LabelAuthor.Location = new System.Drawing.Point(56, 230);
            this._LabelAuthor.Name = "_LabelAuthor";
            this._LabelAuthor.Size = new System.Drawing.Size(50, 17);
            this._LabelAuthor.TabIndex = 5;
            this._LabelAuthor.Tag = "ledx::pdiag.author";
            this._LabelAuthor.Text = "A&uthor:";
            // 
            // _TextApplication
            // 
            this._TextApplication.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextApplication.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextApplication.Location = new System.Drawing.Point(155, 196);
            this._TextApplication.Name = "_TextApplication";
            this._TextApplication.Size = new System.Drawing.Size(757, 25);
            this._TextApplication.TabIndex = 4;
            this._TextApplication.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelApplication
            // 
            this._LabelApplication.AutoSize = true;
            this._LabelApplication.Location = new System.Drawing.Point(56, 199);
            this._LabelApplication.Name = "_LabelApplication";
            this._LabelApplication.Size = new System.Drawing.Size(76, 17);
            this._LabelApplication.TabIndex = 4;
            this._LabelApplication.Tag = "ledx::pdiag.application";
            this._LabelApplication.Text = "&Application:";
            // 
            // _TextVersion
            // 
            this._TextVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextVersion.Location = new System.Drawing.Point(155, 164);
            this._TextVersion.Name = "_TextVersion";
            this._TextVersion.Size = new System.Drawing.Size(100, 25);
            this._TextVersion.TabIndex = 3;
            this._TextVersion.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelVersion
            // 
            this._LabelVersion.AutoSize = true;
            this._LabelVersion.Location = new System.Drawing.Point(59, 167);
            this._LabelVersion.Name = "_LabelVersion";
            this._LabelVersion.Size = new System.Drawing.Size(54, 17);
            this._LabelVersion.TabIndex = 3;
            this._LabelVersion.Tag = "ledx::pdiag.version";
            this._LabelVersion.Text = "&Version:";
            // 
            // _TextName
            // 
            this._TextName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextName.Location = new System.Drawing.Point(155, 43);
            this._TextName.Name = "_TextName";
            this._TextName.Size = new System.Drawing.Size(757, 25);
            this._TextName.TabIndex = 0;
            this._TextName.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelName
            // 
            this._LabelName.AutoSize = true;
            this._LabelName.Location = new System.Drawing.Point(56, 46);
            this._LabelName.Name = "_LabelName";
            this._LabelName.Size = new System.Drawing.Size(46, 17);
            this._LabelName.TabIndex = 0;
            this._LabelName.Tag = "ledx::pdiag.name";
            this._LabelName.Text = "&Name:";
            // 
            // _GridData
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._GridData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this._GridData.Location = new System.Drawing.Point(0, 24);
            this._GridData.Name = "_GridData";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._GridData.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._GridData.Size = new System.Drawing.Size(989, 524);
            this._GridData.TabIndex = 2;
            this._GridData.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this._GridData_EditingControlShowing);
            this._GridData.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this._GridData_RowValidating);
            // 
            // _PanelEmpty
            // 
            this._PanelEmpty.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this._PanelEmpty.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PanelEmpty.Location = new System.Drawing.Point(0, 0);
            this._PanelEmpty.Name = "_PanelEmpty";
            this._PanelEmpty.Size = new System.Drawing.Size(989, 524);
            this._PanelEmpty.TabIndex = 3;
            // 
            // _TextSource
            // 
            this._TextSource.AcceptsReturn = true;
            this._TextSource.AcceptsTab = true;
            this._TextSource.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._TextSource.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TextSource.HideSelection = false;
            this._TextSource.Location = new System.Drawing.Point(0, 24);
            this._TextSource.Multiline = true;
            this._TextSource.Name = "_TextSource";
            this._TextSource.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._TextSource.Size = new System.Drawing.Size(985, 500);
            this._TextSource.TabIndex = 4;
            this._TextSource.WordWrap = false;
            this._TextSource.TextChanged += new System.EventHandler(this._TextSource_TextChanged);
            // 
            // _PanelLocaleProperties
            // 
            this._PanelLocaleProperties.Controls.Add(this._CheckDefault);
            this._PanelLocaleProperties.Controls.Add(this._TextLocaleSmallIcon);
            this._PanelLocaleProperties.Controls.Add(this._LabelLocaleSmallIcon);
            this._PanelLocaleProperties.Controls.Add(this._TextLocaleIcon);
            this._PanelLocaleProperties.Controls.Add(this._LabelLocaleIcon);
            this._PanelLocaleProperties.Controls.Add(this._TextLocaleMeta);
            this._PanelLocaleProperties.Controls.Add(this._LabelLocaleMeta);
            this._PanelLocaleProperties.Controls.Add(this._TextLocaleBase);
            this._PanelLocaleProperties.Controls.Add(this._LabelLocaleBase);
            this._PanelLocaleProperties.Controls.Add(this._TextLocaleKey);
            this._PanelLocaleProperties.Controls.Add(this._LabelLocaleKey);
            this._PanelLocaleProperties.Controls.Add(this._TextLocaleAuthor);
            this._PanelLocaleProperties.Controls.Add(this._LabelLocaleAuthor);
            this._PanelLocaleProperties.Controls.Add(this._TextLocaleApplication);
            this._PanelLocaleProperties.Controls.Add(this._LabelLocaleApplication);
            this._PanelLocaleProperties.Controls.Add(this._TextLocaleVersion);
            this._PanelLocaleProperties.Controls.Add(this._LabelLocaleVersion);
            this._PanelLocaleProperties.Controls.Add(this._TextLocaleName);
            this._PanelLocaleProperties.Controls.Add(this._LabelLocaleName);
            this._PanelLocaleProperties.Dock = System.Windows.Forms.DockStyle.Fill;
            this._PanelLocaleProperties.Location = new System.Drawing.Point(0, 24);
            this._PanelLocaleProperties.Name = "_PanelLocaleProperties";
            this._PanelLocaleProperties.Size = new System.Drawing.Size(989, 500);
            this._PanelLocaleProperties.TabIndex = 7;
            // 
            // _CheckDefault
            // 
            this._CheckDefault.AutoSize = true;
            this._CheckDefault.Location = new System.Drawing.Point(133, 201);
            this._CheckDefault.Name = "_CheckDefault";
            this._CheckDefault.Size = new System.Drawing.Size(72, 21);
            this._CheckDefault.TabIndex = 9;
            this._CheckDefault.Text = " &Default";
            this._CheckDefault.UseVisualStyleBackColor = true;
            this._CheckDefault.CheckedChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _TextLocaleSmallIcon
            // 
            this._TextLocaleSmallIcon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextLocaleSmallIcon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLocaleSmallIcon.Location = new System.Drawing.Point(155, 166);
            this._TextLocaleSmallIcon.Name = "_TextLocaleSmallIcon";
            this._TextLocaleSmallIcon.Size = new System.Drawing.Size(757, 25);
            this._TextLocaleSmallIcon.TabIndex = 4;
            this._TextLocaleSmallIcon.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelLocaleSmallIcon
            // 
            this._LabelLocaleSmallIcon.AutoSize = true;
            this._LabelLocaleSmallIcon.Location = new System.Drawing.Point(56, 169);
            this._LabelLocaleSmallIcon.Name = "_LabelLocaleSmallIcon";
            this._LabelLocaleSmallIcon.Size = new System.Drawing.Size(70, 17);
            this._LabelLocaleSmallIcon.TabIndex = 4;
            this._LabelLocaleSmallIcon.Tag = "ledx::pdiag.smallicon";
            this._LabelLocaleSmallIcon.Text = "&Small Icon:";
            // 
            // _TextLocaleIcon
            // 
            this._TextLocaleIcon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextLocaleIcon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLocaleIcon.Location = new System.Drawing.Point(155, 135);
            this._TextLocaleIcon.Name = "_TextLocaleIcon";
            this._TextLocaleIcon.Size = new System.Drawing.Size(757, 25);
            this._TextLocaleIcon.TabIndex = 3;
            this._TextLocaleIcon.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelLocaleIcon
            // 
            this._LabelLocaleIcon.AutoSize = true;
            this._LabelLocaleIcon.Location = new System.Drawing.Point(56, 138);
            this._LabelLocaleIcon.Name = "_LabelLocaleIcon";
            this._LabelLocaleIcon.Size = new System.Drawing.Size(35, 17);
            this._LabelLocaleIcon.TabIndex = 3;
            this._LabelLocaleIcon.Tag = "ledx::pdiag.icon";
            this._LabelLocaleIcon.Text = "&Icon:";
            // 
            // _TextLocaleMeta
            // 
            this._TextLocaleMeta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextLocaleMeta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLocaleMeta.Location = new System.Drawing.Point(152, 349);
            this._TextLocaleMeta.Name = "_TextLocaleMeta";
            this._TextLocaleMeta.Size = new System.Drawing.Size(757, 25);
            this._TextLocaleMeta.TabIndex = 8;
            this._TextLocaleMeta.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelLocaleMeta
            // 
            this._LabelLocaleMeta.AutoSize = true;
            this._LabelLocaleMeta.Location = new System.Drawing.Point(53, 352);
            this._LabelLocaleMeta.Name = "_LabelLocaleMeta";
            this._LabelLocaleMeta.Size = new System.Drawing.Size(67, 17);
            this._LabelLocaleMeta.TabIndex = 8;
            this._LabelLocaleMeta.Tag = "ledx::pdiag.meta";
            this._LabelLocaleMeta.Text = "&Metadata:";
            // 
            // _TextLocaleBase
            // 
            this._TextLocaleBase.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextLocaleBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLocaleBase.Location = new System.Drawing.Point(155, 104);
            this._TextLocaleBase.Name = "_TextLocaleBase";
            this._TextLocaleBase.Size = new System.Drawing.Size(757, 25);
            this._TextLocaleBase.TabIndex = 2;
            this._TextLocaleBase.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelLocaleBase
            // 
            this._LabelLocaleBase.AutoSize = true;
            this._LabelLocaleBase.Location = new System.Drawing.Point(56, 107);
            this._LabelLocaleBase.Name = "_LabelLocaleBase";
            this._LabelLocaleBase.Size = new System.Drawing.Size(38, 17);
            this._LabelLocaleBase.TabIndex = 2;
            this._LabelLocaleBase.Tag = "ledx::pdiag.base";
            this._LabelLocaleBase.Text = "&Base:";
            // 
            // _TextLocaleKey
            // 
            this._TextLocaleKey.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextLocaleKey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLocaleKey.Location = new System.Drawing.Point(155, 74);
            this._TextLocaleKey.Name = "_TextLocaleKey";
            this._TextLocaleKey.Size = new System.Drawing.Size(100, 25);
            this._TextLocaleKey.TabIndex = 1;
            this._TextLocaleKey.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelLocaleKey
            // 
            this._LabelLocaleKey.AutoSize = true;
            this._LabelLocaleKey.Location = new System.Drawing.Point(56, 77);
            this._LabelLocaleKey.Name = "_LabelLocaleKey";
            this._LabelLocaleKey.Size = new System.Drawing.Size(93, 17);
            this._LabelLocaleKey.TabIndex = 1;
            this._LabelLocaleKey.Tag = "ledx::pdiag.langkey";
            this._LabelLocaleKey.Text = "Language &Key:";
            // 
            // _TextLocaleAuthor
            // 
            this._TextLocaleAuthor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextLocaleAuthor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLocaleAuthor.Location = new System.Drawing.Point(152, 318);
            this._TextLocaleAuthor.Name = "_TextLocaleAuthor";
            this._TextLocaleAuthor.Size = new System.Drawing.Size(757, 25);
            this._TextLocaleAuthor.TabIndex = 7;
            this._TextLocaleAuthor.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelLocaleAuthor
            // 
            this._LabelLocaleAuthor.AutoSize = true;
            this._LabelLocaleAuthor.Location = new System.Drawing.Point(53, 321);
            this._LabelLocaleAuthor.Name = "_LabelLocaleAuthor";
            this._LabelLocaleAuthor.Size = new System.Drawing.Size(50, 17);
            this._LabelLocaleAuthor.TabIndex = 7;
            this._LabelLocaleAuthor.Tag = "ledx::pdiag.author";
            this._LabelLocaleAuthor.Text = "A&uthor:";
            // 
            // _TextLocaleApplication
            // 
            this._TextLocaleApplication.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextLocaleApplication.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLocaleApplication.Location = new System.Drawing.Point(152, 287);
            this._TextLocaleApplication.Name = "_TextLocaleApplication";
            this._TextLocaleApplication.Size = new System.Drawing.Size(757, 25);
            this._TextLocaleApplication.TabIndex = 6;
            this._TextLocaleApplication.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelLocaleApplication
            // 
            this._LabelLocaleApplication.AutoSize = true;
            this._LabelLocaleApplication.Location = new System.Drawing.Point(53, 290);
            this._LabelLocaleApplication.Name = "_LabelLocaleApplication";
            this._LabelLocaleApplication.Size = new System.Drawing.Size(76, 17);
            this._LabelLocaleApplication.TabIndex = 6;
            this._LabelLocaleApplication.Tag = "ledx::pdiag.application";
            this._LabelLocaleApplication.Text = "&Application:";
            // 
            // _TextLocaleVersion
            // 
            this._TextLocaleVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLocaleVersion.Location = new System.Drawing.Point(152, 255);
            this._TextLocaleVersion.Name = "_TextLocaleVersion";
            this._TextLocaleVersion.Size = new System.Drawing.Size(100, 25);
            this._TextLocaleVersion.TabIndex = 5;
            this._TextLocaleVersion.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelLocaleVersion
            // 
            this._LabelLocaleVersion.AutoSize = true;
            this._LabelLocaleVersion.Location = new System.Drawing.Point(56, 258);
            this._LabelLocaleVersion.Name = "_LabelLocaleVersion";
            this._LabelLocaleVersion.Size = new System.Drawing.Size(54, 17);
            this._LabelLocaleVersion.TabIndex = 5;
            this._LabelLocaleVersion.Tag = "ledx::pdiag.version";
            this._LabelLocaleVersion.Text = "&Version:";
            // 
            // _TextLocaleName
            // 
            this._TextLocaleName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._TextLocaleName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextLocaleName.Location = new System.Drawing.Point(155, 43);
            this._TextLocaleName.Name = "_TextLocaleName";
            this._TextLocaleName.Size = new System.Drawing.Size(757, 25);
            this._TextLocaleName.TabIndex = 0;
            this._TextLocaleName.TextChanged += new System.EventHandler(this._OnPropertyChange);
            // 
            // _LabelLocaleName
            // 
            this._LabelLocaleName.AutoSize = true;
            this._LabelLocaleName.Location = new System.Drawing.Point(56, 46);
            this._LabelLocaleName.Name = "_LabelLocaleName";
            this._LabelLocaleName.Size = new System.Drawing.Size(46, 17);
            this._LabelLocaleName.TabIndex = 0;
            this._LabelLocaleName.Tag = "ledx::pdiag.name";
            this._LabelLocaleName.Text = "&Name:";
            // 
            // _FindSearch
            // 
            this._FindSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._FindSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._FindSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._FindSearch.Location = new System.Drawing.Point(512, 32);
            this._FindSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._FindSearch.Name = "_FindSearch";
            this._FindSearch.Pinned = false;
            this._FindSearch.SearchText = "";
            this._FindSearch.Size = new System.Drawing.Size(452, 125);
            this._FindSearch.TabIndex = 5;
            this._FindSearch.Visible = false;
            this._FindSearch.Search += new System.EventHandler(this._FindSearch_Search);
            // 
            // _ReplaceSearch
            // 
            this._ReplaceSearch.AllowReplace = true;
            this._ReplaceSearch.AllowReplaceAll = true;
            this._ReplaceSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._ReplaceSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._ReplaceSearch.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._ReplaceSearch.Location = new System.Drawing.Point(512, 32);
            this._ReplaceSearch.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._ReplaceSearch.Name = "_ReplaceSearch";
            this._ReplaceSearch.Pinned = false;
            this._ReplaceSearch.ReplaceText = "";
            this._ReplaceSearch.SearchText = "";
            this._ReplaceSearch.Size = new System.Drawing.Size(452, 181);
            this._ReplaceSearch.TabIndex = 6;
            this._ReplaceSearch.Visible = false;
            this._ReplaceSearch.SearchTextChanged += new System.EventHandler(this._ReplaceSearch_SearchTextChanged);
            this._ReplaceSearch.Search += new System.EventHandler(this._FindSearch_Search);
            this._ReplaceSearch.Replace += new System.EventHandler(this._ReplaceSearch_Replace);
            this._ReplaceSearch.ReplaceAll += new System.EventHandler(this._ReplaceSearch_ReplaceAll);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 524);
            this.Controls.Add(this._TextSource);
            this.Controls.Add(this._GridData);
            this.Controls.Add(this._PanelProperties);
            this.Controls.Add(this._PanelLocaleProperties);
            this.Controls.Add(this._FindSearch);
            this.Controls.Add(this._MenuStripMain);
            this.Controls.Add(this._ReplaceSearch);
            this.Controls.Add(this._PanelEmpty);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this._MenuStripMain;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormMain";
            this.Text = "LEd";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Resize += new System.EventHandler(this.FormMain_Resize);
            this._MenuStripMain.ResumeLayout(false);
            this._MenuStripMain.PerformLayout();
            this._PanelProperties.ResumeLayout(false);
            this._PanelProperties.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._GridData)).EndInit();
            this._PanelLocaleProperties.ResumeLayout(false);
            this._PanelLocaleProperties.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _MenuStripMain;
        private System.Windows.Forms.ToolStripMenuItem _MenuFile;
        private System.Windows.Forms.ToolStripMenuItem _MenuNew;
        private System.Windows.Forms.ToolStripMenuItem _MenuOpen;
        private System.Windows.Forms.ToolStripMenuItem _MenuSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuSaveAs;
        private System.Windows.Forms.ToolStripSeparator _MenuSeparatorFile0;
        private System.Windows.Forms.ToolStripMenuItem _MenuClose;
        private System.Windows.Forms.ToolStripMenuItem _MenuExit;
        private System.Windows.Forms.ToolStripMenuItem _MenuEdit;
        private System.Windows.Forms.ToolStripMenuItem _MenuCut;
        private System.Windows.Forms.ToolStripMenuItem _MenuCopy;
        private System.Windows.Forms.ToolStripMenuItem _MenuPaste;
        private System.Windows.Forms.ToolStripSeparator _MenuSeaparatorEdit0;
        private System.Windows.Forms.ToolStripMenuItem _MenuFind;
        private System.Windows.Forms.ToolStripMenuItem _MenuFindNext;
        private System.Windows.Forms.ToolStripSeparator _MenuSaparatorEdit1;
        private System.Windows.Forms.ToolStripMenuItem _MenuReplace;
        private System.Windows.Forms.ToolStripMenuItem _MenuDelete;
        private System.Windows.Forms.ToolStripMenuItem _MenuHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuShowHelp;
        private System.Windows.Forms.ToolStripMenuItem _MenuAbout;
        private System.Windows.Forms.ToolStripMenuItem _MenuView;
        private System.Windows.Forms.ToolStripMenuItem _MenuViewTable;
        private System.Windows.Forms.ToolStripMenuItem _MenuViewSource;
        private System.Windows.Forms.ToolStripSeparator _MenuSeparatorFile1;
        private System.Windows.Forms.ToolStripSeparator _MenuViewBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuViewProperties;
        private System.Windows.Forms.Panel _PanelProperties;
        private System.Windows.Forms.DataGridView _GridData;
        private System.Windows.Forms.Panel _PanelEmpty;
        private System.Windows.Forms.TextBox _TextSource;
        private System.Windows.Forms.TextBox _TextName;
        private System.Windows.Forms.Label _LabelName;
        private System.Windows.Forms.TextBox _TextApplication;
        private System.Windows.Forms.Label _LabelApplication;
        private System.Windows.Forms.TextBox _TextVersion;
        private System.Windows.Forms.Label _LabelVersion;
        private System.Windows.Forms.TextBox _TextMeta;
        private System.Windows.Forms.Label _LabelMeta;
        private System.Windows.Forms.TextBox _TextPrefixes;
        private System.Windows.Forms.Label _LabelPrefixes;
        private System.Windows.Forms.TextBox _TextKey;
        private System.Windows.Forms.Label _LabelKey;
        private System.Windows.Forms.TextBox _TextAuthor;
        private System.Windows.Forms.Label _LabelAuthor;
        private System.Windows.Forms.ToolStripMenuItem _MenuSelectAll;
        private System.Windows.Forms.ToolStripMenuItem _MenuDeselectAll;
        private System.Windows.Forms.ToolStripMenuItem _MenuUndo;
        private System.Windows.Forms.ToolStripSeparator _MenuEditBlank2;
        private Robbiblubber.Util.Controls.FindControl _FindSearch;
        private System.Windows.Forms.ToolStripMenuItem _MenuTools;
        private System.Windows.Forms.ToolStripMenuItem _MenuImport;
        private Robbiblubber.Util.Controls.ReplaceControl _ReplaceSearch;
        private System.Windows.Forms.ToolStripSeparator _MenuToolsBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuSettings;
        private System.Windows.Forms.ToolTip _ToolTip;
        private System.Windows.Forms.ToolStripMenuItem _MenuNewTable;
        private System.Windows.Forms.ToolStripMenuItem _MenuNewDefinition;
        private System.Windows.Forms.Panel _PanelLocaleProperties;
        private System.Windows.Forms.TextBox _TextLocaleSmallIcon;
        private System.Windows.Forms.Label _LabelLocaleSmallIcon;
        private System.Windows.Forms.TextBox _TextLocaleIcon;
        private System.Windows.Forms.Label _LabelLocaleIcon;
        private System.Windows.Forms.TextBox _TextLocaleMeta;
        private System.Windows.Forms.Label _LabelLocaleMeta;
        private System.Windows.Forms.TextBox _TextLocaleBase;
        private System.Windows.Forms.Label _LabelLocaleBase;
        private System.Windows.Forms.TextBox _TextLocaleKey;
        private System.Windows.Forms.Label _LabelLocaleKey;
        private System.Windows.Forms.TextBox _TextLocaleAuthor;
        private System.Windows.Forms.Label _LabelLocaleAuthor;
        private System.Windows.Forms.TextBox _TextLocaleApplication;
        private System.Windows.Forms.Label _LabelLocaleApplication;
        private System.Windows.Forms.TextBox _TextLocaleVersion;
        private System.Windows.Forms.Label _LabelLocaleVersion;
        private System.Windows.Forms.TextBox _TextLocaleName;
        private System.Windows.Forms.Label _LabelLocaleName;
        private System.Windows.Forms.ToolStripMenuItem _MenuRecent;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebug;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStart;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugStop;
        private System.Windows.Forms.ToolStripSeparator _MenuDebugBlank0;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugSave;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugView;
        private System.Windows.Forms.ToolStripMenuItem _MenuDebugUpload;
        private System.Windows.Forms.CheckBox _CheckDefault;
    }
}

