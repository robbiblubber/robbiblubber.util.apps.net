﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Robbiblubber.Util.Coding;



namespace Robbiblubber.Util.Apps.LocaleEditor
{
    /// <summary>Locale definition class.</summary>
    public class TTable: IDefinition
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Data.</summary>
        private List<TData> _Data = new List<TData>();

        /// <summary>Properties.</summary>
        private TProperties _Properties = new TProperties();

        /// <summary>Saved source.</summary>
        private string _SavedSource = "*";

        /// <summary>Modified flag.</summary>
        private bool _Modified = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public TTable()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        public TTable(string fileName)
        {
            if(fileName.ToLower().EndsWith(".phil7"))
            {
                _FromPhil(fileName);
            }
            else
            {
                IEncoder enc = Base64.Instance;

                if(fileName.ToLower().EndsWith(".lstab"))
                {
                    enc = PlainText.Instance;
                }
                else if(fileName.ToLower().EndsWith(".lsctab"))
                {
                    enc = GZip.BASE64;
                }

                _FromCfg(Ddp.Load(fileName, enc));
            }

            FileName = fileName;
            _SavedSource = _ToCfg().Text;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static methods                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Masks a PHP string literal.</summary>
        /// <param name="value">Value.</param>
        /// <returns>Masked value.</returns>
        internal static string __MaskPhil(string value)
        {
            return value.Replace(@"\", @"\\").Replace("'", @"\'");
        }


        /// <summary>Unmasks a PHP string literal.</summary>
        /// <param name="value">Value.</param>
        /// <returns>Unmasked value.</returns>
        internal static string __UnmaskPhil(string value)
        {
            return value.Replace(@"\'", "'").Replace(@"\\", @"\");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the definition source.</summary>
        public string Source
        {
            get
            {
                return _ToCfg().Text;
            }
            set
            {
                _FromCfg(Ddp.Parse(value));
            }
        }


        /// <summary>Gets if the definition is a table.</summary>
        public bool IsTable
        {
            get { return true; }
        }


        /// <summary>Gets the definition data.</summary>
        public List<TData> Data
        {
            get { return _Data; }
        }


        /// <summary>Gets the definition properties</summary>
        public IProperties Properties
        {
            get { return _Properties; }
        }


        /// <summary>Gets or sets the file name for this instace.</summary>
        public string FileName
        {
            get; set;
        }


        /// <summary>Gets a value indicating if the object has been modified.</summary>
        public bool Modified
        {
            get
            {
                if(_Modified) { return true; }
                return (_Modified = (_SavedSource != _ToCfg().Text));
            }
        }


        /// <summary>Gets the saved source.</summary>
        public string SavedSource
        {
            get { return _SavedSource; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the object.</summary>
        /// <param name="filename">File name.</param>
        public void Save(string filename)
        {
            FileName = filename;
            Save();
        }


        /// <summary>Saves the object.</summary>
        public void Save()
        {
            Ddp cfg = _ToCfg();

            if(FileName.ToLower().EndsWith(".phil7"))
            {
                File.WriteAllText(FileName, _ToPhil());
            }
            else
            {
                IEncoder enc = Base64.Instance;

                if(FileName.ToLower().EndsWith(".lstab"))
                {
                    enc = PlainText.Instance;
                }
                else if(FileName.ToLower().EndsWith(".lsctab"))
                {
                    enc = GZip.BASE64;
                }

                cfg.Save(FileName, enc);
            }

            _SavedSource = cfg.Text;
            _Modified = false;
        }


        /// <summary>Returns if the locale contains a definition for a symbol.</summary>
        /// <param name="symbol">Symbol.</param>
        /// <returns>Returns TRUE if the local contains a definition for the symbol, otherwise returns FALSE.</returns>
        public bool ContainsDefinition(string symbol)
        {
            foreach(TData i in Data)
            {
                if(i.Symbol == symbol) { return true; }
            }

            return false;
        }


        /// <summary>Returns if the locale contains a definition for a symbol.</summary>
        /// <param name="data">Data.</param>
        /// <returns>Returns TRUE if the local contains a definition for the symbol, otherwise returns FALSE.</returns>
        public bool ContainsDefinition(TData data)
        {
            return ContainsDefinition(data.Symbol);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a configuration file object for this instance.</summary>
        /// <returns>Configuration file.</returns>
        private Ddp _ToCfg()
        {
            Ddp rval = new Ddp();

            if(string.IsNullOrWhiteSpace(_Properties.Version)) { _Properties.Version = "1.0"; }
            rval.SetValue("/lstab/version", _Properties.Version);

            if(!string.IsNullOrWhiteSpace(_Properties.Name)) { rval.SetValue("/lstab/name", _Properties.Name); }
            if(!string.IsNullOrWhiteSpace(_Properties.Application)) { rval.SetValue("/lstab/application", _Properties.Application); }
            if(!string.IsNullOrWhiteSpace(_Properties.Author)) { rval.SetValue("/lstab/author", _Properties.Author); }
            if(!string.IsNullOrWhiteSpace(_Properties.Key)) { rval.SetValue("/lstab/lang", _Properties.Key); }
            if(!string.IsNullOrWhiteSpace(_Properties.Meta)) { rval.SetValue("/lstab/meta", _Properties.Meta); }
            if(!string.IsNullOrWhiteSpace(_Properties.Prefixes)) { rval.SetValue("/lstab/prefixes", _Properties.Prefixes); }

            foreach(TData i in _Data)
            {
                if(!string.IsNullOrWhiteSpace(i.Symbol)) { rval.SetValue("/data/" + i.Symbol, i.Expression); }
            }

            return rval;
        }


        /// <summary>Imports a configuration file object into this instance.</summary>
        /// <param name="cfg">Configuration file.</param>
        private void _FromCfg(Ddp cfg)
        {
            _Properties.Version = cfg.GetString("/lstab/version");
            _Properties.Name = cfg.GetString("/lstab/name");
            _Properties.Application = cfg.GetString("/lstab/application");
            _Properties.Author = cfg.GetString("/lstab/author");
            _Properties.Key = cfg.GetString("/lstab/lang");
            _Properties.Meta = cfg.GetString("/lstab/meta");
            _Properties.Prefixes = cfg.GetString("/lstab/prefixes");

            _Data.Clear();

            foreach(DdpEntry i in cfg.Sections["data"].Entries)
            {
                _Data.Add(new TData(i.Name, i.StringValue));
            }
            _Data.Sort();
        }


        /// <summary>Returns phil-file contents.</summary>
        /// <returns>PHP code.</returns>
        private string _ToPhil()
        {
            StringBuilder rval = new StringBuilder();

            rval.AppendLine(@"<?php namespace Robbiblubber\Util\Localization;");
            rval.AppendLine();
            rval.AppendLine("Locale::__feed(");
            rval.AppendLine("'" + __MaskPhil(_Properties.Version) + "',");
            rval.AppendLine("'" + __MaskPhil(_Properties.Name) + "',");
            rval.AppendLine("'" + __MaskPhil(_Properties.Application) + "',");
            rval.AppendLine("'" + __MaskPhil(_Properties.Author) + "',");
            rval.AppendLine("'" + __MaskPhil(_Properties.Key) + "',");
            rval.AppendLine("'" + __MaskPhil(_Properties.Meta) + "',");
            rval.AppendLine("'" + __MaskPhil(_Properties.Prefixes) + "',");
            rval.AppendLine("Array(");

            for(int i = 0; i < _Data.Count; i++)
            {
                rval.AppendLine("'" + __MaskPhil(_Data[i].Symbol) + "' => '" + __MaskPhil(_Data[i].Expression) + (((i + 1) < _Data.Count) ? "'," : "'"));
            }
            rval.AppendLine("));");
            rval.Append("?>");

            return rval.ToString();
        }


        /// <summary>Imports a phil7-file into this instance.</summary>
        /// <param name="fileName">File name.</param>
        private void _FromPhil(string fileName)
        {
            string[] lines = File.ReadAllLines(fileName);

            _Properties.Version = __UnmaskPhil(lines[3].Substring(1, lines[3].Length - 3));
            _Properties.Name = __UnmaskPhil(lines[4].Substring(1, lines[4].Length - 3));
            _Properties.Application = __UnmaskPhil(lines[5].Substring(1, lines[5].Length - 3));
            _Properties.Author = __UnmaskPhil(lines[6].Substring(1, lines[6].Length - 3));
            _Properties.Key = __UnmaskPhil(lines[7].Substring(1, lines[7].Length - 3));
            _Properties.Meta = __UnmaskPhil(lines[8].Substring(1, lines[8].Length - 3));
            _Properties.Prefixes = __UnmaskPhil(lines[9].Substring(1, lines[9].Length - 3));

            _Data.Clear();
            
            for(int i = 11; i < lines.Length; i++)
            {
                string data = null;
                if(lines[i].EndsWith("',"))
                {
                    data = lines[i].Substring(1, lines[i].Length - 3);
                }
                else if(lines[i].EndsWith("'"))
                {
                    data = lines[i].Substring(1, lines[i].Length - 2);
                }
                else if(lines[i] == "));")
                {
                    break;
                }
                else continue;

                string spc_ap = ("" + ((char) 4));
                while(data.Contains(spc_ap)) { spc_ap += ((char) 4); }
                data = data.Replace(@"\'", spc_ap);

                int split = data.IndexOf("' => '");
                _Data.Add(new TData(data.Substring(0, split).Replace(spc_ap, "'").Replace(@"\\", @"\"), data.Substring(split + 6).Replace(spc_ap, "'").Replace(@"\\", @"\")));
            }
            
            _Data.Sort();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] TProperties                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class provides locale properties.</summary>
        public class TProperties: IProperties
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            public TProperties()
            {
                Version = "1.0";
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets or sets the locale table name.</summary>
            public string Name { get; set; }

            /// <summary>Gets or sets the locale table version.</summary>
            public string Version { get; set; }

            /// <summary>Gets or sets the locale table application name.</summary>
            public string Application { get; set; }

            /// <summary>Gets or sets the locale table author.</summary>
            public string Author { get; set; }

            /// <summary>Gets or sets the locale language key.</summary>
            public string Key { get; set; }

            /// <summary>Gets or sets the locale table metadata.</summary>
            public string Meta { get; set; }

            /// <summary>Gets or sets the locale table prefixes.</summary>
            public string Prefixes { get; set; }
        }
    }
}
