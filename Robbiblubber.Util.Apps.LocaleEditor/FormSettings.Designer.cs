﻿namespace Robbiblubber.Util.Apps.LocaleEditor
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
            this._LabelLanguage = new System.Windows.Forms.Label();
            this._ButtonOK = new System.Windows.Forms.Button();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._LabelMessage = new System.Windows.Forms.Label();
            this._ChkDebug = new System.Windows.Forms.CheckBox();
            this._LcboLanguage = new Robbiblubber.Util.Localization.Controls.LocaleComboBox();
            this.SuspendLayout();
            // 
            // _LabelLanguage
            // 
            this._LabelLanguage.AutoSize = true;
            this._LabelLanguage.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelLanguage.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelLanguage.Location = new System.Drawing.Point(40, 18);
            this._LabelLanguage.Name = "_LabelLanguage";
            this._LabelLanguage.Size = new System.Drawing.Size(135, 13);
            this._LabelLanguage.TabIndex = 1;
            this._LabelLanguage.Tag = "ledx::settings.language.text";
            this._LabelLanguage.Text = "User Interface &Language:";
            // 
            // _ButtonOK
            // 
            this._ButtonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ButtonOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonOK.Location = new System.Drawing.Point(115, 153);
            this._ButtonOK.Name = "_ButtonOK";
            this._ButtonOK.Size = new System.Drawing.Size(147, 29);
            this._ButtonOK.TabIndex = 3;
            this._ButtonOK.Tag = "utctrl::common.button.ok";
            this._ButtonOK.Text = "&OK";
            this._ButtonOK.UseVisualStyleBackColor = true;
            this._ButtonOK.Click += new System.EventHandler(this._ButtonOK_Click);
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(268, 153);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(147, 29);
            this._ButtonCancel.TabIndex = 4;
            this._ButtonCancel.Tag = "utctrl::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _LabelMessage
            // 
            this._LabelMessage.AutoSize = true;
            this._LabelMessage.Location = new System.Drawing.Point(40, 51);
            this._LabelMessage.Name = "_LabelMessage";
            this._LabelMessage.Size = new System.Drawing.Size(262, 17);
            this._LabelMessage.TabIndex = 5;
            this._LabelMessage.Text = "Language options are currently unavailable.\r\n";
            this._LabelMessage.Visible = false;
            // 
            // _ChkDebug
            // 
            this._ChkDebug.AutoSize = true;
            this._ChkDebug.Location = new System.Drawing.Point(43, 110);
            this._ChkDebug.Name = "_ChkDebug";
            this._ChkDebug.Size = new System.Drawing.Size(372, 21);
            this._ChkDebug.TabIndex = 8;
            this._ChkDebug.Tag = "debugx::udiag.enable";
            this._ChkDebug.Text = "   Enable &debug features (show Debug menu in menu strip)";
            this._ChkDebug.UseVisualStyleBackColor = true;
            // 
            // _LcboLanguage
            // 
            this._LcboLanguage.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LcboLanguage.Location = new System.Drawing.Point(43, 37);
            this._LcboLanguage.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LcboLanguage.Name = "_LcboLanguage";
            this._LcboLanguage.Size = new System.Drawing.Size(372, 31);
            this._LcboLanguage.TabIndex = 2;
            this._LcboLanguage.Value = null;
            // 
            // FormSettings
            // 
            this.AcceptButton = this._ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(459, 208);
            this.Controls.Add(this._ChkDebug);
            this.Controls.Add(this._ButtonOK);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._LcboLanguage);
            this.Controls.Add(this._LabelLanguage);
            this.Controls.Add(this._LabelMessage);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSettings";
            this.Tag = "ledx::settings.window.caption";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelLanguage;
        private Robbiblubber.Util.Localization.Controls.LocaleComboBox _LcboLanguage;
        private System.Windows.Forms.Button _ButtonOK;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Label _LabelMessage;
        private System.Windows.Forms.CheckBox _ChkDebug;
    }
}