﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization;
using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Apps.LocaleEditor
{
    /// <summary>This class implements the settings window.</summary>
    public partial class FormSettings: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormSettings()
        {
            InitializeComponent();

            this.Localize();

            ShowDebug = _ChkDebug.Checked = Program.__Main.ShowDebug;

            if(Locale.Locales.Count == 0)
            {
                _LabelLanguage.Visible = _LcboLanguage.Visible = false;
                _LabelMessage.Visible = true;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the selected locale.</summary>
        public Locale SelectedLocale
        {
            get; private set;
        }


        /// <summary>Gets if the debug menu is shown.</summary>
        public bool ShowDebug
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Button "Cancel" click.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        /// <summary>Button "OK" click.</summary>
        private void _ButtonOK_Click(object sender, EventArgs e)
        {
            SelectedLocale = _LcboLanguage.Value;
            ShowDebug = _ChkDebug.Checked;

            Close();
        }
    }
}
