﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Apps.LocaleEditor
{
    /// <summary>About form class.</summary>
    public partial class FormAbout: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FormAbout()
        {
            InitializeComponent();

            _LabelVersion.Text = "Version " + VersionOp.ApplicationVersion.ToVersionString();
        }
    }
}
