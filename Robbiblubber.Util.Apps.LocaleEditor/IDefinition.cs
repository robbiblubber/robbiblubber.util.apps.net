﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Util.Apps.LocaleEditor
{
    /// <summary>Locale definitions and tables implement this interface.</summary>
    public interface IDefinition
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the definition source.</summary>
        string Source { get; set; }


        /// <summary>Gets if the definition is a table.</summary>
        bool IsTable { get; }


        /// <summary>Gets the definition data.</summary>
        List<TData> Data { get; }


        /// <summary>Gets the definition properties</summary>
        IProperties Properties { get; }


        /// <summary>Gets or sets the file name for this instace.</summary>
        string FileName { get; set; }


        /// <summary>Gets a value indicating if the object has been modified.</summary>
        bool Modified { get; }


        /// <summary>Gets the saved source.</summary>
        string SavedSource { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the object.</summary>
        /// <param name="filename">File name.</param>
        void Save(string filename);


        /// <summary>Saves the object.</summary>
        void Save();
    }
}
