﻿using System;



namespace Robbiblubber.Util.Apps.LocaleEditor
{
    /// <summary>Properties implement this interface.</summary>
    public interface IProperties
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the locale definition name.</summary>
        string Name { get; set; }

        /// <summary>Gets or sets the locale definition version.</summary>
        string Version { get; set; }

        /// <summary>Gets or sets the locale definition application name.</summary>
        string Application { get; set; }

        /// <summary>Gets or sets the locale definition author.</summary>
        string Author { get; set; }

        /// <summary>Gets or sets the locale language key.</summary>
        string Key { get; set; }

        /// <summary>Gets or sets the locale definition metadata.</summary>
        string Meta { get; set; }
    }
}
